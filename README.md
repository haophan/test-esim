# Installing

Prerequisites:

- Node 14+

To set up the app execute the following commands.

```bash
git clone https://github.com/ITR-vietnam/btcy-bioflux-frontend-bioheart_e_commerce
cd btcy-bioflux-frontend-bioheart_e_commerce
npm install
```

## Start development server

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Build

For dev: `env` includes `alpha`, `delta`, `staging`

```bash
npm run build:<env>

# Example:
npm run build:alpha
```

For production:

```bash
npm run build
```

Run build:

```bash
npm start
```
