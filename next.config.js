require('dotenv').config({ path: 'env' });
const webpack = require('webpack');

const withImages = require('next-images');

/**
 * @type {import('next').NextConfig}
 */
const nextConfig = withImages({
  reactStrictMode: false,
  images: {
    disableStaticImages: true,
    loader: 'akamai',
    path: '',
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/home',
        permanent: true,
      },
    ];
  },
  trailingSlash: true,
  async exportPathMap(defaultPathMap, {
    dev, dir, outDir, distDir, biuldId,
  }) {
    return {
      '/': { page: '/' },
      '/login': { page: '/login' },
      '/signup': { page: '/signup' },
      '/forgot-password': { page: '/forgot-password' },
      '/home': { page: '/home' },
      '/checkout': { page: '/checkout' },
      '/products/[slug]': { page: '/products/[slug]' },
      '/payment-result': {page: '/payment-result'},
      '/verify-email': {page: '/verify-email'},
    };
  },
  webpack: (config) => {
    config.plugins.push(
      new webpack.EnvironmentPlugin(process.env),
    );
    return config;
  },
  output: 'standalone',
});

module.exports = nextConfig;

