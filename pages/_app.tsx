/* eslint-disable react/jsx-filename-extension */
import React, { useEffect, useState } from 'react';
import Amplify from '@aws-amplify/core';
import { CacheProvider } from '@emotion/react';
import { ThemeProvider } from '@mui/material/styles';
import { AppProps } from 'next/app';
import Head from 'next/head';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { updatedAwsConfig } from '../src/aws-amplify/handler';
import AppComponent from '../src/layout/appComponent';
import ShopLayoutComponent from '../src/layout/shopLayoutComponent';
import persistorRedux from '../src/redux';
import theme from '../src/theme';
import createEmotionCache from '../src/utils/createEmotionCache';
import '../styles/globals.css';
import '../styles/styles.scss';
import LoadingPage from '../src/components/loadingPage';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

Amplify.configure(updatedAwsConfig());

const MyApp = ({ Component, pageProps }: AppProps) => {
  const [isSSR, setIsSSR] = useState(true);

  useEffect(() => {
    // Resolve Hydration error React 18
    setIsSSR(false);
  }, []);

  return (
    <Provider store={persistorRedux.reduxStore}>
      <PersistGate loading={null} persistor={persistorRedux.persistor}>
        <CacheProvider value={clientSideEmotionCache}>
          <Head>
            <title>Bioheart - shop</title>
            <meta charSet="utf-8" />
            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
          </Head>
          {!isSSR && (
          <ThemeProvider theme={theme}>
            <AppComponent />
            <LoadingPage />
            <ShopLayoutComponent >
              <Component {...pageProps} />
            </ShopLayoutComponent>
          </ThemeProvider>
          )}
        </CacheProvider>
      </PersistGate>
    </Provider>
  );
};

export default MyApp;
