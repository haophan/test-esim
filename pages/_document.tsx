/* eslint-disable react/jsx-filename-extension */
import Document, {
  Head, Html, Main, NextScript,
} from 'next/document';
import { ServerStyleSheets } from '@mui/styles';
import React from 'react';
import createEmotionServer from '@emotion/server/create-instance';
import createEmotionCache from '../src/utils/createEmotionCache';

// import SEO from '../site-config.js'
// import { withTranslation } from '../i18n.js';

class MyDocument extends Document {
  // static async getInitialProps(ctx) {
  //     const initialProps = await Document.getInitialProps(ctx);
  //     let props = { ...initialProps };

  //     return props;
  // }

  render() {
    return (
      <Html lang="en">
        <Head>
          {/* <link href="../styles/fonts.module.css" rel="stylesheet"/> */}
          {/* <link rel="preload" href="/fonts/Cera-Pro-Regular.ttf" as="font" type="font/woff2" crossOrigin="anonymous" />
                    <link rel="preload" href="/fonts/Cera-Pro-Medium.ttf" as="font" type="font/woff2" crossOrigin="anonymous" />
                    <link rel="preload" href="/fonts/Cera-Pro-Bold.ttf" as="font" type="font/woff2" crossOrigin="anonymous" /> */}
          <link rel="icon" href="/images/shop/png/logo-bioheart.png" />
          <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyConH6CaPysaRd5GYFTN1N8Yy3b1RpxIbA&libraries=places" />
        </Head>
        <body>
          <Main />
          <NextScript />

          {/* <script src="/static/js/vendor/jquery-2.2.4.min.js"></script>
                    <script src="/static/js/popper.min.js"></script>
                    <script src="/static/js/vendor/bootstrap.min.js"></script>
                    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
                    <script src="/static/js/easing.min.js"></script>
                    <script src="/static/js/hoverIntent.js"></script>
                    <script src="/static/js/superfish.min.js"></script>
                    <script src="/static/js/jquery.ajaxchimp.min.js"></script>
                    <script src="/static/js/jquery.magnific-popup.min.js"></script>
                    <script src="/static/js/hexagons.min.js"></script>
                    <script src="/static/js/jquery.nice-select.min.js"></script>
                    <script src="/static/js/jquery.counterup.min.js"></script>
                    <script src="/static/js/waypoints.min.js"></script>
                    <script src="/static/js/mail-script.js"></script>
                    <script src="/static/js/product-detail.js"></script>
                    <script src="/static/js/chatbot.js"></script> */}
        </body>
      </Html>
    );
  }
}

MyDocument.getInitialProps = async (ctx) => {
  const originalRenderPage = ctx.renderPage;
  const sheets = new ServerStyleSheets();

  // You can consider sharing the same emotion cache between all the SSR requests to speed up performance.
  // However, be aware that it can have global side effects.
  const cache = createEmotionCache();
  const { extractCriticalToChunks } = createEmotionServer(cache);

  ctx.renderPage = () => originalRenderPage({
    enhanceApp: (WrappedComponent: any) => (props: any) => sheets.collect(<WrappedComponent emotionCache={cache} {...props} />),
  });

  const initialProps = await Document.getInitialProps(ctx);
  // This is important. It prevents emotion to render invalid HTML.
  // See https://github.com/mui/material-ui/issues/26561#issuecomment-855286153
  const emotionStyles = extractCriticalToChunks(initialProps.html);
  const emotionStyleTags = emotionStyles.styles.map((style) => (
    <style
      data-emotion={`${style.key} ${style.ids.join(' ')}`}
      key={style.key}
            // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: style.css }}
    />
  ));
  return {
    ...initialProps,
    emotionStyleTags,
    styles: (
      <>
        {initialProps.styles}
        {sheets.getStyleElement()}
      </>
    ),
  };
};

// export default withTranslation('common')(MyDocument);
export default MyDocument;
