import { Box } from "@mui/material";
import { NextRouter, useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Checkout from "../../src/components/checkout";
import CheckOutInstruction from "../../src/components/checkOutInstruction";
import { auth } from "../../src/utils/auth";

import styles from "./styles.module.scss";

interface IProps {}

// enum Status {
//   CHECKOUT = "CHECKOUT",
//   INSTRUCTION = "INSTRUCTION",
// }

const CheckoutPage = (props: IProps): JSX.Element => {
  // CHECKOUT, INSTRUCTION


  const isLoggedIn = auth.isLoggedIn();
  if (!isLoggedIn) {
    return (
      <Box className={styles.checkoutPage}>
        <CheckOutInstruction  />
      </Box>
    );
  }

  // when checkout success (receive an successfull callback from stripe --> clear cart)
  return (
    <Box className={styles.checkoutPage} >
      <Checkout />
    </Box>
  );
};

export default CheckoutPage;
