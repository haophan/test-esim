import { Box } from '@mui/material';
import { NextRouter, useRouter } from 'next/router';
import React, { useRef, useState } from 'react';
import ForgetPassword from '../../src/layout/forgetPassword';
import { useMergeState } from '../../src/utils/hooks/useMergeState';
import style from './style.module.scss';

const ForgotPassword = () => {
  const router: NextRouter = useRouter();

  const [state, setState] = useMergeState({
    currentAction: 'FORGOT_PASSWORD',
  });

  const [currentStep, setCurrentStep] = useState<number>(1);

  const email = useRef<string>('');

  const onMoveToUpdatePassword = (username: string) => {
    email.current = username;
    setCurrentStep(2);
  };

  // const onPasswordUpdated = () => {
  //   setState({ currentAction: 'PASSWORD_UPDATED' });
  // };

  const backToSignIn = () => {
    router.replace('/login');
  };

  return (
    <Box className={style.forgotPassword}>
      <ForgetPassword />
    </Box>
  );
};

export default ForgotPassword;
