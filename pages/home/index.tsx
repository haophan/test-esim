import { Box } from "@mui/material";
import React from "react";
import HomeStore from "../../src/layout/homeStore";

import IBriefProductCard from "../../src/modals/briefProductCardInterface";
import IBriefProduct from "../../src/modals/briefProductInterface";
import IHomeProduct from "../../src/modals/homeProductInterface";
import IJSONResponse from "../../src/modals/jsonResponseInterface";

import { getHomeProductV2 } from "../../src/rest/get/getHomeProductV2";
import { GetServerSideProps } from "next";

import styles from "./styles.module.scss";
import _ from "lodash";

interface IProps {
  homeProduct: IBriefProduct;
  relationProducts: IBriefProductCard[];
}

const HomePage = (props: IProps): JSX.Element => {
  if (_.isEmpty(props)) {
    return <></>
  }
  return (
    <Box className={styles.homePage}>
      <HomeStore
        homeProduct={props.homeProduct || {}}
        relationProducts={props.relationProducts || []}
      />
    </Box>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const result: IJSONResponse<IHomeProduct> = await getHomeProductV2();
  
  return {
    props: {
      ...result.data
    }, // will be passed to the page component as props
  };
};

export default HomePage;
