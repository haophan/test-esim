import React, { useEffect } from "react";
import { NextRouter, useRouter } from "next/router";

const Home = (): JSX.Element => {
  const router: NextRouter = useRouter();

  useEffect(() => {
    router.push("/home");
  }, [router.isReady]);

  return <></>;
};

// export async function getStaticProps() {
//   return {
//     redirect: {
//       destination: '/account',
//       permanent: true,
//     },
//   };
// }

export default Home;
