import {
  Box
} from '@mui/material';
import { NextRouter, useRouter } from 'next/router';
import React, { useEffect } from 'react';
import Login from '../../src/layout/login';
import { auth } from '../../src/utils/auth';
import style from './style.module.scss';

const LoginPage = (): JSX.Element => {
  const router: NextRouter = useRouter();
  const isLoggedIn: boolean = auth.isLoggedIn();
  
  useEffect(() => {
    if (isLoggedIn) {
      router.push('/home/');
    }
  }, [router.isReady])

  return (
    <Box className={style.loginPage}>
      <Login />
    </Box>
  );
};

export default LoginPage;
