import { Box } from "@mui/material";
import { NextRouter, useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import CheckoutResult from "../../src/components/checkoutResult";
import { removeBuyNow } from "../../src/redux/reducers/buyNow";
import { clearCart } from "../../src/redux/reducers/cart";

import styles from "./styles.module.scss";

interface IProps {}

enum Status {
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}

const PaymentResultPage = (props: IProps): JSX.Element => {
  // SUCCESS, FAILED
  const dispatch = useDispatch();
  const [state, setState] = useState<string>(Status.SUCCESS);
  const [orderId, setOrderId] = useState<number>(0);
  const [failedFromBuyNow, setFailedFromBuyNow] = useState<boolean>(false);

  const router: NextRouter = useRouter();

  useEffect(() => {
    if (router.query.status) {
      setState(router.query.status.toString().toUpperCase());
      if (router.query.status === Status.SUCCESS) {
        if (router.query.fromBuyNow && router.query.fromBuyNow === 'true') {
          dispatch(removeBuyNow());
        } else {
          dispatch(clearCart());
        }
      }
      if (router.query.status === Status.FAILED) {
        if (router.query.fromBuyNow) {
          setFailedFromBuyNow(router.query?.fromBuyNow === "true");
        }
      }
    }
    if (router.query.orderId) {
      setOrderId(parseInt(router.query.orderId.toString(), 10));
    }
  }, [router.isReady]);

  // when checkout success (receive an successfull callback from stripe --> clear cart)
  return (
    <Box className={styles.paymentResultPage}>
      <CheckoutResult
        view={state}
        orderId={orderId}
        failedFromBuyNow={failedFromBuyNow}
      />
    </Box>
  );
};

export default PaymentResultPage;
