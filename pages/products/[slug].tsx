/* eslint-disable react/jsx-filename-extension */
//@ts-nocheck
import React, { useEffect, useState } from "react";
import { Box } from "@mui/material";

import { GetStaticPaths, GetStaticProps } from "next";
import { NextRouter, useRouter } from "next/router";

import { getProductBySlug } from "../../src/rest/get/getProductBySlug";
import { getProductSlugs } from "../../src/rest/get/getProductSlugs";

import IJSONResponse from "../../src/modals/jsonResponseInterface";
import IProductSlug from "../../src/modals/productSlugInterface";
import IProductDetail from "../../src/modals/productDetailInterface";
import IBriefProductCard from "../../src/modals/briefProductCardInterface";
import ViewProductDetail from "../../src/layout/viewProductDetail";
import IDetailProduct from "../../src/modals/detailProductInterface";

import styles from "./styles.module.scss";
import _ from "lodash";
import { useDispatch } from "react-redux";
import { setLoadingPage } from "../../src/redux/reducers/loading";
interface IProps {
  mainProduct: IDetailProduct;
  relationProducts: IBriefProductCard[];
  isError: boolean;
}

const ProductDetailPage = (props: IProps): JSX.Element => {
  const dispatch = useDispatch();
  const router: NextRouter = useRouter();
  const [state, setState] = useState<{
    mainProduct: IDetailProduct;
    relationProducts: IBriefProductCard[];
    isError?: boolean;
  }>();

  const [isLoadedData, setLoadedData] = useState<boolean>(false)

  useEffect(() => {
    const fetchProductData = async (productName: string) => {
      dispatch(setLoadingPage({ loading: true }));
      const result: IJSONResponse<IProductDetail> = await getProductBySlug(
        productName
      );
      if (result.errors) {
        setState({ ...state, isError: true });
        router.replace("/home/");
      }
      if (result.data) {
        setState({ ...result.data });
        setLoadedData(true);
      }
      dispatch(setLoadingPage({ loading: false }));
    };
    if (router.isReady) {
      fetchProductData(router.query.slug?.toString() || "");
    }
  }, [router.isReady]);

  // useEffect(() => {
  //   if (props.isError) {
  //     router.replace("/home");
  //   }
  // }, [props.isError, router.isReady]);

  // if (_.isEmpty(props)) {
  //   return <></>;
  // }

  return (
    <Box className={styles.productDetailPage}>
      {isLoadedData && (
        <ViewProductDetail
          mainProduct={state.mainProduct}
          relationProducts={state.relationProducts}
        />
      )}
    </Box>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  // const result: IJSONResponse<IProductSlug[]> = await getProductSlugs();
  let paths: { params: { slug: string } }[];
  // if (!result.data || result.errors) {
  //   paths = [];
  // } else {
  //   paths = result.data.map((slug: IProductSlug) => ({
  //     params: { slug: slug.slug.toString() },
  //   }));
  // }
  paths = ['bioheart-bundle', 'bioheart-strap', 'bioheart-charger'].map((slug: string) => ({
    params: { slug: slug.toString() },
  }));
  return {
    paths: paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  // const { params } = context;
  // if (params?.slug && typeof params?.slug === "string") {
  //   const result: IJSONResponse<IProductDetail> = await getProductBySlug(
  //     params.slug
  //   );
  //   if (!result.data) {
  //     return {
  //       props: {
  //         mainProduct: {},
  //         relationProducts: [],
  //         isError: true,
  //       },
  //       // redirect: {
  //       //   destination: "/home",
  //       //   permanent: false,
  //       // },
  //     };
  //   }
  //   return {
  //     props: {
  //       ...result.data,
  //       isError: false,
  //     },
  //   };
  // } else {
  //   return {
  //     props: {
  //       mainProduct: {},
  //       relationProducts: [],
  //       isError: true,
  //     },
  //     // redirect: {
  //     //   destination: "/home",
  //     //   permanent: false,
  //     // },
  //   };
  // }

  // // const result: IGetProductBySlugResponse | Error = await getProductBySlug(
  // //   params?.slug,
  // //   []
  // // );
  // // if (result instanceof Error) {
  // //   return {
  // //     redirect: {
  // //       destination: "/home",
  // //       permanent: false,
  // //     },
  // //   };
  // // }
  // // if (!result.mainProduct || !params?.slug) {
  // //   return {
  // //     redirect: {
  // //       destination: "/home",
  // //       permanent: false,
  // //     },
  // //   };
  // // }
  // return {
  //   props: {
  //     data: {},
  //   },
  //   // revalidate: 10,
  // };
  return {
    props: {
      mainProduct: {},
      relationProducts: [],
      isError: false,
    }
  }
};

export default ProductDetailPage;
