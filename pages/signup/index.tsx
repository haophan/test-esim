import { Box } from '@mui/material';
import React from 'react';
import SignUp from '../../src/layout/signUp';

import style from './style.module.scss';

const CreateAccount = () => {
  return (
    <Box className={style.createAccount}>
      <SignUp />
    </Box>
  );
};

export default CreateAccount;
