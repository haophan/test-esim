/* eslint-disable react/jsx-filename-extension */
import { Box, Link } from "@mui/material";
import { NextRouter, useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import CustomButton from "../../src/components/buttons/customButton";
import verifyEmail from "../../public/images/shop/png/verifyEmail.png";

import styles from "./styles.module.scss";

const VerifyEmail = (): JSX.Element => {
  // after sign up successfully
  // if account is not verifed
  const router: NextRouter = useRouter();
  const [email, setEmail] = useState<string>("");

  const onSignInClick = () => {
    router.push("/login");
  };

  useEffect(() => {
    if (router.isReady && router.query) {
      if (router.query.email) {
        setEmail(router.query.email.toString());
      } else {
        router.push("/login");
      }
    }
  }, [router.isReady]);

  return (
    <Box className={styles.verifyEmail}>
      <Box className={styles.wrapper}>
        <img src={verifyEmail} alt="" />
        <h1 className={styles.title}>Verify your email</h1>
        <p className={styles.description}>
          We&apos;ve sent an email to <strong>{`${email}`}</strong> to verify
          your email address and activate your account. The link in the email
          will expire in 24 hours
        </p>
        <div className={styles.horizotalDivider} />
        <p className={styles.instruction}>
          Please verify your email before returning to the{" "}
        </p>
        <Link className={styles.signInBtn} href="/login">
          Sign in
        </Link>
      </Box>
    </Box>
  );
};

export default VerifyEmail;
