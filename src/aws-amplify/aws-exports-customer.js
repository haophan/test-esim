const aws = {
  aws_project_region: 'us-east-2',
  aws_cognito_identity_pool_id:
    'us-east-2:d330654d-a73b-4a6f-93ff-595169d13abc',
  aws_cognito_region: 'us-east-2',
  aws_user_pools_id: 'us-east-2_cGLQ1C29F',
  aws_user_pools_web_client_id: '3ga26h7k7rgkj00iajk1he7b9k',
  oauth: {
    domain: 'biocare-customer.auth.us-east-2.amazoncognito.com',
    scope: [
      'aws.cognito.signin.user.admin',
      'email',
      'openid',
      'phone',
      'profile',
    ],
    redirectSignIn: 'bioheart://',
    redirectSignOut: 'bioheart://',
    responseType: 'code',
  },
  federationTarget: 'COGNITO_USER_POOLS',
  aws_mobile_analytics_app_id: '8b1058d489254710aa8e2c8edbe5527c',
  aws_mobile_analytics_app_region: 'us-east-1',
  aws_user_files_s3_bucket: 'bioheart-user-files182600-customer',
  aws_user_files_s3_bucket_region: 'us-east-2',
};

export default aws;
