import awsConfigAlpha from './aws-exports-alpha';
import awsConfigDelta from './aws-exports-delta';
import awsConfigStaging from './aws-exports-staging';
import awsConfigCustomer from './aws-exports-customer';

const ENV_ENUM = {
  localhost: 'localhost',
  alpha: 'alpha',
  staging: 'staging',
  customer: 'customer',
  delta: 'delta',
};

// change env for developer
const env = process.env.HOST;
console.log('process.env.ENV', process.env.HOST);

const HOST_URL_ALPHA = 'https://shopad.alpha.bioheart.bioflux.io/api/';
const HOST_URL_DELTA = 'https://shopad.alpha.bioheart.bioflux.io/api/';
const HOST_URL_STAGING = 'https://gateway.staging.cardiac.biotricity.com/';
const HOST_URL_CUSTOMER = 'https://gateway.cardiac.biotricity.com/';

const LINK_REDIRECT_AWS_ALPHA = 'http://localhost:3000/login/';
const LINK_REDIRECT_AWS_DELTA = 'https://gateway.delta.cardiac.bioflux.io/login/';
const LINK_REDIRECT_AWS_STAGING = 'https://gateway.staging.cardiac.biotricity.com/';
const LINK_REDIRECT_AWS_CUSTOMER = 'https://gateway.cardiac.biotricity.com/';
const LINK_REDIRECT_AWS_LOCAL = 'http://localhost:3000/login/';

const LINK_LOGOUT_AWS_ALPHA = 'https://login.alpha.bioflux.io/session/end';
const LINK_LOGOUT_AWS_DELTA = 'https://login.delta.bioflux.io/session/end';
const LINK_LOGOUT_AWS_STAGING = 'https://login.staging.biotricity.com/session/end';
const LINK_LOGOUT_AWS_CUSTOMER = 'https://login.biotricity.com/session/end';

const URL_GEOCODE_ALPHA = 'https://sm.alpha.bioflux.io/api/geocode';
const URL_GEOCODE_DELTA = 'https://sm.delta.bioflux.io/api/geocode';
const URL_GEOCODE_STAGING = 'https://sm.staging.biotricity.com/api/geocode';
const URL_GEOCODE_CUSTOMER = 'https://sm.customer.biotricity.com/api/geocode';

const URL_SOCKET_ALPHA = 'https://sio.alpha.cardiac.bioflux.io';
const URL_SOCKET_DELTA = 'https://sio.delta.cardiac.bioflux.io';
const URL_SOCKET_STAGING = 'https://sio.staging.cardiac.biotricity.com/';
const URL_SOCKET_CUSTOMER = 'https://sio.cardiac.biotricity.com';

const STRIPE_KEY_ALPHA = 'pk_test_51IaDh1HDRhnbjf6yNVgtYOtcMDWTFq708CzPReYcuRgicQvTKTbnQBNXRZtFIywuB3J7B5bdfg34atKqrBFuwm0m00jHAMzV76';
const STRIPE_KEY_DELTA = 'pk_test_51IaDh1HDRhnbjf6yNVgtYOtcMDWTFq708CzPReYcuRgicQvTKTbnQBNXRZtFIywuB3J7B5bdfg34atKqrBFuwm0m00jHAMzV76';
const STRIPE_KEY_STAGING = 'pk_test_51IaDh1HDRhnbjf6yNVgtYOtcMDWTFq708CzPReYcuRgicQvTKTbnQBNXRZtFIywuB3J7B5bdfg34atKqrBFuwm0m00jHAMzV76';
const STRIPE_KEY_CUSTOMER = 'pk_live_51IaDh1HDRhnbjf6yO0mYBXdfUxUHkBS8o4tsvT0TzvaGKCdswXc8nycQSI1IadRuFl63sXtmuYii4ykzpLhEDYJm0019j9pGgD';

export const LINK_CONFIG = env === ENV_ENUM.localhost ? {
  LINK_REDIRECT_AWS: LINK_REDIRECT_AWS_LOCAL,
  LINK_LOGOUT_AWS: LINK_LOGOUT_AWS_ALPHA,
  AWS_CONFIG: awsConfigAlpha,
  HOST_URL: HOST_URL_ALPHA,
  URL_GEOCODE: URL_GEOCODE_ALPHA,
  URL_SOCKET: URL_SOCKET_ALPHA,
  STRIPE_KEY: STRIPE_KEY_ALPHA,
} : env === ENV_ENUM.alpha ? {
  LINK_REDIRECT_AWS: LINK_REDIRECT_AWS_ALPHA,
  LINK_LOGOUT_AWS: LINK_LOGOUT_AWS_ALPHA,
  AWS_CONFIG: awsConfigAlpha,
  HOST_URL: HOST_URL_ALPHA,
  URL_GEOCODE: URL_GEOCODE_ALPHA,
  URL_SOCKET: URL_SOCKET_ALPHA,
  STRIPE_KEY: STRIPE_KEY_ALPHA,
} : env === ENV_ENUM.delta ? {
  LINK_REDIRECT_AWS: LINK_REDIRECT_AWS_DELTA,
  LINK_LOGOUT_AWS: LINK_LOGOUT_AWS_DELTA,
  AWS_CONFIG: awsConfigDelta,
  HOST_URL: HOST_URL_DELTA,
  URL_GEOCODE: URL_GEOCODE_DELTA,
  URL_SOCKET: URL_SOCKET_DELTA,
  STRIPE_KEY: STRIPE_KEY_DELTA,
} : env === ENV_ENUM.staging ? {
  LINK_REDIRECT_AWS: LINK_REDIRECT_AWS_STAGING,
  LINK_LOGOUT_AWS: LINK_LOGOUT_AWS_STAGING,
  AWS_CONFIG: awsConfigStaging,
  HOST_URL: HOST_URL_STAGING,
  URL_GEOCODE: URL_GEOCODE_STAGING,
  URL_SOCKET: URL_SOCKET_STAGING,
  STRIPE_KEY: STRIPE_KEY_STAGING,
} : env === ENV_ENUM.customer ? {
  LINK_REDIRECT_AWS: LINK_REDIRECT_AWS_CUSTOMER,
  LINK_LOGOUT_AWS: LINK_LOGOUT_AWS_CUSTOMER,
  AWS_CONFIG: awsConfigCustomer,
  HOST_URL: HOST_URL_CUSTOMER,
  URL_GEOCODE: URL_GEOCODE_CUSTOMER,
  URL_SOCKET: URL_SOCKET_CUSTOMER,
  STRIPE_KEY: STRIPE_KEY_CUSTOMER,
} : {
  LINK_REDIRECT_AWS: LINK_REDIRECT_AWS_CUSTOMER,
  LINK_LOGOUT_AWS: LINK_LOGOUT_AWS_CUSTOMER,
  AWS_CONFIG: awsConfigCustomer,
  HOST_URL: HOST_URL_CUSTOMER,
  URL_GEOCODE: URL_GEOCODE_CUSTOMER,
  URL_SOCKET: URL_SOCKET_CUSTOMER,
  STRIPE_KEY: STRIPE_KEY_STAGING,
};
