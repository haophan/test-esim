import { LINK_CONFIG } from './env';

export const updatedAwsConfig = () => ({
  ...LINK_CONFIG.AWS_CONFIG,
  ssr: true,
  oauth: {
    ...LINK_CONFIG.AWS_CONFIG.oauth,
    redirectSignIn: LINK_CONFIG.LINK_REDIRECT_AWS,
    redirectSignOut: LINK_CONFIG.LINK_REDIRECT_AWS,
  },
});
