import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import { Box } from "@mui/material";
import { useRouter } from "next/router";
import PriceTag from "../priceTag";
import ProductDescription from "../productDescription";

import mockBioheartDevice from "../../../public/images/shop/svg/mock-bioheart-device.svg";

import CustomButton from "../buttons/customButton";
import styles from "./styles.module.scss";
import IBriefProduct from "../../modals/briefProductInterface";
import IBundle from "../../modals/bundleInterface";

interface IProps {
  className: string;
  product: IBriefProduct;
  isHome?: boolean;
}

const BriefProduct = (props: IProps): JSX.Element => {
  const router = useRouter();

  const onLearnMore = () => {
    router.push(`/products/${props?.product.slug}`);
  };
  return (
    <Box className={classnames(styles.briefProduct, props.className)}>
      <Box className={styles.briefProductWrapper}>
        <Box className={styles.briefProductImage}>
          <img
            src={props.product.thumbnail?.url || mockBioheartDevice}
            width="100%"
            height="100%"
            alt=""
          />
        </Box>
        <Box className={styles.briefProductInfo}>
          <h2 className={styles.productName}>{props.product.title}</h2>
          <PriceTag
            className={styles.priceTag}
            originalPrice={props.product.price}
            discount={props.product?.discount}
            finalPrice={props.product?.final_price}
            mainHomeProduct
          />
          {props.product.bundles &&
            props.product.bundles.map((bundle: IBundle) => {
              return (
                <ProductDescription
                  key={bundle.id}
                  className={styles.productDescription}
                  title={props.product.title}
                  is_bundle={props.product.is_bundle}
                  bundle={bundle}
                  isHome={props.isHome}
                />
              );
            })}

          <CustomButton
            className={styles.learnMoreBtn}
            type="primary"
            onClick={onLearnMore}
          >
            Learn more
          </CustomButton>
        </Box>
      </Box>
    </Box>
  );
};

BriefProduct.defaultProps = {
  className: "",
};

BriefProduct.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default BriefProduct;
