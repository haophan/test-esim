import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import { Box } from "@mui/material";
import bioheartStrap from "../../../public/images/shop/svg/mock-bioheart-strap.svg";
import CustomButton from "../buttons/customButton";
import PriceTag from "../priceTag";
import styles from "./styles.module.scss";
import { NextRouter, useRouter } from "next/router";
import IBriefProductCard from "../../modals/briefProductCardInterface";
import IProductOption from "../../modals/productOptionInterface";
import IProductVariation from "../../modals/productVariationInterface";

interface IProps {
  className: string;
  product: IBriefProductCard;
}

const BriefProductCard = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();

  const onClickLearnMore = () => {
    router.push(`/products/${props.product.slug}`);
  };

  return (
    <Box className={classnames(styles.briefProductCard, props.className)}>
      <Box className={styles.briefProductCardImage}>
        <img src={props.product.thumbnail.url} height="100%" width="100%" alt="" />
        {props.product?.product_options &&
          props.product.product_options.map((productOption: IProductOption) => {
            if (productOption.product_variations) {
              return (
                <Box className={styles.variationArea} key={productOption.id}>
                  <p className={styles.variationText}>{`Available ${productOption.title}`}</p>
                  <Box className={styles.variations}>
                    {productOption.product_variations.map(
                      (variation: IProductVariation) => {
                        return <Box className={styles.variation} key={variation.id}>{variation.short_name}</Box>;
                      }
                    )}
                  </Box>
                </Box>
              );
            } else {
              return <></>;
            }
          })}
      </Box>
      <Box className={styles.briefProductCardInfo}>
        <h2 className={styles.productName}>{props.product.title}</h2>
        <p className={styles.productDescription}>{props.product.description}</p>
        <PriceTag
          originalPrice={props.product.price}
          discount={props.product?.discount}
          finalPrice={props.product?.final_price}
        />
      </Box>
      <Box className={styles.buttons}>
        <CustomButton
          className={styles.learnMoreBtn}
          type="primary"
          onClick={onClickLearnMore}
        >
          Learn more
        </CustomButton>
      </Box>
    </Box>
  );
};

BriefProductCard.defaultProps = {
  className: "",
  description:
    "Extra comfort strap for the Bioheart Heart Monitor.Stretchy material for you to wear.",
  originalPrice: 199,
  saleOffRate: 0,
};

BriefProductCard.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** product description */
  description: PropTypes.string,
  /** original price */
  originalPrice: PropTypes.number,
  /** sale off rate */
  saleOffRate: PropTypes.number,
};

export default BriefProductCard;
