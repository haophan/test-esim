import React from 'react';
import PropTypes from 'prop-types';
import { LoadingButton } from '@mui/lab';
import style from './style.module.scss';

const CustomButton = (props) => (
  <LoadingButton
    className={[
      style.customButton,
      props.type === 'primary' ? style.primaryType : undefined,
      props.type === 'outlined' ? style.outlinedType : undefined,
      props.type === 'outlinedDefault' ? style.outlinedDefaultType : undefined,
      props.type === 'outlinedPrimary' ? style.outlinedPrimaryType : undefined,
      props.type === 'danger' ? style.dangerType : undefined,
      props.type === 'outlinedDanger' ? style.outlinedDangerType : undefined,
      props.size === 'large' ? style.largeSize : undefined,
      props.size === 'small' ? style.smallSize : undefined,
      props.size === 'medium' ? style.mediumSize : undefined,
      props.isOnlyIcon ? style.onlyIcon : undefined,
      props.isSquare ? style.squareButton : undefined,
      props.className].join(' ')}
    component={props.component}
    disabled={props.disabled}
    disableElevation={false}
    disableFocusRipple={false}
    disableRipple={false}
    endIcon={props.endIcon}
    fullWidth={props.fullWidth}
    href={props.href}
    type={props.htmlType}
    size={props.size}
    startIcon={props.isOnlyIcon ? props.icon : props.startIcon}
    sx={props.sx}
    variant="text"
    loading={props.loading}
    loadingIndicator={props.loadingIndicator}
    loadingPosition={props.loadingPosition}
    onClick={props.onClick}
    classes={{
      // root: style.root,

      // text: props.variant === 'text' ? style.text : undefined,
      // textInherit: props.variant === 'text' && props.color === 'inherit' ? style.textInherit : undefined,
      // textPrimary: props.variant === 'text' && props.color === 'primary' ? style.textPrimary : undefined,
      // textSecondary: props.variant === 'text' && props.color === 'secondary' ? style.textSecondary : undefined,
      // textSuccess: props.variant === 'text' && props.color === 'success' ? style.textSuccess : undefined,
      // textError: props.variant === 'text' && props.color === 'error' ? style.textError : undefined,
      // textInfo: props.variant === 'text' && props.color === 'info' ? style.textInfo : undefined,
      // textWarning: props.variant === 'text' && props.color === 'warning' ? style.textWarning : undefined,

      // outlined: props.variant === 'outlined' ? style.outlined : undefined,
      // outlinedInherit: props.variant === 'outlined' && props.color === 'inherit' ? style.outlinedInherit : undefined,
      // outlinedPrimary: props.variant === 'outlined' && props.color === 'primary' ? style.outlinedPrimary : undefined,
      // outlinedSecondary: props.variant === 'outlined' && props.color === 'secondary' ? style.outlinedSecondary : undefined,
      // outlinedSuccess: props.variant === 'outlined' && props.color === 'success' ? style.outlinedSuccess : undefined,
      // outlinedError: props.variant === 'outlined' && props.color === 'error' ? style.outlinedError : undefined,
      // outlinedInfo: props.variant === 'outlined' && props.color === 'info' ? style.outlinedInfo : undefined,
      // outlinedWarning: props.variant === 'outlined' && props.color === 'warning' ? style.outlinedWarning : undefined,

      // contained: props.variant === 'contained' ? style.contained : undefined,
      // containedInherit: props.variant === 'contained' && props.color === 'inherit' ? style.containedInherit : undefined,
      // containedPrimary: props.variant === 'contained' && props.color === 'primary' ? style.containedPrimary : undefined,
      // containedSecondary: props.variant === 'contained' && props.color === 'secondary' ? style.containedSecondary : undefined,
      // containedSuccess: props.variant === 'contained' && props.color === 'success' ? style.containedSuccess : undefined,
      // containedError: props.variant === 'contained' && props.color === 'error' ? style.containedError : undefined,
      // containedInfo: props.variant === 'contained' && props.color === 'info' ? style.containedInfo : undefined,
      // containedWarning: props.variant === 'contained' && props.color === 'warning' ? style.containedWarning : undefined,

      // disableElevation: props.disableElevation ? style.disableElevation : undefined,
      // disabled: props.disabled ? style.disabled : undefined,
      // colorInherit: props.color === 'inherit' ? style.colorInherit : undefined,

      // textSizeSmall: props.size === 'small' && props.variant === 'text' ? style.textSizeSmall : undefined,
      // textSizeMedium: props.size === 'medium' && props.variant === 'text' ? style.textSizeMedium : undefined,
      // textSizeLarge: props.size === 'large' && props.variant === 'text' ? style.textSizeLarge : undefined,

      // outlinedSizeSmall: props.size === 'small' && props.variant === 'outlined' ? style.outlinedSizeSmall : undefined,
      // outlinedSizeMedium: props.size === 'medium' && props.variant === 'outlined' ? style.outlinedSizeMedium : undefined,
      // outlinedSizeLarge: props.size === 'large' && props.variant === 'outlined' ? style.outlinedSizeLarge : undefined,

      // containedSizeSmall: props.size === 'small' && props.variant === 'contained' ? style.containedSizeSmall : undefined,
      // containedSizeMedium: props.size === 'medium' && props.variant === 'contained' ? style.containedSizeMedium : undefined,
      // containedSizeLarge: props.size === 'large' && props.variant === 'contained' ? style.containedSizeLarge : undefined,

      // sizeSmall: props.size === 'small' ? style.sizeSmall : undefined,
      // sizeMedium: props.size === 'medium' ? style.sizeMedium : undefined,
      // sizeLarge: props.size === 'large' ? style.sizeLarge : undefined,

      // fullWidth: props.fullWidth ? style.fullWidth : undefined,
      // startIcon: props.startIcon ? style.startIcon : undefined,
      // endIcon: props.endIcon ? style.endIcon : undefined,

      // iconSizeSmall: props.size === 'small' ? style.iconSizeSmall : undefined,
      // iconSizeMedium: props.size === 'medium' ? style.iconSizeMedium : undefined,
      // iconSizeLarge: props.size === 'large' ? style.iconSizeLarge : undefined,

      loading: props.loading ? style.loading : undefined,

      // loadingIndicator: props.loadingIndicator ? style.loadingIndicator : undefined,
      // loadingIndicatorCenter: props.loadingPosition === 'center' ? style.loadingIndicatorCenter : undefined,
      // loadingIndicatorEnd: props.loadingPosition === 'end' ? style.loadingIndicatorEnd : undefined,
      // loadingIndicatorStart: props.loadingPosition === 'start' ? style.loadingIndicatorStart : undefined,

      // endIconLoadingEnd: props.loading && props.loadingPosition === 'end' ? style.endIconLoadingEnd : undefined,
      // startIconLoadingStart: props.loading && props.loadingPosition === 'start' ? style.startIconLoadingStart : undefined,
    }}
  >
    {props.children}
  </LoadingButton>
);

CustomButton.defaultProps = {
  className: '',
  children: undefined,
  type: 'primary',
  component: undefined,
  disabled: false,
  isOnlyIcon: false,
  isSquare: false,
  endIcon: undefined,
  icon: undefined,
  fullWidth: false,
  href: undefined,
  htmlType: 'html',
  size: 'large',
  startIcon: undefined,
  sx: undefined,
  loading: false,
  loadingIndicator: undefined,
  loadingPosition: 'center',
  onClick: () => {},
};

CustomButton.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  type: PropTypes.oneOf(['primary', 'outlined', 'outlinedDefault', 'outlinedPrimary', 'danger', 'outlinedDanger']),
  component: PropTypes.elementType,
  disabled: PropTypes.bool,
  isOnlyIcon: PropTypes.bool,
  isSquare: PropTypes.bool,
  endIcon: PropTypes.node,
  icon: PropTypes.node,
  fullWidth: PropTypes.bool,
  href: PropTypes.string,
  htmlType: PropTypes.oneOf(['submit', 'html']),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  startIcon: PropTypes.node,
  sx: PropTypes.oneOf([PropTypes.arrayOf(PropTypes.oneOf([PropTypes.func, PropTypes.shape(), PropTypes.bool])), PropTypes.func, PropTypes.shape()]),
  loading: PropTypes.bool,
  loadingIndicator: PropTypes.node,
  loadingPosition: PropTypes.oneOf(['start', 'end', 'center']),
  onClick: PropTypes.func,
};

export default CustomButton;
