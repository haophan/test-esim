import classnames from "classnames";
import PropTypes from "prop-types";
import React, { useEffect, useRef, useState } from "react";
import { IconButton } from "@mui/material";
import { Button, Divider, Popover } from 'antd';
import Image from "next/image";
import { useSelector } from "react-redux";
import { NextRouter, useRouter } from "next/router";
import shoppingCartIcon from '../../../public/images/shop/svg/shopping-cart.svg';
import styles from "./styles.module.scss";
import _ from "lodash";

interface IProps {
  className: string;
  isOpenCartPanel: boolean;
  togglePanel: (event: React.KeyboardEvent | React.MouseEvent) => void;
}

const CartInfoPopup = (props: IProps): JSX.Element => {
  const timeOutRef = useRef<any>(undefined)

  const cart: any = useSelector<any>((state) => state.cart);

  const [isOpenCartPopup, setOpenCartPopup] = useState<boolean>(false);
  const toggleCartPopup = (event: React.KeyboardEvent | React.MouseEvent) => {
    setOpenCartPopup(false)
    props.togglePanel(event);
  };

  const totalItems = cart.products.reduce(
    (accumulator: any, currentValue: any) =>
      accumulator + currentValue.quantity,
    0
  );

  const totalRef = useRef<number>(totalItems)

  useEffect(() => {
    if (totalItems > totalRef.current && !props.isOpenCartPanel) {
      setOpenCartPopup(true);
      if (timeOutRef.current) {
        clearTimeout(timeOutRef.current)
      }
      timeOutRef.current = setTimeout(() => {
        setOpenCartPopup(false);
      }, 3000)
    }
    totalRef.current = totalItems;
  }, [totalItems, props.isOpenCartPanel])

  const popoverContent = (
    <div className={styles.cartInfoMenu}>
      <div className={styles.cartInfoTitle}>Item added to cart</div>
      {_.map(cart?.products || [], (item, idx) => (
        <div className={styles.cartInfoItem} key={`cart-product-${idx}`}>
          <div className={styles.cartImageDisplay}>
            <div className={styles.cartImageContainer}><img src={item?.product?.thumbnail?.url} alt=""></img></div>
            <div className={styles.cartQuantity}>{item?.quantity}</div>
          </div>
          <div>
            <div className={styles.productTitle}>{item?.product?.title}</div>
            <div className={styles.productSize} >{item?.product?.optionType ? `Strap size: ${item?.product?.selectedOptions?.variation?.fullname}` : ''}</div>
          </div>
        </div>
      ))}
      <Divider className={styles.divider} />
      <div className={styles.buttonContainer}>
        <Button onClick={toggleCartPopup} className={styles.cartButton}>
          View cart
        </Button>
      </div>
    </div>
  );
  return (
    <Popover
      open={isOpenCartPopup}
      placement="bottomRight"
      content={popoverContent}
      className={styles.cartInfoPopover}
      trigger="click"
      getPopupContainer={trigger => trigger}
    >
      <IconButton
        onClick={toggleCartPopup}
        className={classnames(styles.cartBtn, props.className)}
      >
        <Image src={shoppingCartIcon} width={24} height={24} alt="cart-icon" />
        {cart.products.length !== 0 && (
          <div className={styles.inCartNumber}>
            {cart.products.length}
          </div>
        )}
      </IconButton>
    </Popover>

  );
};

CartInfoPopup.defaultProps = {
  className: "",
  isOpenCartPanel: false,
  togglePanel: () => { },
};

CartInfoPopup.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** is drawer open */
  isOpenCartPanel: PropTypes.bool,
  /** on close event */
  togglePanel: PropTypes.func,
};

export default CartInfoPopup;
