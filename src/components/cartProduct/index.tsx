import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import { Box } from "@mui/material";
import { useDispatch } from "react-redux";
import mockBioheartDevice from "../../../public/images/shop/svg/mock-bioheart-device.svg";
import {
  decreaseQuantity,
  increaseQuantity,
  removeProduct,
} from "../../redux/reducers/cart";

import ICartProduct from "../../modals/cartProductInterface";
import CustomButton from "../buttons/customButton";
import SetQuantity from "../setQuantity";
import styles from "./styles.module.scss";

interface IProps {
  className: string;
  quantity: number;
  product: ICartProduct;
}

const CartProduct = (props: IProps): JSX.Element => {
  const dispatch = useDispatch();

  const onRemove = () => {
    dispatch(
      removeProduct({
        id: props.product.id,
        optionId: props.product.selectedOptions
          ? props.product.selectedOptions.id
          : undefined,
        variationId: props.product.selectedOptions
          ? props.product.selectedOptions.variation.id
          : undefined,
      })
    );
  };

  const onIncrease = () => {
    dispatch(
      increaseQuantity({
        id: props.product.id,
        optionId: props.product.selectedOptions
          ? props.product.selectedOptions.id
          : undefined,
        variationId: props.product.selectedOptions
          ? props.product.selectedOptions.variation.id
          : undefined,
      })
    );
  };

  const onDecrease = () => {
    if (props.quantity === 1) return;
    dispatch(
      decreaseQuantity({
        id: props.product.id,
        optionId: props.product.selectedOptions
          ? props.product.selectedOptions.id
          : undefined,
        variationId: props.product.selectedOptions
          ? props.product.selectedOptions.variation.id
          : undefined,
      })
    );
  };

  const onChangeQuantity = (isAdd = false) => {
    if (isAdd) {
      onIncrease();
    } else {
      onDecrease();
    }
  };
  // cart data will be stored in redux
  return (
    <Box className={classnames(styles.cartProduct, props.className)}>
      <Box className={styles.cartProductImage}>
        <img
          src={
            props.product.thumbnail
              ? props.product.thumbnail.url
              : mockBioheartDevice
          }
          width="100%"
          height="100%"
          alt=""
        />
      </Box>
      <Box className={styles.cartProductInfo}>
        <Box className={styles.cartProductInfoZone}>
          <p className={styles.productDescription}>{props.product.title}</p>
          {/* <PriceTag
            className={styles.productPrice}
            originalPrice={props.product.price}
            discount={props.product?.discount}
            finalPrice={props.product?.final_price}
          /> */}
          <h4 className={styles.productPrice}>{`$${props.product.price}`}</h4>
          {props.product.selectedOptions &&
            props.product.selectedOptions.variation && (
              <p
                className={styles.productSize}
              >{`Strap size: ${props.product.selectedOptions.variation.fullname}`}</p>
            )}
        </Box>
        <Box
          className={classnames(
            styles.cartProductQuantity,
            props.product.is_bundle ? styles.isBunbleProduct : ""
          )}
        >
          {!props.product.buy_limit && (
            <SetQuantity
              className={classnames(styles.setQuantity)}
              quantity={props.quantity}
              onChangeQuantity={onChangeQuantity}
            />
          )}
          <CustomButton
            className={styles.removeBtn}
            type="text"
            onClick={onRemove}
          >
            Remove
          </CustomButton>
        </Box>
      </Box>
    </Box>
  );
};

CartProduct.defaultProps = {
  className: "",
};

CartProduct.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default CartProduct;
