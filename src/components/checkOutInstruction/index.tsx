import classnames from "classnames";
import React, { useEffect, useRef } from "react";

import { Box } from "@mui/material";
import instructionImage from "../../../public/images/shop/svg/checkout-instruction.svg";
import CustomButton from "../buttons/customButton";

import styles from "./styles.module.scss";
import { NextRouter, useRouter } from "next/router";

interface IProps {
  className?: string;
}

const CheckOutInstruction = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();
  const isBuyNowRef = useRef<boolean>(false);

  const onCreateAccount = (): void => {
    router.push("/signup");
  };

  const onLogin = (): void => {
    router.push(`/login${isBuyNowRef.current ? '?fromBuynowCheckout=true' : '?fromBuynowCheckout=false'}`);
  };

  useEffect(() => {
    if (router.isReady && router.query) {
      isBuyNowRef.current = router.query.buyNow === "true";
    }
  }, [router.isReady]);

  return (
    <Box className={classnames(styles.checkOutInstruction, props.className)}>
      <Box className={styles.instructionImage}>
        <img src={instructionImage} width="100%" height="100%" alt="" />
      </Box>
      <Box className={styles.instruction}>
        <h4 className={styles.instructionTitle}>Please sign in to checkout</h4>
        <p className={styles.instructionDescription}>
          Sign in with a Biotricity account to checkout and track status of your
          order.
        </p>
      </Box>
      <Box className={styles.actions}>
        <CustomButton
          className={styles.actionBtn}
          type="outlined"
          fullWidth
          onClick={onCreateAccount}
        >
          Create an account
        </CustomButton>
        <CustomButton
          className={styles.actionBtn}
          type="text"
          fullWidth
          onClick={onLogin}
        >
          I already have one
        </CustomButton>
      </Box>
    </Box>
  );
};

export default CheckOutInstruction;
