import classnames from "classnames";
import PropTypes from "prop-types";
import React, { useState } from "react";

import { Box } from "@mui/material";
import CustomButton from "../../buttons/customButton";
import CheckoutProduct from "../../checkoutProduct";
import CustomInput from "../../inputs/customInput";

import styles from "./styles.module.scss";
import { useSelector } from "react-redux";
import ICartInformation from "../../../modals/cartInformationInterface";

interface IProps {
  className: string;
  cart: {
    products: ICartInformation[];
  };
  onSetGiftCode: (gc: string | undefined) => void;
}

const CartInfoSection = (props: IProps): JSX.Element => {
  // const [giftCode, setGiftCode] = useState<string>();

  // const [error, setError] = useState<string>();

  const totalItems = props.cart.products.length !== 0 ? props.cart.products.reduce(
    (accumulator: number, currentValue: ICartInformation) =>
      accumulator + currentValue.quantity,
    0
  ) : 0;
  const totalPrice = props.cart.products.length !== 0 ? props.cart.products.reduce(
    (accumulator: number, currentValue: ICartInformation) =>
      accumulator + currentValue.quantity * currentValue.product?.price,
    0
  ): 0;
  const deliveryFee = 0;
  const inTotalPrice = totalPrice + deliveryFee;

  return (
    <Box className={classnames(styles.cartInfo, props.className)}>
      <h4 className={styles.cartSummary}>
        Items detail
        <span>
          <p className={styles.cartQuantity}>{`(${totalItems} item${
            totalItems === 1 ? "" : "s"
          })`}</p>
        </span>
      </h4>
      {props.cart.products && props.cart.products.length !== 0 && (
        <Box className={styles.items}>
          {props.cart.products.map((product: ICartInformation, index: number) => (
            <CheckoutProduct key={index} product={product} />
          ))}
        </Box>
      )}
      {/* <CustomInput
        containerClassName={styles.couponInput}
        placeholder="Gift card or discount code"
        name="giftCode"
        value={giftCode}
        onChange={onChangeInput}
        endAdornment={
          <CustomButton
            type="text"
            disabled={!giftCode}
            className={styles.applyBtn}
            onClick={onClickApply}
          >
            Apply
          </CustomButton>
        }
      /> */}
      <Box className={styles.paymentInfo}>
        <Box className={styles.row}>
          <p className={styles.title}>{`Subtotal (${totalItems} item${
            totalItems === 1 ? "" : "s"
          })`}</p>
          <p className={styles.price}>{`$${totalPrice}`}</p>
        </Box>
        <Box className={styles.row}>
          <p className={styles.title}>Shipping</p>
          <p className={styles.price}>
            {deliveryFee ? `$${deliveryFee}` : "Free"}
          </p>
        </Box>
      </Box>
      <Box className={styles.totalInfo}>
        <Box className={styles.row}>
          <p className={classnames(styles.title, styles.totalTitle)}>
            In total
          </p>
          <p
            className={classnames(styles.price, styles.totalPrice)}
          >{`$${inTotalPrice}`}</p>
        </Box>
      </Box>
    </Box>
  );
};

CartInfoSection.defaultProps = {
  className: "",
};

CartInfoSection.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default CartInfoSection;
