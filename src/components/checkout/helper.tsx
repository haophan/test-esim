//@ts-nocheck
import Stripe from "stripe";
import { loadStripe } from "@stripe/stripe-js";
import ICartInformation from "../../modals/cartInformationInterface";

const stripeSecret = new Stripe("sk_test_4wmHw3j7xVkoN9L3awz5jk1b00AtVIIOYP", {
  // apiVersion: "2022-11-15",
});

// const stripe = new Stripe('sk_test_...', {
//   apiVersion: '2020-08-27',
// });
export const stripeHandler = async (
  shippingInfo: any,
  cart: { products: ICartInformation[] },
  isBuyNow: boolean,
  orderId?: number
) => {
  try {
    // get lineItems
    // get subcriptions
    // check mode
    let mode = "payment";
    let lineItems: Array<{
      name: string;
      amount: number;
      currency: string;
      quantity: number;
      images?: any;
    }> = [];
    let subscriptionData: Array<{ plan: string }> = [];
    cart.products.forEach((product: ICartInformation) => {
      if (product.product.is_bundle) {
        mode = "subscription";
      }
      if (product.product.stripe_subscription_id) {
        subscriptionData.push({
          plan: product.product.stripe_subscription_id,
        });
      }
      lineItems.push({
        name: product.product.title,
        amount: product.product.price * 100,
        currency: product.product.currency,
        quantity: product.quantity,
        // images: product.product.thumbnail?.url
        //   ? [product.product.thumbnail.url]
        //   : [],
      });
    });
    const paymentInfo = {
      mode: mode,
      payment_method_types: ["card"],
      line_items: lineItems,
      success_url: `http://localhost:3000/payment-result?status=SUCCESS${
        isBuyNow ? "&fromBuyNow=true" : ""
      }${
        orderId ? `&orderId=${orderId}` : ""
      }&session_id={CHECKOUT_SESSION_ID}`,
      cancel_url: `http://localhost:3000/payment-result?status=FAILED${
        isBuyNow ? "&fromBuyNow=true" : ""
      }`,
      customer_email: shippingInfo.email,
      subscription_data: {
        items: subscriptionData,
      },
      metadata: { orderId: orderId },
    } as any;
    const paymentSession = await stripeSecret.checkout.sessions.create(
      paymentInfo
    );

    const stripePromise = await loadStripe(
      "pk_test_c2x5bDNjWIwzyh39sJVL95P400Kosqyefa"
    );
    if (stripePromise) { 
      await stripePromise.redirectToCheckout({ sessionId: paymentSession.id });
    }
  } catch (error) {
    // console.log(error);
  }
};
