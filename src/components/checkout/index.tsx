import classnames from "classnames";
import PropTypes from "prop-types";
import React, { useEffect, useRef, useState } from "react";

import { Box, IconButton } from "@mui/material";
import { NextRouter, useRouter } from "next/router";
import CartInfoSection from "./cartInfoSection";
import ShippingInfoSection from "./shippingInfoSection";

import styles from "./styles.module.scss";
import createOrder from "../../rest/post/createOrder";
import { useSelector } from "react-redux";
import _ from "lodash";
import { stripeHandler } from "./helper";
import ICartInformation from "../../modals/cartInformationInterface";
import backIcon from "../../../public/images/shop/svg/black-back-arrow-icon.svg";
import { auth } from "../../utils/auth";

interface IProps {
  className: string;
}

const Checkout = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();

  const [isBuyNow, setBuyNow] = useState<boolean>(false);

  const loginData = auth.getLoginData();

  const cartProducts: { products: ICartInformation[] } = useSelector(
    (state: any) => state.cart
  );
  const buyNowProduct: ICartInformation = useSelector(
    (state: any) => state.buyNow
  );

  const giftCodeRef = useRef<string | undefined>("");

  const onSetGiftCode = (gc: string | undefined) => {
    giftCodeRef.current = gc;
  };

  const onBack = () => {
    router.back();
  };

  const onPayNow = async (shippingInfo: any, isBuyNow: boolean) => {
    if (!loginData.accessToken) {
      router.push('/login');
    }
    const result = await createOrder(
      shippingInfo,
      isBuyNow ? { products: [buyNowProduct] } : cartProducts,
    );
    if (result.isSuccess) {
      await stripeHandler(
        shippingInfo,
        isBuyNow ? { products: [buyNowProduct] } : cartProducts,
        isBuyNow,
        result.data?.orderId
      );
    }
    
  };

  useEffect(() => {
    if (router.isReady) {
      if (router.query.buyNow) {
        if (_.isEmpty(buyNowProduct)) {
          router.push("/home");
          return;
        }
        setBuyNow(router.query.buyNow === "true");
      } else {
        if (cartProducts.products.length === 0) {
          router.push("/home");
        }
      }
    }
  }, [router.isReady]);

  return (
    <Box className={classnames(styles.checkoutPage, props.className)}>
      <Box className={styles.checkoutHeader}>
        <IconButton classes={{ root: styles.backArrowBtn }} onClick={onBack}>
          <img src={backIcon} alt="" />
        </IconButton>
        <h4 className={styles.title}>Check out</h4>
      </Box>
      <Box className={styles.checkoutBody}>
        <CartInfoSection
          className={styles.cartInfo}
          onSetGiftCode={onSetGiftCode}
          cart={isBuyNow ? { products: [buyNowProduct] } : cartProducts}
        />
        <ShippingInfoSection
          className={styles.shippingInfo}
          isBuyNow={isBuyNow}
          onPayNow={onPayNow}
        />
      </Box>
    </Box>
  );
};

Checkout.defaultProps = {
  className: "",
};

Checkout.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default Checkout;
