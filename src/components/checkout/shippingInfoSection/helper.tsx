export const isDisabledPayNowBtn = (state: any): boolean => {
  return !state.firstName || !state.lastName || !state.email || !state.country || !state.address || !state.city || !state.state || !state.zip || !state.phoneNumber
}