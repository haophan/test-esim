import classnames from "classnames";
import React, { useState } from "react";

import { Box } from "@mui/material";
import CustomButton from "../../buttons/customButton";
import CustomInput from "../../inputs/customInput";
import NormalInput from "../../inputs/normalInput";
import NormalSelect from "../../normalSelect";

import styles from "./styles.module.scss";
import { COUNTRY_DATA } from "../../../utils/countryData";
import { getFullCode, mappingState } from "../../../utils/stateData";
import PhoneWithFlagInput from "../../phoneNumberWithFlag";
import {
  checkingDivisionZipCode,
  isValidEmail,
  isValidPhone,
} from "../../../utils";

import selectArrowIcon from "../../../../public/images/shop/svg/select-arrow-icon.svg";
import { isDisabledPayNowBtn } from "./helper";
import Image from "next/image";

interface IState {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string;
  email: string;
  city: string;
  state: string;
  secondaryAddress?: string;
  country: string;
  zip: string;
}

interface IErrorState {
  emailError?: string;
  phoneNumberError?: string;
  zipError?: string;
}
interface IProps {
  className: string;
  isBuyNow: boolean;
  onPayNow: (shippingInfo: IState, isBuyNow: boolean) => void;
}

const ShippingInfoSection = (props: IProps): JSX.Element => {
  const [state, setState] = useState<IState>({
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    country: "US",
    address: "",
    secondaryAddress: "",
    city: "",
    state: "",
    zip: "",
  });

  const [errorState, setErrorState] = useState<IErrorState>({
    emailError: "",
    phoneNumberError: "",
    zipError: "",
  });

  const [loading, setLoading] = useState<boolean>(false);

  const isError = () => {
    return (
      !!errorState.emailError ||
      !!errorState.phoneNumberError ||
      !!errorState.zipError
    );
  };

  const resetError = () => {
    setErrorState({
      emailError: "",
      phoneNumberError: "",
      zipError: "",
    });
  };

  const onCheckIsValidZipcode = async (state: string, zipcode: string) => {
    const result = await checkingDivisionZipCode(state, zipcode);
    return result;
  };

  const onChangeInput = (name: string, value: string) => {
    if (isError()) {
      resetError();
    }
    if (name === "country") {
      setState({
        ...state,
        address: "",
        city: "",
        state: "",
        zip: "",
        phoneNumber: "",
      });
    }
    setState({ ...state, [name]: value });
  };

  const onChangeAddress = (name: string, address: any) => {
    if (isError()) {
      resetError();
    }
    if (name === "address") {
      setState({
        ...state,
        address: address.address,
        city: address.city,
        state: address.state.code,
        zip: address.zip,
      });
    }
  };

  const onChangeSelect = (
    name: string,
    value: { label: string; value: string }
  ) => {
    if (isError()) {
      resetError();
    }
    if (name === "country") {
      setState({
        ...state,
        [name]: value.value,
        address: "",
        city: "",
        state: "",
        zip: "",
      });
      return;
    }
    setState({ ...state, [name]: value.value });
  };

  const onPayNow = async () => {
    setLoading(true);
    if (!isValidEmail(state.email)) {
      setErrorState({ ...errorState, emailError: "Invalid email." });
      setLoading(false);
      return;
    }
    if (["US", "CA"].includes(state.country)) {
      const stateFullCode = getFullCode(state.country, state.state);
      if (!stateFullCode) {
        setLoading(false);
        return;
      } else {
        const isValidZip = await onCheckIsValidZipcode(
          stateFullCode,
          state.zip
        );
        if (!isValidZip.isSuccess || !isValidZip.isValid) {
          setErrorState({ ...errorState, zipError: "Invalid Zip code." });
          setLoading(false);
          return;
        }
      }
    }

    if (!isValidPhone(state.phoneNumber.toString(), state.country)) {
      setErrorState({
        ...errorState,
        phoneNumberError: "Invalid phone number.",
      });
      setLoading(false);
      return;
    }

    setLoading(false);

    props.onPayNow(state, props.isBuyNow);
  };

  return (
    <Box className={classnames(styles.shippingInfo, props.className)}>
      <Box className={styles.instruction}>
        <h4 className={styles.instructionTitle}>Shipping address</h4>
        <p className={styles.instructionDescription}>
          Where we should deliver your order?
        </p>
      </Box>
      <Box className={styles.shippingInfoZone}>
        <CustomInput
          containerClassName={styles.mt16}
          isRequire
          title="First name"
          value={state.firstName}
          name="firstName"
          placeholder="Enter first name"
          onChange={onChangeInput}
        />
        <CustomInput
          containerClassName={styles.mt16}
          isRequire
          title="Last name"
          value={state.lastName}
          name="lastName"
          placeholder="Enter last name"
          onChange={onChangeInput}
        />
        <CustomInput
          containerClassName={styles.mt16}
          isRequire
          title="Email"
          value={state.email}
          name="email"
          placeholder="Enter email"
          onChange={onChangeInput}
          isError={!!errorState.emailError}
          errorMessage={errorState.emailError}
        />
        <NormalSelect
          name="country"
          title="Country"
          isRequire
          className={classnames(styles.mt16)}
          placeholder="Select..."
          data={COUNTRY_DATA}
          isObject
          onChange={onChangeSelect}
          suffixIcon={<Image src={selectArrowIcon} width={24} height={24} />}
          value={state.country}
        />
        <NormalInput
          name="address"
          isRequire
          className={classnames(styles.mt16, styles.normalInput)}
          title="Address"
          placeholder="Enter address"
          type="search-address"
          value={state.address}
          country={state.country || "US"}
          onChangeAddress={onChangeAddress}
        />
        <CustomInput
          name="secondaryAddress"
          containerClassName={styles.mt16}
          className={classnames(styles.normalInput)}
          title="Secondary address"
          placeholder="Enter address"
          value={state.secondaryAddress}
          onChange={onChangeInput}
          moreTitle="(Optional)"
        />
        <NormalInput
          name="city"
          isRequire
          className={classnames(styles.mt16, styles.normalInput)}
          title="City"
          placeholder="Enter city"
          type="search-address"
          value={state.city}
          country={state.country || "US"}
          onChangeAddress={onChangeAddress}
          isSearchCity
        />
        <Box className={classnames(styles.flexRow, styles.mt16)}>
          {["US", "CA"].includes(state.country) ? (
            <NormalSelect
              className={classnames(styles.normalInput, styles.insideInput)}
              title="State"
              isRequire
              name="state"
              placeholder="Select state"
              isObject
              data={mappingState(state.country)}
              value={state.state}
              suffixIcon={
                <Image src={selectArrowIcon} width={24} height={24} />
              }
            />
          ) : (
            <CustomInput
              containerClassName={styles.insideInput}
              title="State"
              isRequire
              name="state"
              placeholder="Enter state"
              value={state.state}
              onChange={onChangeInput}
            />
          )}
          <CustomInput
            containerClassName={styles.insideInput}
            title="Zip code"
            name="zip"
            isRequire
            placeholder="Enter zip code"
            value={state.zip}
            onChange={onChangeInput}
            errorMessage={errorState.zipError}
            isError={!!errorState.zipError}
          />
        </Box>
        <PhoneWithFlagInput
          className={classnames(styles.mt16, styles.normalInput)}
          label="Phone number"
          isRequire
          name="phoneNumber"
          placeholder="Enter your number"
          value={state.phoneNumber}
          isPhone
          countryObject={COUNTRY_DATA.find(
            (country: any) => country.value === state.country
          )}
          onChange={onChangeInput}
          onBlur={() => {}}
          getInputRef={() => {}}
          isError={!!errorState.phoneNumberError}
          errorMessage={errorState.phoneNumberError}
        />
        <CustomButton
          className={styles.payNowBtn}
          fullWidth
          onClick={onPayNow}
          disabled={isDisabledPayNowBtn(state) || isError()}
          loading={loading}
        >
          Pay now
        </CustomButton>
      </Box>
    </Box>
  );
};

export default ShippingInfoSection;
