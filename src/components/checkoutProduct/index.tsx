import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import { Box } from "@mui/material";
import mockBioheartDevice from "../../../public/images/shop/svg/mock-bioheart-device.svg";
import styles from "./styles.module.scss";
import ICartInformation from "../../modals/cartInformationInterface";

interface IProps {
  className?: string;
  product: ICartInformation;
}

const CheckoutProduct = (props: IProps): JSX.Element => {
  return (
    <Box className={classnames(styles.checkoutProduct, props.className)}>
      <Box className={styles.checkoutProductInfoWrapper}>
        <Box className={styles.checkoutProductInfoImage}>
          <img src={props.product.product.thumbnail?.url || mockBioheartDevice} width="100%" height="100%" alt="" />
        </Box>
        <Box className={styles.checkoutProductInfoZone}>
          <p className={styles.productName}>{props.product.product.title}</p>
          {props.product.product?.selectedOptions && (
            <p className={styles.variation}>{`Strap size: ${props.product.product.selectedOptions.variation.fullname}`}</p>
          )}
          <p
            className={styles.productQuantity}
          >{`x ${props.product.quantity}`}</p>
        </Box>
      </Box>
      <p
        className={styles.productPrice}
      >{`$ ${props.product.product.price}`}</p>
    </Box>
  );
};

export default CheckoutProduct;
