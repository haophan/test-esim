import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import { Box } from "@mui/material";
import styles from "./styles.module.scss";
import checkoutFailed from "../../../../public/images/shop/svg/checkout-successful.svg";
import CustomButton from "../../buttons/customButton";
import { NextRouter, useRouter } from "next/router";

interface IProps {
  className: string;
  failedFromBuyNow?: boolean;
}

const CheckoutFailed = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();

  const onTryAgain = (): void => {
    if (props.failedFromBuyNow) {
      router.push("/checkout?buyNow=true");
    } else {
      router.push("/checkout");
    }
  };

  const onBackToHome = (): void => {
    router.push("/home");
  };
  return (
    <Box className={classnames(styles.checkoutFailed, props.className)}>
      <Box className={styles.checkoutFailedImage}>
        <img src={checkoutFailed} width="100%" height="100%" alt="" />
      </Box>
      <Box className={styles.checkoutFailedInstruction}>
        <h4 className={styles.checkoutFailedInstructionTitle}>
          Payment failed!
        </h4>
        <p className={styles.checkoutFailedInstructionDescription}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna{" "}
        </p>
      </Box>
      <Box className={styles.actions}>
        <CustomButton
          className={styles.actionBtn}
          type="outlined"
          fullWidth
          onClick={onTryAgain}
        >
          Try again
        </CustomButton>
        <CustomButton
          className={styles.actionBtn}
          type="text"
          fullWidth
          onClick={onBackToHome}
        >
          Back to home page
        </CustomButton>
      </Box>
    </Box>
  );
};
CheckoutFailed.defaultProps = {
  className: "",
};

CheckoutFailed.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default CheckoutFailed;
