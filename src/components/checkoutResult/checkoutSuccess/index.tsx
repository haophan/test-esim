import classnames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { Box } from '@mui/material';
import styles from './styles.module.scss';
import checkoutSuccess from '../../../../public/images/shop/svg/checkout-successful.svg';
import CustomButton from '../../buttons/customButton';
import { NextRouter, useRouter } from 'next/router';

interface IProps {
  className: string,
  orderId: number,
}

const CheckoutSuccess = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();

  const onViewOrder = () => {

  }

  const onContinueShoping = () => {
    router.push('/home/');
  }
  return (
    <Box className={classnames(styles.checkoutSuccess, props.className)}>
      <Box className={styles.checkoutSuccessImage}>
        <img src={checkoutSuccess} width="100%" height="100%" alt="" />
      </Box>
      <Box className={styles.checkoutSuccessInstruction}>
        <h4 className={styles.checkoutSuccessInstructionTitle}>Thank you for your order!</h4>
        <p className={styles.checkoutSuccessInstructionDescription}>{`Order number: #${props.orderId}`}</p>
      </Box>
      <Box className={styles.actions}>
        <CustomButton
          className={styles.actionBtn}
          type="outlined"
          fullWidth
          onClick={onViewOrder}
        >
          View your order
        </CustomButton>
        <CustomButton
          className={styles.actionBtn}
          type="text"
          fullWidth
          onClick={onContinueShoping}
        >
          Continue shopping
        </CustomButton>
      </Box>
    </Box>
  );
};
CheckoutSuccess.defaultProps = {
  className: '',
};

CheckoutSuccess.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default CheckoutSuccess;
