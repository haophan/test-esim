import React, { useState } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import { Box } from '@mui/material';
import styles from './styles.module.scss';
import CheckoutSuccess from './checkoutSuccess';
import CheckoutFailed from './checkoutFailed';

interface IProps {
  className: string,
  view: string,
  orderId: number,
  failedFromBuyNow: boolean,
}

const CheckoutResult = (props: IProps): JSX.Element => {
  return (
    <Box className={classnames(styles.checkoutResult, props.className)}>
      { props.view === 'SUCCESS' && <CheckoutSuccess orderId={props.orderId} /> }
      { props.view === 'FAILED' && <CheckoutFailed failedFromBuyNow={props.failedFromBuyNow} />}
    </Box>
  );
};

CheckoutResult.defaultProps = {
  className: '',
  view: '',
};

CheckoutResult.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** current view */
  view: PropTypes.oneOf(['SUCCESS', 'FAILED']),
};

export default CheckoutResult;
