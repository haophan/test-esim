import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

const CountDownComponent = (props) => {
  const [currentCount, setCurrentCount] = useState(props.seconds);

  useEffect(() => {
    const interval = setInterval(() => {
      if (currentCount === 1) {
        props.onStopCountDown();
      }
      setCurrentCount((preCurrentCount) => {
        if (preCurrentCount === 1) {
          props.onStopCountDown();
          clearInterval(interval);
        }
        return preCurrentCount - 1;
      });
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const render = () => `${currentCount}s`;

  return (
    <>
      {render()}
    </>
  );
};

CountDownComponent.defaultProps = {
  seconds: 59,
  onStopCountDown: () => {},
};

CountDownComponent.propTypes = {
  seconds: PropTypes.number,
  onStopCountDown: PropTypes.func,
};

export default CountDownComponent;
