import React from 'react';
import PropTypes from 'prop-types';
import Image from 'next/image';
import classnames from 'classnames'
import styles from './styles.module.scss';

interface IProps {
  className?: string,
  countryCode?: string,
}


const CountryFlag = (props: IProps): JSX.Element => (
  <Image
    width={18}
    height={12}
    className={classnames(styles.countryFlag, props.className)}
      // https://gist.github.com/bantya/9a619ab7b8f262e83fda
    src={`http://flags.fmcdn.net/data/flags/mini/${props.countryCode?.toLowerCase()}.png`}
    alt=""
  />
);
CountryFlag.defaultProps = {
  className: '',
  countryCode: '',
};

CountryFlag.propTypes = {
  className: PropTypes.string,
  countryCode: PropTypes.string,
};

export default CountryFlag;
