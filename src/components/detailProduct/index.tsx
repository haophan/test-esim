import classnames from "classnames";
import React, { useEffect, useState } from "react";

import { Box } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import CustomButton from "../buttons/customButton";
import PriceTag from "../priceTag";
import ProductDescription from "../productDescription";
import SetQuantity from "../setQuantity";

import { addProduct } from "../../redux/reducers/cart";
import SizeSelection from "../sizeSelection";
import styles from "./styles.module.scss";
import IDetailProduct from "../../modals/detailProductInterface";
import IProductOption from "../../modals/productOptionInterface";
import IProductVariation from "../../modals/productVariationInterface";
import InfoIcon from "@mui/icons-material/Info";
import { updateBuyNow } from "../../redux/reducers/buyNow";
import { NextRouter, useRouter } from "next/router";
import _ from "lodash";
import IBundle from "../../modals/bundleInterface";
import { showWarningNotification } from "../notification";
import ImagesSlider from "../imagesSlider";

interface IProps {
  className?: string;
  product: IDetailProduct;
}

const DetailProduct = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();
  const cartInfomartion: any = useSelector<any>((state) => state.cart);
  const isLimited: number = cartInfomartion.products.findIndex(
    (product: any) => product.product.id === props.product.id
  );

  const dispatch = useDispatch();

  const [state, setState] = useState<{
    currentQuantity: number;
    selectedOption?: {
      id: number;
      variation: IProductVariation;
    };
  }>({
    currentQuantity: 1,
    selectedOption: undefined,
  });

  const resetState = (isHasSize = false) => {
    setState({ ...state, currentQuantity: 1 });
  };

  const onChangeQuantity = (isAdd = false) => {
    let newQuan;
    if (isAdd) {
      newQuan = state.currentQuantity + 1;
    } else {
      newQuan = state.currentQuantity - 1;
    }
    setState({ ...state, currentQuantity: newQuan });
  };

  const onChangeSize = (e: any, selectedOption: any) => {
    setState({ ...state, selectedOption });
  };

  const onAddToCart = (e: any) => {
    // if product has options --> must check state.selectedOption
    if (
      props.product.product_options &&
      props.product.product_options.length !== 0 &&
      !state.selectedOption
    ) {
      showWarningNotification("You must choost option!");
      return;
    }
    // if product has buy_limit --> must check the limit
    if (props.product.buy_limit) {
      const isProductExistedInCart = cartInfomartion.products.find(
        (cartItem: any) => cartItem.product.id === props.product.id
      );
      if (isProductExistedInCart) {
        showWarningNotification("This product only can be 1 unit in your cart");
        return;
      }
    }
    dispatch(
      addProduct({
        quantity: state.currentQuantity,
        product: {
          id: props.product.id,
          title: props.product.title,
          is_bundle: props.product.is_bundle,
          buy_limit: props.product?.buy_limit,
          price: props.product.price,
          discount: props.product.discount,
          final_price: props.product.final_price,
          optionType:
            props.product?.product_options &&
            props.product.product_options.length !== 0
              ? props.product.product_options[0].title
              : undefined,
          stripe_subscription_id: props.product.stripe_subscription_id
            ? props.product.stripe_subscription_id
            : undefined,
          currency: props.product.currency,
          thumbnail: props.product?.thumbnail,
          selectedOptions: !_.isEmpty(state.selectedOption)
            ? state.selectedOption
            : undefined,
        },
      })
    );
    resetState();
  };

  const onBuyItNow = (e: any) => {
    dispatch(
      updateBuyNow({
        quantity: state.currentQuantity,
        product: {
          id: props.product.id,
          title: props.product.title,
          is_bundle: props.product.is_bundle,
          buy_limit: props.product.buy_limit,
          price: props.product.price,
          discount: props.product.discount,
          final_price: props.product.final_price,
          optionType:
            props.product?.product_options &&
            props.product.product_options.length !== 0
              ? props.product.product_options[0].title
              : undefined,
          thumbnail: props.product?.thumbnail,
          stripe_subscription_id: props.product.stripe_subscription_id,
          currency: props.product.currency,
          selectedOptions: state.selectedOption
            ? state.selectedOption
            : undefined,
        },
      })
    );
    router.push("/checkout?buyNow=true");
    resetState();
  };

  useEffect(() => {
    if (state.currentQuantity !== 1) {
      resetState();
    }
  }, [state.selectedOption]);
  // carosel - info
  return (
    <Box className={classnames(styles.detailProduct, props.className)}>
      <Box className={styles.detailProductWrapper}>
        <ImagesSlider
          className={styles.detailProductImage}
          images={props.product.detail_images}
        />
        <Box className={styles.detailProductInfo}>
          <h2 className={styles.productName}>{props.product.title}</h2>
          <PriceTag
            className={styles.priceTag}
            originalPrice={props.product.price}
            discount={props.product?.discount}
            finalPrice={props.product?.final_price}
            mainProduct
          />
          <Box>
            {props.product.bundles &&
              props.product.bundles.map((bundle: IBundle, index: number) => {
                return (
                  <ProductDescription
                    key={bundle.id}
                    className={styles.productDescription}
                    title={props.product.title}
                    is_bundle={props.product.is_bundle}
                    bundle={bundle}
                  />
                );
              })}
            {props.product?.description && (
              <p className={styles.description}>{props.product.description}</p>
            )}
          </Box>
          {props.product?.product_options &&
            props.product.product_options.map(
              (productOption: IProductOption) => (
                <SizeSelection
                  key={productOption.id}
                  productOption={productOption}
                  className={styles.chooseStrap}
                  selectedOption={state.selectedOption}
                  onChangeSize={onChangeSize}
                />
              )
            )}
          {!(
            isLimited !== -1 &&
            cartInfomartion.products[isLimited].product.buy_limit
          ) && (
            <Box className={styles.actions}>
              <CustomButton
                className={styles.buyBtn}
                type="outlined"
                onClick={onBuyItNow}
                disabled={
                  props.product.product_options &&
                  props.product.product_options.length !== 0 &&
                  !state.selectedOption
                }
              >
                Buy it now
              </CustomButton>
              <CustomButton
                className={styles.addBtn}
                type="primary"
                onClick={onAddToCart}
                disabled={
                  props.product.product_options &&
                  props.product.product_options.length !== 0 &&
                  !state.selectedOption
                }
              >
                Add to cart
              </CustomButton>
            </Box>
          )}
          {props.product.buy_limit && props.product.buy_limit === 1 ? (
            <Box
              className={classnames(
                styles.quantityLimitationAnnoucement,
                styles.mt24
              )}
            >
              <InfoIcon classes={{ root: styles.infoIcon }} />
              <p className={styles.announcementText}>
                You can only add one Bioheart Bundle to each order.
              </p>
            </Box>
          ) : (
            <SetQuantity
              className={styles.setQuantity}
              quantity={state.currentQuantity}
              onChangeQuantity={onChangeQuantity}
            />
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default DetailProduct;
