import React from "react";
import classnames from "classnames";

import styles from "./styles.module.scss";
import emptyCartImage from "../../../../../public/images/shop/png/emptyCart.png";
import { Box } from "@mui/material";

interface IProps {
  className?: string
}

const EmptyCart = (props: IProps): JSX.Element => {
  return (
    <Box className={classnames(styles.emptyCart, props.className)}>
      <img src={emptyCartImage} alt="empty-cart" />
      <h1 className={styles.emptyCartTitle}>Your cart is empty</h1>
      <p className={styles.emptyCartDescription}>Look like you haven&apos;t made your choice yet</p>
    </Box>
  );
};

export default EmptyCart;
