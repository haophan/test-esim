import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import { Box, Drawer, IconButton } from "@mui/material";
import Image from "next/image";
import { useSelector } from "react-redux";
import { NextRouter, useRouter } from "next/router";
import greyCloseIcon16 from "../../../../public/images/shop/svg/grey-close-icon-16.svg";

import CustomButton from "../../buttons/customButton";
import CartProduct from "../../cartProduct";
import styles from "./styles.module.scss";
import EmptyCart from "./emptyCart";

interface IProps {
  className: string;
  paperClassName: string;
  open: boolean;
  togglePanel: (event: React.KeyboardEvent | React.MouseEvent) => void;
}

const CartDrawer = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();
  const cart: any = useSelector<any>((state) => state.cart);
  const totalItems = cart.products.reduce(
    (accumulator: any, currentValue: any) =>
      accumulator + currentValue.quantity,
    0
  );
  const totalPrice = cart.products.reduce(
    (accumulator: any, currentValue: any) =>
      accumulator +
      currentValue.quantity *
      (currentValue.product?.discount
        ? currentValue.product.final_price
        : currentValue.product.price),
    0
  );
  const deliveryFee = 0;
  const inTotalPrice = totalPrice + deliveryFee;

  const onClose = (e: React.KeyboardEvent | React.MouseEvent) => {
    props.togglePanel(e);
  };

  const onCheckout = (e: React.MouseEvent) => {
    onClose(e);
    router.push("/checkout");
  };
  return (
    <Drawer
      anchor="right"
      open={props.open}
      onClose={onClose}
      classes={{
        root: classnames(styles.cartDrawer, props.className),
        paper: classnames(styles.cartDrawerPaper, props.paperClassName),
      }}
    >
      <Box className={styles.cartDrawerContent}>
        <Box className={styles.cartDrawerHeader}>
          <h4>Your cart</h4>
          <IconButton onClick={onClose}>
            <Image
              width={16}
              height={16}
              src={greyCloseIcon16}
              alt="close-icon"
            />
          </IconButton>
        </Box>
        {cart.products.length === 0 ? (
          <EmptyCart className={styles.cartDrawerBody} />
        ) : (
          <>
            <Box className={styles.cartDrawerBody}>
              {cart.products.map((product: any, index: number) => (
                <CartProduct
                  className={index === cart.products.length - 1 ? styles.lastestCartProduct : undefined}
                  key={`${product.id}-${index}`}
                  quantity={product.quantity}
                  product={product.product}
                />
              ))}
            </Box>
            <Box className={styles.cartDrawerCheckout}>
              <Box className={styles.paymentInfo}>
                <Box className={styles.row}>
                  <p
                    className={styles.title}
                  >{`Subtotal (${totalItems} item${
                    totalItems === 1 ? "" : "s"
                  })`}</p>
                  <p className={styles.price}>{`$${totalPrice}`}</p>
                </Box>
                <Box className={styles.row}>
                  <p className={styles.title}>Shipping</p>
                  <p className={styles.price}>
                    {deliveryFee ? `$${deliveryFee}` : "Free"}
                  </p>
                </Box>
              </Box>
              <Box className={styles.totalInfo}>
                <Box className={styles.row}>
                  <p className={styles.title}>In total</p>
                  <p
                    className={classnames(styles.price, styles.totalPrice)}
                  >{`$${inTotalPrice}`}</p>
                </Box>
              </Box>
              <CustomButton
                className={styles.checkoutBtn}
                type="primary"
                onClick={onCheckout}
              >
                Check out
              </CustomButton>
            </Box>
          </>
        )}
      </Box>
    </Drawer>
  );
};

CartDrawer.defaultProps = {
  className: "",
  paperClassName: "",
  open: false,

  togglePanel: () => { },
};

CartDrawer.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** override paper className */
  paperClassName: PropTypes.string,
  /** is drawer open */
  open: PropTypes.bool,

  /** on close event */
  togglePanel: PropTypes.func,
};

export default CartDrawer;
