import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import {
  Box,
  Drawer,
  IconButton,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import Image from 'next/image';
import { List } from 'antd';
import LogoutIcon from '@mui/icons-material/Logout';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import greyCloseIcon16 from '../../../../public/images/shop/svg/grey-close-icon-16.svg';

import styles from './styles.module.scss';

interface IProps {
  className: string
  open: boolean
  togglePanel: (e: any) => void
  onSignOut: (e: any) => void
}

const MoreActionsDrawer = (props: IProps) => {
  const onClose = (e: any) => {
    props.togglePanel(e);
  };

  const onSignOut = (e: any): void => {
    props.onSignOut(e);
    onClose(e);
  }
  return (
    <Drawer
      anchor="right"
      open={props.open}
      onClose={onClose}
      classes={{
        root: classnames(styles.moreActionsDrawer, props.className),
        paper: styles.moreActionsDrawerPaper,
      }}
    >
      <Box className={styles.moreActionsDrawerContent}>
        <Box className={styles.moreActionsDrawerHeader}>
          <IconButton onClick={onClose}>
            <Image
              width={16}
              height={16}
              src={greyCloseIcon16}
              alt="close-icon"
            />
          </IconButton>
        </Box>
        <List>
          <ListItem key="my-profile-drawer" disablePadding>
            <ListItemButton onClick={onClose}>
              <ListItemIcon classes={{ root: styles.profileActionsIcon }}>
                <AssignmentIndIcon />
              </ListItemIcon>
              <ListItemText
                classes={{ root: styles.profileActionsText }}
                primary="My profile"
              />
            </ListItemButton>
          </ListItem>
          <ListItem key="log-out-drawer" disablePadding>
            <ListItemButton onClick={onSignOut}>
              <ListItemIcon classes={{ root: styles.profileActionsIcon }}>
                <LogoutIcon />
              </ListItemIcon>
              <ListItemText
                classes={{ root: styles.profileActionsText }}
                primary="Log out"
              />
            </ListItemButton>
          </ListItem>
        </List>
      </Box>
    </Drawer>
  );
};

MoreActionsDrawer.defaultProps = {
  className: '',
  open: false,

  togglePanel: () => {},
};

MoreActionsDrawer.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** is open drawer */
  open: PropTypes.bool,

  /** on close event */
  togglePanel: PropTypes.func,
};

export default MoreActionsDrawer;
