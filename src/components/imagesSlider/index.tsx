import React, { useEffect, useRef, useState } from "react";
import classnames from "classnames";
import { Box } from "@mui/material";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import styles from "./styles.module.scss";
interface IProps {
  className?: string;
  images: any[];
}
const ImagesSlider = (props: IProps): JSX.Element => {
  const [currentSlideShow, setCurrentSlideShow] = useState<number>(0);
  const sliderWrapperElemetnRef = useRef<HTMLElement | null>(null);
  const isNextRef = useRef<boolean>(false);
  const onChangeSlideShow = (index: number) => {
    isNextRef.current = index > currentSlideShow;
    if (currentSlideShow === index) {
      return;
    } else {
      setCurrentSlideShow(index);
    }
  };
  // useEffect(() => {
  //   const timeout = setTimeout(() => {
  //     if (currentSlideShow === props.images.length - 1) {
  //       setCurrentSlideShow(0);
  //     } else {
  //       setCurrentSlideShow(currentSlideShow + 1);
  //     }
  //   }, 3000);
  //   return () => {
  //     if (timeout) {
  //       clearTimeout(timeout);
  //     }
  //   };
  // }, [currentSlideShow]);
  
  useEffect(() => {
    const currentElement = document.getElementById(
      `slide-${props.images[currentSlideShow].id}`
    );
    if (currentElement && sliderWrapperElemetnRef.current) {
      if (isNextRef.current) {
        sliderWrapperElemetnRef.current.scrollTo({
          left: currentElement.offsetLeft - 128,
          behavior: "smooth",
        });
      } else {
        sliderWrapperElemetnRef.current.scrollTo({
          left: currentElement.offsetLeft - 128,
          behavior: "smooth",
        });
      }
    }
  }, [currentSlideShow]);
  useEffect(() => {
    const sliderWrapper: HTMLElement | null =
      document.getElementById(`image-slider`);
    if (sliderWrapper) {
      sliderWrapperElemetnRef.current = sliderWrapper;
    }
  }, []);
  return (
    <Box className={classnames(styles.myImageSlider, props.className)}>
      {/* <img src={mockBioheartDevice} width="100%" height="100%" alt="" /> */}
      <Box className={classnames(styles.bigImageWrapper, styles.fade)}>
        <img
          src={props.images[currentSlideShow]?.url}
          width="100%"
          height="100%"
          alt=""
        />
        {currentSlideShow !== 0 && (
          <Box className={styles.arrowWrapper}>
            <KeyboardArrowLeftIcon
              onClick={() => onChangeSlideShow(currentSlideShow - 1)}
              classes={{ root: styles.leftArrow }}
            />
          </Box>
        )}
        {currentSlideShow !== props.images.length - 1 && (
          <Box className={styles.arrowWrapper}>
            <KeyboardArrowRightIcon
              onClick={() => onChangeSlideShow(currentSlideShow + 1)}
              classes={{ root: styles.rightArrow }}
            />
          </Box>
        )}
      </Box>
      <Box className={styles.imgSlide} id="image-slider">
        {props.images.map((image: any, index: number) => (
          <Box
            id={`slide-${image.id}`}
            key={image.id}
            className={classnames(
              styles.miniImageWrapper,
              currentSlideShow === index ? styles.miniImageWrapperActive : ""
            )}
            onClick={() => onChangeSlideShow(index)}
          >
            <img src={image.url} alt=""/>
          </Box>
        ))}
      </Box>
      <Box className={styles.dotSlide}>
        {props.images.map((image: any, index: number) => (
          <Box
            key={image.id}
            className={classnames(
              styles.dots,
              currentSlideShow === index ? styles.activeDot : ""
            )}
            onClick={() => onChangeSlideShow(index)}
          />
        ))}
      </Box>
    </Box>
  );
};
export default ImagesSlider;
