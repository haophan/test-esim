import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import style from './style.module.scss';

const InputTitle = (props) => {
  const { className, title, moreTitle } = props;
  if (title) {
    return (
      <div className={classnames(style.inputTitle, className)}>
        <span>
          {title}
          <span className={style.starSign}>{props.isRequire && (<> *</>)}</span>
        </span>
        {moreTitle && <span className={style.moreTitle}>{props.moreTitle}</span>}
      </div>
    );
  }
  return null;
};
InputTitle.defaultProps = {
  className: '',
  title: '',
  moreTitle: '',
  isRequire: false,
};
InputTitle.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  moreTitle: PropTypes.string,
  isRequire: PropTypes.bool,
};

export default InputTitle;
