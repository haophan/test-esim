/* eslint-disable camelcase */
import React, {
  useRef, useImperativeHandle, useEffect, forwardRef,
} from 'react';
import PropTypes from 'prop-types';
import Select, { components } from 'react-select';
import usePlacesAutocomplete, { getDetails } from 'use-places-autocomplete';
import classnames from 'classnames';
import _ from 'lodash';
import Image from 'next/image';
import noResults from '../../../../public/images/shop/png/no-options.png';
import { useMergeState } from '../../../utils/hooks/useMergeState';

const CustomInput = (selectProps) => {
  const newProps = _.cloneDeep(selectProps);
  _.assign(newProps, { isHidden: false });
  return (<components.Input {...newProps} />);
};

const getDataGoogle = (addressComponents) => {
  const object = {};
  _.forEach(addressComponents, (element) => {
    if (typeof element.types !== 'undefined') {
      if (element.types[0] === 'postal_code') {
        _.assign(object, { zip: element.short_name });
      }
      if (element.types[0] === 'administrative_area_level_1') {
        const state = {
          name: element.long_name,
          code: element.short_name,
        };
        _.assign(object, { state });
      }
      if (element.types[0] === 'country') {
        const country = {
          name: element.long_name,
          code: element.short_name,
        };
        _.assign(object, { country });
      }
      if (element.types[0] === 'locality') {
        _.assign(object, { city: element.long_name });
      } else if (element.types.includes('sublocality') || element.types.includes('sublocality_level_1')) {
        _.assign(object, { city: element.long_name });
      }
    }
  });
  return object;
};

const AutoCompleteInput = forwardRef((props, ref) => {
  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      minHeight: 'unset',
      height: '3rem',
      // boxShadow: state.isFocused ? '0px 0px 4px 1px #397ACC' : 'none',
      borderRadius: '8px',
      padding: '0 0 0 10px',
      backgroundColor: state.isDisabled ? '#E7E9EC' : '#F5F7FA',
      '&:hover': {
        border: props.isError
          ? '1px solid #40a9ff'
          : '1px solid #6EB43F',
      },
      '&:focus': {
        border: props.isError
          ? '1px solid #40a9ff'
          : '1px solid #6EB43F',
      },
      border: state.isFocused ? '1px solid #6EB43F'
        : '1px solid #F5F7FA',
      boxShadow: props.isError
        ? 'unset' : state.isFocused ? 'unset'
          : 'unset',
    }),
    indicatorSeparator: (provided, state) => ({
      ...provided,
      display: 'none',
    }),
    menu: (provided, state) => ({
      ...provided,
      borderRadius: '4px',
      border: 'none',
      padding: '12px 0',
      boxShadow: '0px 4px 20px rgba(99, 134, 76, 0.1)',
      marginTop: '0',
    }),
    multiValue: (provided, state) => ({
      ...provided,
      backgroundColor: '#0D9E92',
      color: 'white',
      padding: '0 0.5rem',
    }),
    multiValueLabel: (provided, state) => ({
      ...provided,
      color: 'white',
      padding: 0,
      paddingLeft: 0,
      fontSize: '0.875rem',
    }),
    multiValueRemove: (provided, state) => ({
      ...provided,
      padding: 0,
      marginLeft: '0.75rem',

      '&:hover': {
        backgroundColor: '#0D9E92',
        color: 'white',
        cursor: 'pointer',
      },
    }),
    option: (provided, state) => ({
      ...provided,
      backgroundColor: state.isSelected ? '#E6F4DC' : 'none',
      // color: state.isSelected ? '#49494F' : '#49494F',
      color: 'black',
      fontWeight: state.isSelected ? 'bold' : 'normal',
      fontSize: '16px',
      display: 'flex',
      alignItems: 'center',
      minHeight: '48px',
      border: 'none',
      borderRadius: '0px',
      padding: '8px 16px',
      '&:hover': {
        backgroundColor: 'none',
        cursor: 'pointer',
      },
      '&:not(:first-of-type)': {
        borderTop: '1px solid #E6E9EB',
      },
    }),
    placeholder: (provided, state) => ({
      ...provided,
      color: '#696A6B',
      // fontStyle: 'italic',
    }),
    singleValue: (provided, state) => ({
      ...provided,
      color: '#1D2F47',
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      maxHeight: 'calc(8.25rem - 2 * 0.75rem)',
      overflowY: 'auto',
      padding: 0,
    }),
    clearIndicator: (provided, state) => ({
      ...provided,
      padding: 0,
      width: '15px',
    }),
  };
  const refCurrent = useRef({});
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    requestOptions: {
      componentRestrictions: {
        country: props.country ? props.country : ['CA', 'US'],
      },
      types: props.isSearchCity ? ['(cities)'] : [],
    },
    cache: false,
  });

  const [state, setState] = useMergeState({
    addressArray: [],
    address: '',
  });

  const resetData = () => {
    setState({ addressArray: [], address: '' });
  };

  useImperativeHandle(ref, () => ({
    clearData: () => {
      setState({ addressArray: [], address: '' });
      refCurrent.current = [];
    },
  }));

  const onInputChange = (inputValue, { action }) => {
    if (action === 'input-change') {
      if (props.onChangeInput) {
        props.onChangeInput(inputValue, props.isSearchCity);
      }
      setValue(inputValue);
      setState({ address: inputValue });
    }
  };

  const onChange = (addressSelect) => {
    if (addressSelect) {
      const eleData = _.find(refCurrent.current.dataAddress, (x) => x.value === addressSelect.value);
      if (eleData) {
        const { placeId } = eleData;
        if (placeId) {
          const parameter = {
            placeId,
          };
          getDetails(parameter).then((result) => {
            if (result) {
              const { address_components: addressComponents = {}, utc_offset_minutes = 0 } = result;
              const object = getDataGoogle(addressComponents);
              _.assign(object, { utcOffset: utc_offset_minutes || 0 });
              if (!props.isSearchCity) {
                _.assign(object, { address: eleData.address });
              }
              if (props.onChange) {
                props.onChange(props?.name, object, props.isSearchCity);
                props.onChangeAddress(props.name, object);
              }
            }
          }).catch((err) => {
            console.error('Failed to get geocode', err);
          });
        }
        setState({ address: eleData.address });
      }
    } else {
      resetData();
      if (props.eventClear) {
        props.eventClear(props.isSearchCity);
      }
    }
  };

  useEffect(() => {
    if (props.value !== undefined) {
      setState({ address: props.value });
    }
  }, [props.value]);

  useEffect(() => {
    const newArray = [];
    const newArrayData = [];
    _.forEach(data, (x, index) => {
      const {
        structured_formatting: { main_text: mainText, secondary_text: secondaryText },
        place_id: placeId,
      } = x;
      newArray.push({
        value: index,
        label: `${mainText} ${secondaryText || ''}`,
      });
      newArrayData.push({
        value: index,
        label: `${mainText} ${secondaryText || ''}`,
        placeId,
        address: mainText,
      });
    });
    refCurrent.current = { dataAddress: newArrayData };
    setState({ addressArray: newArray });
  }, [data]);
  return (
    <div className={classnames('custom-select-input', props.className)}>
      {
        !!props.title && (
          <div className="custom-select-input__label">{props.title}</div>
        )
      }

      <Select
        inputId={props.id}
        onKeyDown={props.onKeyPress}
        isClearable
        isDisabled={props.disable}
        styles={customStyles}
        name={props.name}
        placeholder={props.placeholder}
        options={state.addressArray}
        filterOption={() => true}
        inputValue={state.address}
        value=""
        onInputChange={onInputChange}
        onChange={onChange}
        components={{
          Input: CustomInput,
          DropdownIndicator: () => null,
        }}
        menuPlacement="auto"
        noOptionsMessage={() => (
          <>
            <Image src={noResults} width={64} height={64} />
            <div>No results found</div>
          </>
        )}
      />

      {
        props.isError && !!props.errorMessage
          ? (
            <div className="custom-select-input__error-message">{props.errorMessage}</div>
          )
          : null
      }
    </div>
  );
});

AutoCompleteInput.displayName = 'AutoCompleteInput';

AutoCompleteInput.defaultProps = {
  id: '',
  isSearchCity: false,
  disable: false,
  className: '',
  country: ['CA', 'US'],
  title: '',
  placeholder: '',
  isError: false,
  errorMessage: '',
  value: '',
  onKeyPress: () => { },
  onChangeAddress: () => { },
};

AutoCompleteInput.propTypes = {
  isSearchCity: PropTypes.bool,
  disable: PropTypes.bool,
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  country: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  title: PropTypes.string,
  placeholder: PropTypes.string,
  isError: PropTypes.bool,
  errorMessage: PropTypes.string,
  value: PropTypes.string,
  onChangeInput: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  eventClear: PropTypes.func.isRequired,
  id: PropTypes.string,
  onKeyPress: PropTypes.func,
  onChangeAddress: PropTypes.func,
};

export default AutoCompleteInput;
