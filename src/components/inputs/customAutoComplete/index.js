import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {
  Autocomplete, Box, InputAdornment, TextField,
} from '@mui/material';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import style from './style.module.scss';

const CustomAutoComplete = (props) => {
  // const [value, setValue] = useState([]);
  const [inputValue, setInputValue] = useState();
  const onChange = (event, value, reason, details) => {
    props.onChange(value);
    setInputValue('');
    // setValue(data);
  };

  const onInputChange = (event, value, reason) => {
    setInputValue(value);
  };
  return (
    <Box className={props.className} id={props.id}>
      <Autocomplete
        multiple
        filterSelectedOptions
        options={props.options}
        getOptionLabel={(option) => option.label}
        defaultValue={[]}
        value={props.value}
        onChange={onChange}
        onInputChange={onInputChange}
        inputValue={inputValue}
        size="medium"
        fullWidth
        open={!!inputValue}
        onFocus={props.onFocus}
        onBlur={props.onBlur}
        onMouseOver={props.onMouseOver}
        onMouseOut={props.onMouseOut}
        renderInput={(params) => (
          <TextField
            classes={{
              root: style.inputField,
            }}
            {...params}
            InputProps={{
              ...params.InputProps,
              startAdornment:
                (
                  <>
                    <InputAdornment
                      position="start"
                      classes={{
                        root: style.inputAdornment,
                      }}
                    >
                      {props.isShowBackArrow
                        ? <ArrowBackIcon onClick={props.clearAllFilters} />
                        : <SearchIcon />}
                    </InputAdornment>
                    {params.InputProps.startAdornment}
                  </>
                ),
            }}
            variant="outlined"
            placeholder={props.placeholder}
          />
        )}
        renderOption={(optionProps, option, { selected }) => (
          <div {...optionProps} className={style.autocompleteOption}>
            <div className={style.title}>
              {option.label}
            </div>
            <AddIcon className={style.icon} />
          </div>
        )}
        classes={{
          root: style.customAutoComplete,
          focused: style.customAutoCompleteFocused,
          tag: style.tag,
          endAdornment: style.endAdornment,
        }}
      />
    </Box>

  );
};

PropTypes.defaultProps = {
  id: undefined,
  placeholder: 'Search snapshot with tags',
  options: [],
};

PropTypes.propTypes = {
  id: PropTypes.string,
  placeholder: PropTypes.string,
  options: PropTypes.arrayOf(),
};

export default CustomAutoComplete;
