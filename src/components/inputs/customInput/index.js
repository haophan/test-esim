import { Visibility, VisibilityOff } from "@mui/icons-material";
import { Box, IconButton, Input, InputAdornment } from "@mui/material";
import classnames from "classnames";
import _ from "lodash";
import PropTypes from "prop-types";
import React, { useState } from "react";
import InputMask from "react-input-mask";
import NumberFormat from "react-number-format";
import InputTitle from "../../inputTitle";
import style from "./style.module.scss";

const CustomInput = (props) => {
  const [showPassword, setShowPassword] = useState(false);

  const {
    placeholder,
    onChange,
    name,
    multiline,
    id,
    value,
    startAdornment,
    endAdornment,
    disableUnderline,
    disabled,
    rows,
    required,
    type,
    className,
    containerClassName,
    title,
    fullWidth,
    errorMessage,
    isError,
    inputProps,
    onKeyDown,
    sx,
    phoneNumberMask,
    dateMask,
    isIntegerNumber,
    isRealNumber,
    isRequire,
    moreTitle,
  } = props;

  const onChangeInput = (inputValue) => {
    if (isIntegerNumber) {
      const integerRegax = /^(?!00)\d+$/;
      if (
        (!_.isNaN(inputValue) && integerRegax.test(inputValue)) ||
        inputValue === ""
      ) {
        onChange(name, inputValue);
        return;
      }
      return;
    }
    if (isRealNumber) {
      const realRegax = /^(?!00)\d*\.?\d?$/;
      if (
        (!_.isNaN(inputValue) && realRegax.test(inputValue)) ||
        inputValue === ""
      ) {
        onChange(name, inputValue);
        return;
      }
      return;
    }
    onChange(name, inputValue);
  };

  const onChangeMask = (e) => {
    const { name: newName, value: newValue } = e.target;
    onChange(newName, newValue);
  };

  const format = (inputValue) => inputValue;

  return (
    <Box sx={sx} className={containerClassName} id={props.containerId}>
      {title && (
        <InputTitle
          title={title}
          isRequire={props.isRequire}
          moreTitle={moreTitle}
        />
      )}
      {type === "password" ? (
        <Input
          classes={{
            root: style.customInput,
            focused: style.focusInput,
            error: errorMessage && style.errorInput,
          }}
          id={id}
          value={value}
          startAdornment={startAdornment}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={() => {
                  setShowPassword(!showPassword);
                }}
                edge="end"
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
          disabled={disabled}
          disableUnderline={disableUnderline}
          fullWidth={fullWidth}
          type={showPassword ? "text" : "password"}
          error={!!errorMessage}
          name={name}
          onChange={(e) => onChangeInput(e.target.value)}
          placeholder={placeholder}
          className={classnames("custom-input", className)}
        />
      ) : type === "number" ? (
        <NumberFormat
          id={id}
          className={classnames("custom-input-number", className)}
          placeholder={placeholder}
          name={name}
          onValueChange={(input) => onChange(props.name, input.value)}
          value={value}
          disabled={disabled}
          allowNegative={false}
          isNumericString
          format={format}
          onKeyDown={onKeyDown}
        />
      ) : phoneNumberMask || dateMask ? (
        <InputMask
          className={classnames(
            "custom-mask-input",
            isError ? "error-mask" : undefined,
            className
          )}
          name={name}
          type={type}
          placeholder={placeholder}
          mask={dateMask ? "99/99/9999" : "999-999-9999"}
          maskChar={null}
          value={value}
          onChange={onChangeMask}
          disabled={disabled}
        />
      ) : (
        <Input
          classes={{
            root: style.customInput,
            focused: style.focusInput,
            error: errorMessage && style.errorInput,
            multiline: style.multiLineInput,
          }}
          id={id}
          value={value}
          startAdornment={startAdornment}
          endAdornment={endAdornment}
          disabled={disabled}
          disableUnderline={disableUnderline}
          required={required}
          multiline={multiline}
          fullWidth={fullWidth}
          rows={rows}
          type={type}
          error={!!errorMessage}
          name={name}
          inputProps={inputProps}
          onChange={(e) => onChangeInput(e.target.value)}
          placeholder={placeholder}
          className={classnames("custom-input", className)}
        />
      )}
      {isError && !!errorMessage && (
        <div className={style.errorMessage}>{errorMessage}</div>
      )}
    </Box>
  );
};

CustomInput.defaultProps = {
  id: "",
  containerId: undefined,
  onChange: () => {},
  name: "",
  placeholder: "Enter your text",
  multiline: false,
  rows: 5,
  required: false,
  type: "text",
  value: "",
  startAdornment: null,
  endAdornment: null,
  disabled: false,
  disableUnderline: true,
  className: undefined,
  containerClassName: "",
  title: "",
  fullWidth: true,
  errorMessage: "",
  isError: false,
  inputProps: {},
  sx: {},
  onKeyDown: () => {},
  phoneNumberMask: false,
  dateMask: false,
  isIntegerNumber: false,
  isRealNumber: false,
  isRequire: false,
  moreTitle: "",
};

CustomInput.propTypes = {
  id: PropTypes.string,
  containerId: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  multiline: PropTypes.bool,
  rows: PropTypes.number,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  disableUnderline: PropTypes.bool,
  startAdornment: PropTypes.node,
  endAdornment: PropTypes.node,
  className: PropTypes.string,
  containerClassName: PropTypes.string,
  title: PropTypes.string,
  fullWidth: PropTypes.bool,
  errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  isError: PropTypes.bool,
  inputProps: PropTypes.objectOf(PropTypes.shape()),
  sx: PropTypes.objectOf(PropTypes.shape()),
  onKeyDown: PropTypes.func,
  phoneNumberMask: PropTypes.bool,
  dateMask: PropTypes.bool,
  isIntegerNumber: PropTypes.bool,
  isRealNumber: PropTypes.bool,
  isRequire: PropTypes.bool,
  moreTitle: PropTypes.string,
};

export default CustomInput;
