import { Box, Input, InputAdornment } from '@mui/material';
import PropTypes from 'prop-types';
import React from 'react';
import style from './style.module.scss';

const NotesInput = (props) => {
  const {
    title,
    id,
    name,
    value,
    maxLength,
    className,
    containerClassName,
    fullWidth,
    multiline,
    maxRows,
    minRows,
    disableUnderline,
    showLength,
    isError,
    errorMessage,
    onChangeNotes,
    placeholder,
  } = props;

  return (
    <Box className={containerClassName}>
      <p className={style.title}>{title}</p>
      <Input
        classes={{
          root: style.notesInput,
          focused: style.focusInput,
          error: errorMessage && style.errorInput,
        }}
        id={id}
        value={value}
        onChange={(e) => onChangeNotes(e.target.value)}
        endAdornment={showLength && (
          <InputAdornment position="end">
            <Box className={style.lengthNotes}>
              {value.length}
              /
              {maxLength}
            </Box>
          </InputAdornment>
        )}
        disableUnderline={disableUnderline}
        fullWidth={fullWidth}
        multiline={multiline}
        maxRows={maxRows}
        minRows={minRows}
        error={!!errorMessage}
        name={name}
        placeholder={placeholder}
        className={className}
      />

      {
        isError && !!errorMessage && <div className={style.errorMessage}>{errorMessage}</div>
      }
    </Box>
  );
};

NotesInput.defaultProps = {
  className: undefined,
  maxLength: 200,
  onChangeNotes: () => {},
};

NotesInput.propTypes = {
  /** Function className */
  className: PropTypes.string,
  /** Max length of value */
  maxLength: PropTypes.number,
  /** Change Notes function */
  onChangeNotes: PropTypes.func,
};

export default NotesInput;
