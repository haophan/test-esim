/* eslint-disable no-plusplus */
export const handleNode = (time) => {
  const list = [];
  for (let i = 0; i <= (time / 60); i++) {
    list.push(`${i}:00`);
  }
  return list;
};

export const handleBackgroundScroll = (range, value, snapshot) => {
  range.style.background = `linear-gradient(90deg, #9ACF77 ${value / snapshot.duration * 100}%, transparent 0)`;
};

export const handleTimeCurrent = (value) => {
  const minute = Math.floor(value / 60);
  const second = value % 60;
  if (second < 10) {
    return `0${minute}:0${second}`;
  }
  return `0${minute}:${second}`;
};
