import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';
import classNames from 'classnames';
import _ from 'lodash';
import style from './style.module.scss';
import { handleNode, handleTimeCurrent } from './helper';

const RangeCustomInput = (props) => {
  const rangeElement = document.getElementById('range');
  const timeCurrentElement = document.getElementById('timeShow');

  const handleChangeValue = (e) => {
    props.onChange(+e.target.value);
    if (timeCurrentElement) {
      timeCurrentElement.classList.add(style.show);
    }
  };

  const handleMouseUp = () => {
    if (timeCurrentElement) {
      timeCurrentElement.classList.remove(style.show);
    }
  };

  const handleClickPoint = (id) => {
    props.onChange(+id * 60);
  };

  useEffect(() => {
    if (rangeElement) {
      rangeElement.style.background = `linear-gradient(90deg, #9ACF77 ${props.value / props.max * 100}%, transparent 0)`;
    }

    if (timeCurrentElement) {
      timeCurrentElement.style.left = `${props.value / props.max * 100}%`;
    }
  });

  return (
    <Box className={classNames(style.rangeCustomInput, props.className)}>
      <Box className={style.scrollGroup}>
        <input
          id="range"
          type="range"
          min={0}
          max={props.max}
          step={1}
          className={style.range}
          value={props.value}
          onChange={handleChangeValue}
          onMouseUp={handleMouseUp}
        />
        <div className={style.timeShow} id="timeShow">
          <p className={style.time}>
            {handleTimeCurrent(props.value)}
          </p>
        </div>
        <div className={style.scroll}>
          {_.map(handleNode(props.max), (id, index) => (
            <div className={style.point} key={id} onClick={(e) => handleClickPoint(index)}>
              <div className={style.circle} />
              <p className={style.value}>{id}</p>
            </div>
          ))}
          {props.specialNode && <div className={style.specialNode} />}
        </div>
      </Box>
    </Box>
  );
};

RangeCustomInput.defaultProps = {
  specialNode: false,
  onChange: () => {},
  max: undefined,
  value: undefined,
};

RangeCustomInput.propTypes = {
  /** Show or hide special note */
  specialNode: PropTypes.bool,
  /** Change range value event */
  onChange: PropTypes.func,
  /** Max value of range */
  max: PropTypes.number,
  /** Value of range */
  value: PropTypes.number,
};

export default RangeCustomInput;
