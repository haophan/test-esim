import React from 'react';
import classnames from 'classnames';
import { Space, Spin } from 'antd';
import { useSelector } from 'react-redux';
import { isLoadingPage } from '../../redux/reducers/loading';

interface IProps {
  /** override className */
  className?: string,
}

const LoadingPage = (props: IProps): JSX.Element => {
  const isLoading = useSelector(isLoadingPage);
  if (!isLoading) {
    return <></>;
  }
  return (
    <Space className={classnames('loading-space', props.className)} size="middle">
      <Spin size="large" />
    </Space>
  );
};

export default LoadingPage;
