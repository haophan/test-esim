import React from 'react';
import { Box } from '@mui/material';
import { Button } from 'antd';
import styles from './styles.module.scss';
import backIcon from '../../../public/images/shop/svg/black-back-arrow-icon.svg'
import classnames from 'classnames';

interface IProps {
  /** override className */
  className?: string,
  /** Title of header */
  title?: string,
  /** call back on click back func */
  onClickBack?: () => void,
}

const LoginHeader = (props: IProps): JSX.Element => {
  return (
    <Box className={classnames(styles.loginPageHeader, props.className)}>
      <Button onClick={props.onClickBack} className={styles.backButton}>
        <img src={backIcon} alt='' />
      </Button>
      <Box className={styles.headerTitle}>{props.title}</Box>
      <Box></Box>
    </Box>
  );
}


export default LoginHeader;