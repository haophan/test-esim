import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import LogoutIcon from '@mui/icons-material/Logout';
import {
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Menu,
} from '@mui/material';

import styles from './styles.module.scss';

interface IProps {
  className: string,
  open: boolean,
  anchorEl: null | HTMLElement,
  onClose: (e: any) => void,
  onSignOut: (e: any) => void,
}

const MoreActionsMenu = (props: IProps): JSX.Element => {
  const onClose = (e: any): void => {
    props.onClose(e);
  };

  const onSignOut = (e: any): void => {
    props.onSignOut(e);
    props.onClose(e);
  }
  return (
    <Menu
      open={props.open}
      onClose={onClose}
      anchorEl={props.anchorEl}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      classes={{
        root: classnames(styles.moreActionsMenu, props.className),
        paper: styles.moreActionsMenuPaper,
        list: styles.moreActionsMenuList,
      }}

    >
      <ListItem key="my-profile-menu" disablePadding>
        <ListItemButton onClick={onClose}>
          <ListItemIcon classes={{ root: styles.profileActionsIcon }}>
            <AssignmentIndIcon />
          </ListItemIcon>
          <ListItemText
            classes={{ root: styles.profileActionsText }}
            primary="My profile"
          />
        </ListItemButton>
      </ListItem>
      <ListItem key="log-out-menu" disablePadding>
        <ListItemButton onClick={onSignOut}>
          <ListItemIcon classes={{ root: styles.profileActionsIcon }}>
            <LogoutIcon />
          </ListItemIcon>
          <ListItemText
            classes={{ root: styles.profileActionsText }}
            primary="Log out"
          />
        </ListItemButton>
      </ListItem>
    </Menu>
  );
};

MoreActionsMenu.defaultProps = {
  className: '',
  open: false,
  anchorEl: null,

  onClose: () => {},
};

MoreActionsMenu.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** is open menu */
  open: PropTypes.bool,
  /** anchor element */
  anchorEl: PropTypes.elementType,

  /** on close event */
  onClose: PropTypes.func,
};

export default MoreActionsMenu;
