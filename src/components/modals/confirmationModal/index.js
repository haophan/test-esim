import {
  Box, Fade, Modal,
} from '@mui/material';
import PropTypes from 'prop-types';
import React from 'react';
import CustomButton from '../../buttons/customButton';
import styles from './styles.module.scss';

const ConfirmationModal = (props) => {
  let title;
  let content;
  let leftBtnTitle;
  let rightBtnTitle;
  let isDanger = false;
  switch (props.type) {
    case 'SIGN_OUT': {
      title = 'Sign out';
      content = 'Are you sure you wish to sign out?';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Sign out';
      isDanger = true;
      break;
    }
    case 'CLEAR_ALL_NOTIFICATIONS': {
      title = 'Delete notifications';
      content = 'Are you sure you want to delete all notifications?';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Delete';
      isDanger = true;
      break;
    }
    case 'DELETE_NOTIFICATION': {
      title = 'Delete notification';
      content = 'Are you sure you want to delete notification?';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Delete';
      isDanger = true;
      break;
    }
    case 'INCOMPLETE_PROFILE': {
      title = 'Incomplete profile';
      content = 'Are you sure you want to leave?\nAll your input information will not be saved.';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Leave';
      break;
    }
    case 'UNSAVED_CHANGES': {
      title = 'Unsave changes';
      content = 'Are you sure you want to leave?\nAll changes to your information will be lost.';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Leave';
      break;
    }
    case 'DELETE_SNAPSHOT': {
      title = 'Delete snapshot';
      content = 'Are you sure you want to delete this rhythm snapshot?';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Delete';
      isDanger = true;
      break;
    }
    case 'DELETE_SUMMARY_REPORT': {
      title = 'Delete summary report';
      content = 'Are you sure you want to delete summary report?';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Delete';
      isDanger = true;
      break;
    }
    case 'RETRY_SUMMARY_REPORT': {
      title = 'Re-generate summary report';
      content = 'Are you sure you want to re-generate summary report?';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Re-generate';
      break;
    }

    default: {
      title = 'Sign out';
      content = 'Are you sure you wish to sign out?';
      leftBtnTitle = 'Cancel';
      rightBtnTitle = 'Sign out';
      isDanger = true;
      break;
    }
  }
  return (
    <Modal
      open={props.open}
      onClose={props.onCloseClick}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      classes={{ root: styles.confirmationModal }}
      closeAfterTransition
    >
      <Fade in={props.open}>
        <Box className={styles.confirmationModalBody}>
          <Box className={styles.confirmationModalTitle}>{title}</Box>
          {content && (
          <Box className={styles.confirmationModalDescription}>
            {content}
          </Box>
          )}
          <Box className={styles.confirmationModalFooter}>
            <CustomButton
              className={styles.btn}
              onClick={props.toggleClick}
              type="outlinedDefault"
            >
              {leftBtnTitle}
            </CustomButton>
            <CustomButton
              className={styles.btn}
              onClick={props.onClick}
              loading={props.loading}
              type={isDanger ? 'danger' : undefined}
            >
              {rightBtnTitle}
            </CustomButton>
          </Box>
        </Box>
      </Fade>
    </Modal>
  );
};

ConfirmationModal.defaultProps = {
  open: false,
  loading: false,
  toggleClick: () => {},
  onClick: () => {},
};

ConfirmationModal.propTypes = {
  /** modal is open or not */
  open: PropTypes.bool,
  /** yes btn loading */
  loading: PropTypes.bool,
  /** toggle modal */
  toggleClick: PropTypes.func,
  /** sign out function */
  onClick: PropTypes.func,
};

export default ConfirmationModal;
