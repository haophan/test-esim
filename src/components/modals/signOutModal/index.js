import {
  Box, Fade, Modal,
} from '@mui/material';
import PropTypes from 'prop-types';
import React from 'react';
import CustomButton from '../../buttons/customButton';
import styles from './styles.module.scss';

const SignOutModal = (props) => (
  <Modal
    open={props.open}
    onClose={props.onCloseClick}
    aria-labelledby="modal-modal-title"
    aria-describedby="modal-modal-description"
    classes={{ root: styles.signOutModal }}
    closeAfterTransition
  >
    <Fade in={props.open}>
      <Box className={styles.signOutModalBody}>
        <Box className={styles.signOutModalTitle}>Sign out</Box>
        <Box className={styles.signOutModalDescription}>
          Are you sure you wish to sign out?
        </Box>
        <Box className={styles.signOutModalFooter}>
          <CustomButton
            className={styles.btn}
            onClick={props.onCloseClick}
            type="outlinedDefault"
          >
            Cancel
          </CustomButton>
          <CustomButton
            className={styles.btn}
            onClick={props.onSignoutClick}
            loading={props.loading}
            type="danger"
          >
            Sign out
          </CustomButton>
        </Box>
      </Box>
    </Fade>
  </Modal>
);

SignOutModal.defaultProps = {
  open: false,
  onCloseClick: () => {},
  onSignoutClick: () => {},
};

SignOutModal.propTypes = {
  /** modal is open or not */
  open: PropTypes.bool,
  /** toggle modal */
  onCloseClick: PropTypes.func,
  /** sign out function */
  onSignoutClick: PropTypes.func,
};

export default SignOutModal;
