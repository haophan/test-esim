import { Select, Tooltip } from 'antd';

import classnames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import InputTitle from '../inputTitle';

import style from './style.module.scss';

const { Option } = Select;

const NormalSelect = (props) => {
  const selectRef = useRef(undefined);

  const {
    className, placeholder, data, title, // onChange,
    showSearch, defaultValue, isValueOutside,
    value, mode, suffixIcon, name,
    disabled, isObject,
  } = props;

  const onChange = (option) => {
    props.onChange(name, { label: option.label, value: option.value });
    selectRef.current.blur();
  };

  const onSearch = (search) => {
    props.onSearch(name, search);
  };

  return (
    <div
      className={classnames(style.selectWrapper, 'select-wrapper', className)}
      key={`select-ct-wrapper-${props.name}`}
    >
      <InputTitle
        title={title}
        isRequire={props.isRequire}
      />

      <Select
        className={style.selectComponent}
        labelInValue
        getPopupContainer={(trigger) => trigger.parentElement}
        disabled={disabled}
        ref={selectRef}
        suffixIcon={suffixIcon}
        mode={mode}
        defaultValue={defaultValue}
        value={isValueOutside ? []
          : isObject ? value || undefined
            : value}
        showSearch={showSearch}
        placeholder={placeholder}
        optionFilterProp="children"
        onChange={onChange}
        onSearch={showSearch ? onSearch : undefined}
        filterOption={(input, option) => {
          const normalizeInput = input.toLowerCase();
          const normalizeFullLabel = (option.children.toLowerCase());
          return normalizeFullLabel.includes(normalizeInput);
        }}
      >
        {
          isObject
            ? _.map(data, (x, i) => (
              <Option key={i} value={x.value}>{x.label}</Option>
            ))
            : _.map(data, (x, i) => (
              <Option key={i} value={x}>{x}</Option>
            ))
        }
      </Select>

      {props.errorMessage && <div className={style.errorMessage}>{props.errorMessage}</div>}
    </div>
  );
};

NormalSelect.defaultProps = {
  className: undefined,
  title: '',
  placeholder: 'Select...',
  data: [],
  onChange: () => { },
  onSearch: () => { },
  showSearch: true,
  defaultValue: undefined,
  isValueOutside: false,
  mode: undefined, // 'tags', // or multiple
  value: [],
  suffixIcon: undefined,
  name: '',
  disabled: false,
  isObject: false,
  isRequire: false,
};

NormalSelect.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  placeholder: PropTypes.string,
  data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.shape()),
  ]),
  onChange: PropTypes.func,
  onSearch: PropTypes.func,
  showSearch: PropTypes.bool,
  defaultValue: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.string,
  ]),
  isValueOutside: PropTypes.bool,
  mode: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.string,
    PropTypes.shape(),
  ]),
  suffixIcon: PropTypes.node,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  isObject: PropTypes.bool,
  isRequire: PropTypes.bool,
};

export default NormalSelect;
