import {
  CheckCircleFilled, CloseCircleFilled, ExclamationCircleFilled, InfoCircleFilled,
} from '@ant-design/icons';
import { notification } from 'antd';
import Image from 'next/image';
import React from 'react';
import cancelIcon from '../../../public/images/shop/svg/icon-close-coal-black-24.svg';

export const showErrorNotification = (message, description, icon) => {
  notification.destroy();
  notification.error({
    message,
    description,
    icon: icon ?? <CloseCircleFilled />,
    placement: 'bottomRight',
    closeIcon: <Image src={cancelIcon} alt="close-icon" width={14} height={14} />,
  });
};
export const showSuccessNotification = (message, description, icon) => {
  notification.destroy();
  notification.success({
    message,
    description,
    icon: icon ?? <CheckCircleFilled />,
    placement: 'bottomRight',
    closeIcon: <Image src={cancelIcon} alt="close-icon" width={24} height={24} />,
  });
};
export const showWarningNotification = (message, description, icon) => {
  notification.destroy();
  notification.warning({
    message,
    description,
    icon: icon ?? <ExclamationCircleFilled />,
    placement: 'bottomRight',
    closeIcon: <Image src={cancelIcon} alt="close-icon" width={24} height={24} />,
  });
};
export const showInfoNotification = (message, description, icon) => {
  notification.destroy();
  notification.info({
    message,
    description,
    icon: icon ?? <InfoCircleFilled />,
    placement: 'bottomRight',
    closeIcon: <Image src={cancelIcon} alt="close-icon" width={24} height={24} />,
  });
};
