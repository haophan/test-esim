import React, { useRef } from "react";
import NumberFormat from "react-number-format";
import { Box } from "@mui/material";
import classnames from "classnames";
import styles from "./styles.module.scss";
import CountryFlag from "../countryFlag";
import InputTitle from "../inputTitle";

interface IProps {
  className?: string;
  name: string;
  label: string;
  value: string | number;
  isRequire?: boolean;
  isError?: boolean;
  errorMessage?: string;
  isPhone?: boolean;
  isTax?: boolean;
  countryObject?: {
    value: string;
    dial: string;
  };
  id?: string;
  disabled?: boolean;
  allowNegative?: boolean;
  isUseOnChange?: boolean;
  placeholder?: string;
  format?: string;
  mask?: string;
  maxLength?: number;
  onChange: (name: string, value: any) => void;
  onBlur: () => void;
  getInputRef: (el: HTMLElement) => void;
}

const PhoneWithFlagInput = (props: IProps): JSX.Element => {
  const inputRef = useRef<HTMLInputElement>();
  const onChange = (value: any) => {
    props.onChange(props.name, value);
  };

  const onFocus = () => {
    if (inputRef.current) {
      inputRef.current.style.borderColor = "#6EB43F";
    }
  };

  const onBlur = () => {
    if (inputRef.current) {
      inputRef.current.style.borderColor = "#F5F7FA";
    }
    props.onBlur();
  };

  return (
    <Box className={classnames(styles.phoneWithFlagInput, props.className)}>
      {props.label && (
        <InputTitle
          title={props.label}
          isRequire
        />
      )}
      <Box
        className={classnames(
          styles.phoneWithFlagInputContainer,
          props.isError && !!props.errorMessage
            ? styles.phoneWithFlagInputContainerError
            : ""
        )}
        ref={inputRef}
      >
        <Box className={styles.phoneWithFlag}>
          <CountryFlag countryCode={props.countryObject?.value} />
          {props.isPhone && (
            <Box
              className={styles.phoneWithFlagDial}
            >{`+${props.countryObject?.dial}`}</Box>
          )}
          {props.isTax && (
            <Box
              className={styles.phoneWithFlagDial}
            >{`${props.countryObject?.dial}`}</Box>
          )}
        </Box>
        <NumberFormat
          // getInputRef={(el: any) => {
          //   props.getInputRef(el);
          // }}
          className={classnames(
            styles.phoneWithFlagInputItem,
            props.disabled && styles.phoneWithFlagInputDisabled
          )}
          value={props.value}
          disabled={props.disabled}
          id={props.id}
          allowNegative={props.allowNegative}
          onValueChange={(e) => {
            if (props.isUseOnChange) return;
            if (!e.floatValue && (e.value === "." || e.value === "")) {
              onChange(e.value);
            } else {
              onChange(e.floatValue);
            }
          }}
          onChange={(e: any) => {
            if (props.isUseOnChange) {
              onChange(e.target.value);
            }
          }}
          onBlur={onBlur}
          onFocus={onFocus}
          placeholder={props.placeholder}
          autoComplete="off"
          mask={props.mask}
          maxLength={props.maxLength}
          format={props.format}
        />
      </Box>
      {props.isError && !!props.errorMessage && (
        <Box className={styles.phoneWithFlagInputErrorMessage}>
          {props.errorMessage}
        </Box>
      )}
    </Box>
  );
};


export default PhoneWithFlagInput;
