import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import { Box } from '@mui/material';
import styles from './styles.module.scss';

interface IProps {
  /** override className */
  className: string,
  /** original product price */
  originalPrice: number,
  /** sale off rate */
  discount?: number,
  /** final price */
  finalPrice?: number,
  /** is main product -> big size */
  mainProduct?: boolean,
  /** is main home product -> big size */
  mainHomeProduct?: boolean,
}

const PriceTag = (props: IProps): JSX.Element => {
  if (!props.discount) {
    return (
      <Box className={classnames(styles.priceTag ,props.className)}>
        <h2 className={classnames(styles.priceTagNormal,
          props.mainProduct ? styles.mainProductSize : undefined,
          props.mainHomeProduct ? styles.mainHomeProductSize : undefined
        )}>{`$${props.originalPrice}`}</h2>
      </Box>

    );
  }
  // const newPrice = ((1 - props.saleOffRate) * props.originalPrice).toFixed(2);
  return (
    <Box className={classnames(styles.priceTag, props.className)}>
      <h2 className={classnames(styles.afterPrice,
        props.mainProduct ? styles.mainProductSize : undefined,
        props.mainHomeProduct ? styles.mainHomeProductSize : undefined
      )}>
        {`$${props.finalPrice}`}
        <span className={styles.originalPrice}>{`$${props.originalPrice}`}</span>
        <span className={styles.saleOffTag}>{`${props.discount}%`}</span>
      </h2>

    </Box>
  );
};

PriceTag.defaultProps = {
  className: '',
  originalPrice: 0,
  saleOffRate: 0,
};

PriceTag.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** original product price */
  originalPrice: PropTypes.number,
  /** sale off rate */
  saleOffRate: PropTypes.number,
};

export default PriceTag;
