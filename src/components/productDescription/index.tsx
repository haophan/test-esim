import React from "react";
import classnames from "classnames";

import { Box } from "@mui/material";
import IProductOption from "../../modals/productOptionInterface";
import IProductOptionDescription from "../../modals/productOptionsDescriptionInterface";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";

import styles from "./styles.module.scss";
import IProductComponent from "../../modals/productComponentInterface";
import IBundleItem from "../../modals/bundleItemInterface";
import IBundle from "../../modals/bundleInterface";
import IBundleItemDescription from "../../modals/bundleItemDescriptionInterface";

interface IProps {
  className: string;
  title: string;
  is_bundle?: boolean;
  isHome?: boolean;
  bundle: IBundle;
}

const ProductDescription = (props: IProps): JSX.Element => {
  return (
    <Box className={classnames(styles.bundle, props.className)}>
      <h1 className={styles.bundleTitle}>{props.bundle.title}</h1>
      {props.bundle.bundle_items.map(
        (bundleItem: IBundleItem, index: number) => {
          return (
            <Box key={bundleItem.id} className={styles.bundleItem}>
              <img
                className={styles.bunleItemIcon}
                src={bundleItem.icon.url}
                alt={bundleItem.title}
              />
              <p className={styles.bundleItemTitle}>{bundleItem.title}</p>
              {bundleItem.bundle_item_descriptions.map(
                (
                  bundleItemDescription: IBundleItemDescription,
                  index: number
                ) => {
                  return (
                    <Box key={bundleItemDescription.id} className={styles.bunleItemDescription}>
                      <ArrowRightAltIcon
                        classes={{ root: styles.rightArrowIcon }}
                      />
                      <p className={styles.descriptionText}>
                        {bundleItemDescription.title}
                      </p>
                    </Box>
                  );
                }
              )}
            </Box>
          );
        }
      )}
      {/* {props.productComponents &&
        props.productComponents.map(
          (productOption: IProductComponent, index: number) => (
            <Box
              key={`${index}${productOption.id}`}
              className={styles.productOptions}
            >
              <Box className={styles.productOptionRow}>
                <img
                  className={styles.productOptionIcon}
                  src={productOption.icon.url}
                  alt={productOption.title}
                />
                <p className={styles.productOptionText}>
                  {productOption.title}
                </p>
              </Box>
              {productOption.bundle_item_descriptions &&
                productOption.bundle_item_descriptions.map(
                  (description: IProductOptionDescription) => (
                    <Box key={description.id} className={styles.descriptionRow}>
                      <ArrowRightAltIcon
                        classes={{ root: styles.rightArrowIcon }}
                      />
                      <p className={styles.descriptionText}>
                        {description.title}
                      </p>
                    </Box>
                  )
                )}
            </Box>
          )
        )} */}
    </Box>
  );
};

ProductDescription.defaultProps = {
  className: "",
  productName: "",
  description: [
    "Bioheart monitor with 3 ECG channels",
    "Bioheart monitor with 3 ECG channels",
    "Bioheart monitor with 3 ECG channels",
    "Unlimited storage and access to your data in the app & on Biosphere",
  ],
};

export default ProductDescription;
