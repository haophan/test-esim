import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import { Box, IconButton } from '@mui/material';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import styles from './styles.module.scss';

interface IProps {
  className: string,
  quantity: number,
  onChangeQuantity: (isAdd: boolean) => void
}

const SetQuantity = (props: IProps) => {
  const onChangeQuantity = (isAdd = false) => {
    if (isAdd) {
      props.onChangeQuantity(isAdd);
    } else {
      props.onChangeQuantity(isAdd);
    }
  };
  return (
    <Box className={classnames(styles.setQuantity, props.className)}>
      <IconButton
        onClick={() => onChangeQuantity(false)}
        disabled={props.quantity === 1}
        classes={{ root: styles.adjustBtn }}
      >
        <RemoveIcon classes={{ root: styles.adjustIcon }} />
      </IconButton>
      <p>{props.quantity}</p>
      <IconButton
        onClick={() => onChangeQuantity(true)}
        classes={{ root: styles.adjustBtn }}
      >
        <AddIcon classes={{ root: styles.adjustIcon }} />
      </IconButton>
    </Box>
  );
};

SetQuantity.defaultProps = {
  className: '',
  quantity: 1,

  onChangeQuantity: () => {},
};

SetQuantity.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** current quantity */
  quantity: PropTypes.number,

  /** on change quantity event */
  onChangeQuantity: PropTypes.func,
};

export default SetQuantity;
