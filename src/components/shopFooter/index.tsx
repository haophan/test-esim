import classnames from 'classnames';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';

import { Box } from '@mui/material';
import Image from 'next/image';
import fullLogoWhite from '../../../public/images/shop/svg/full-logo-white.svg';

import styles from './styles.module.scss';
import moment from 'moment';
import Link from 'next/link';
import { footerOptions } from './helper';
import _ from 'lodash';

interface IProps {
  className: string
}

const ShopFooter = (props: IProps): JSX.Element => {
  const mobileOptions = useMemo(() => footerOptions(true), []);
  const webOptions = useMemo(() => footerOptions(), []);

  return (
    <div className={classnames(styles.shopFooter, props.className)}>
      <Box className={styles.moreInfo}>
        <Box className={styles.fullLogo}>
          <img src={fullLogoWhite} width={150} height={40} alt="full-logo" />
        </Box>
        <Box className={styles.verticalDivider} />
        <Box className={styles.discoveries}>
          {_.map(mobileOptions, (item, index) => (
            <Link href={item.href} key={`mobile-option-${index}`}>
              <p>{item.label}</p>
            </Link>
          ))}
        </Box>
        <Box className={styles.alternativeDiscoveries}>
          {_.map(webOptions, (item, index) => (
            <Link href={item.href} key={`web-option-${index}`}>
              <p>{item.label}</p>
          </Link>
          ))}
        </Box>
      </Box>
      <Box className={styles.copyright}>
        <p>
          {`© ${moment().year()} Biotricity All rights reserved. This website uses cookies to enhance the user experience.`}
        </p>
      </Box>
    </div>
  );
};

ShopFooter.defaultProps = {
  className: '',
};

ShopFooter.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default ShopFooter;
