import MenuIcon from '@mui/icons-material/Menu';
import { IconButton } from '@mui/material';
import classnames from 'classnames';
import Image from 'next/image';
import Link from 'next/link';
import { NextRouter, useRouter } from 'next/router';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import fullLogo from '../../../public/images/shop/svg/full-logo.svg';
import miniLogo from '../../../public/images/shop/svg/mini-logo.svg';
import shoppingCartIcon from '../../../public/images/shop/svg/shopping-cart.svg';
import { logout } from '../../utils';
import { auth } from '../../utils/auth';
import CustomButton from '../buttons/customButton';
import CartInfoPopup from '../cartInfoPopup';
import CartDrawer from '../drawers/cartDrawer';
import MoreActionsDrawer from '../drawers/moreActionsDrawer';
import MoreActionsMenu from '../menues/moreActionsMenu';

import styles from './styles.module.scss';

interface IProps {
  className: string,
}

interface CartProduct {
  id: number;
  description: string;
  quantity: number;
  originalPrice: number;
  saleOffRate: number;
  size: string | undefined;
}

interface IReduxState {
  cart: CartProduct[]
}

const ShopHeader = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();

  const cart: any = useSelector<IReduxState>((state) => state.cart);

  const isLoggedIn: boolean = auth.isLoggedIn();

  const [isOpenCartPanel, setOpenCartPanel] = useState<boolean>(false);
  const [isOpenMorePanel, setOpenMorePanel] = useState<boolean>(false);
  const [morePanelAnchor, setMorePanelAnchor] = useState<null | HTMLElement>(null);

  const toggleCartPanel = (event: React.KeyboardEvent | React.MouseEvent) => {
    setOpenCartPanel(!isOpenCartPanel);
  };

  const toggleMorePanel = (event: React.MouseEvent<HTMLButtonElement>) => {
    setMorePanelAnchor(event.currentTarget);
    setOpenMorePanel(!isOpenMorePanel);
  };

  const onSignInClick = () => {
    router.push('/login');
  };

  const onSignOutClick = async () => {
    await logout();
  }

  return (
    <div className={classnames(styles.shopHeader, props.className)}>
      <div className={styles.shopHeaderWrapper}>
        <div className={styles.miniLogo}>
          <Link href="/home">
            <img src={miniLogo} width={24} height={24} alt="mini-logo" />
          </Link>
        </div>
        <div className={styles.fullLogo}>
          <Link href="/home">
            <img src={fullLogo} width={150} height={40} alt="full-logo" />
          </Link>
        </div>
        <div className={styles.actions}>
          {/* <IconButton
            className={styles.cartBtn}
            onClick={toggleCartPanel}
          >
            <Image src={shoppingCartIcon} width={24} height={24} alt="cart-icon" />
            {cart.products.length !== 0 && (
            <div className={styles.inCartNumber}>
              {cart.products.length}
            </div>
            )}
          </IconButton> */}
          <CartInfoPopup
            togglePanel={toggleCartPanel}
            isOpenCartPanel={isOpenCartPanel}
          />
          {isLoggedIn ? (
            <IconButton
              onClick={toggleMorePanel}
            >
              <MenuIcon />
            </IconButton>
          ) : (
            <CustomButton
              className={styles.signInBtn}
              type="outlined"
              onClick={onSignInClick}
            >
              Sign in
            </CustomButton>
          )}
        </div>
        <MoreActionsDrawer
          className={styles.moreActionsDrawer}
          open={isOpenMorePanel}
          togglePanel={toggleMorePanel}
          onSignOut={onSignOutClick}
        />
        <MoreActionsMenu
          className={styles.moreACtionsMenu}
          open={isOpenMorePanel}
          anchorEl={morePanelAnchor}
          onClose={toggleMorePanel}
          onSignOut={onSignOutClick}
        />
        <CartDrawer
          className={styles.cartDrawer}
          paperClassName={styles.cartDrawerPaper}
          open={isOpenCartPanel}
          togglePanel={toggleCartPanel}
        />
      </div>
    </div>
  );
};

ShopHeader.defaultProps = {
  className: '',
  isLoggedIn: true,
};

ShopHeader.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** is logged or not */
  isLoggedIn: PropTypes.bool,
};

export default ShopHeader;
