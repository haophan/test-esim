export const SIZE_DUMP = Object.freeze([
  {
    value: 'small',
    label: 'Small',
    description: '25.5" - 33.5" | 60cm - 80cm',
  },
  {
    value: 'medium',
    label: 'Medium',
    description: '25.5" - 33.5" | 60cm - 80cm',
  },
  {
    value: 'large',
    label: 'Large',
    description: '25.5" - 33.5" | 60cm - 80cm',
  },
]);
