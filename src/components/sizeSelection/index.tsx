import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import { Box } from "@mui/material";
import { SIZE_DUMP } from "./helper";
import SizeOption from "./sizeOption";

import styles from "./styles.module.scss";
import IProductOption from "../../modals/productOptionInterface";
import IProductVariation from "../../modals/productVariationInterface";
import { AnyAaaaRecord } from "dns";

interface IProps {
  className: string;
  selectedOption?: { id: number; variation?: IProductVariation };
  productOption: IProductOption;
  onChangeSize: (e: any, selectedOption: any) => void;
}

const SizeSelection = (props: IProps): JSX.Element => {
  const onChangeSize = (e: any, selectedVariation: any) => {
    props.onChangeSize(e, {
      id: props.productOption.id,
      variation: selectedVariation,
    });
  };
  return (
    <Box className={classnames(styles.sizeSelection, props.className)}>
      <h3 className={styles.chooseSizeInstructionTitle}>
        {`Choose ${props.productOption.title}`}
      </h3>
      <p className={styles.chooseSizeInstructionContent}>
        Measure your waist to pick the right size
      </p>
      <Box className={styles.sizeOptions}>
        {props.productOption.product_variations &&
          props.productOption.product_variations.map(
            (variation: IProductVariation, index: number) => (
              <Box className={styles.sizeOption} key={`${index}${variation.id}`}>
                <SizeOption
                  key={`${index}${variation.id}`}
                  variation={variation}
                  selectedVariation={props?.selectedOption?.variation}
                  onChangeSize={onChangeSize}
                />
                {props.productOption.product_variations &&
                  index !==
                    props.productOption.product_variations.length - 1 && (
                    <Box className={styles.optionDivider} />
                  )}
              </Box>
            )
          )}
      </Box>
    </Box>
  );
};

SizeSelection.defaultProps = {
  className: "",

  chooseSizeTitle: "Choose strap size",
  chooseSizeContent: "Measure your waist to pick the right size",
  options: SIZE_DUMP,
};

SizeSelection.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** product type */
  chooseSizeTitle: PropTypes.string,
  // /** product type instruction */
  // chooseSizeContent: PriceChange.string,
  // /** options */
  // options: PropTypes.arrayOf(PropTypes.shape()),
};

export default SizeSelection;
