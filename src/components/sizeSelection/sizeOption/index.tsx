import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import { Box, Radio } from "@mui/material";
import styles from "./styles.module.scss";
import IProductVariation from "../../../modals/productVariationInterface";

interface IProps {
  className: string;
  variation: IProductVariation;
  selectedVariation?: IProductVariation;
  onChangeSize: (e: any, selectedVariation?: IProductVariation) => void;
}

const SizeOption = (props: IProps): JSX.Element => {
  const onChangeVariation = (e: any) => {
    props.onChangeSize(e, props.variation);
  };

  return (
    <Box className={classnames(styles.sizeOption, props.className)}>
      <Box className={styles.sizeOpt}>
        <p
          className={classnames(
            styles.sizeName,
            props.selectedVariation &&
              props.selectedVariation.id === props.variation.id
              ? styles.bold
              : ""
          )}
        >
          {props.variation.fullname}
        </p>
        <Radio
          key={props.variation.id}
          value={props.variation.id}
          name="size"
          checked={
            props.selectedVariation
              ? props.selectedVariation.id === props.variation.id
              : false
          }
          onChange={onChangeVariation}
        />
      </Box>
      <p className={styles.sizeInstruction}>{props.variation.description}</p>
    </Box>
  );
};

SizeOption.defaultProps = {
  className: "",
  size: {},
  selectedSize: "",
  onChangeSize: () => {},
};

SizeOption.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default SizeOption;
