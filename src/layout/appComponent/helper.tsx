export const ProtectedRoutes = ['/complete-profile', '/overview', '/summary/heart-rate', '/summary/heart-rate-variability', '/summary/resting-heart-rate', '/summary/active-minutes', '/summary/high-heart-rate-notification', '/summary/low-heart-rate-notification', '/rhythm-diary', '/health-report', '/support', '/_error'];
export const ProtectedRoutesLoggedIn = ['/login', '/forgot-password', '/create-account', '/_error'];
export const allRoutes = [
  '/',
  '/home',
  '/checkout',
  '/signup',
  '/login',
  '/products/[slug]',
  '/forgot-password',
  '/payment-result',
  '/verify-email',
]

export const isValidRoute = (pathname: string): boolean => {
  return allRoutes.includes(pathname)
}
