/* eslint-disable react/jsx-no-useless-fragment */
import { NextRouter, useRouter } from "next/router";
import React, { useEffect } from "react";
import { auth } from "../../utils/auth";
import { isValidRoute } from "./helper";

const AppComponent = (): JSX.Element => {
  // app component will check account is delete or not
  // listen to socker onAccount delete
  const router: NextRouter = useRouter();
  const isLoggedIn: boolean = auth.isLoggedIn();

  useEffect(() => {
    if (!isValidRoute(router.pathname)) {
      router.push("/home");
    }
  }, [router.isReady]);
  
  return <></>;
};

AppComponent.defaultProps = {};

AppComponent.propTypes = {};

export default AppComponent;
