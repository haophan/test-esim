import classnames from 'classnames';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { Box } from '@mui/material';
import CustomButton from '../../../components/buttons/customButton';
import CustomInput from '../../../components/inputs/customInput';
import { isValidEmail } from '../../../utils';
import styles from './styles.module.scss';
import Auth from '@aws-amplify/auth';

interface IProps {
  className: string,
  onChangeStep: (step: number) => void
  onStep1Continue: (email: string) => void,
}

interface IState {
  email: string ,
  errorMessage: string
}

const FindEmailStep = (props: IProps): JSX.Element => {
  const [state, setState] = useState<IState>({
    email: '',
    errorMessage: '',
  });

  const [loading, setLoading] = useState<boolean>(false)

  const onChangeInput = (name: string, value: string) => {
    if (state.errorMessage) {
      setState({ ...state, [name]: value, errorMessage: '' });
      return;
    }
    setState({ ...state, [name]: value });
  };

  const onChangeStep = (step: number = 0) => {
    props.onChangeStep(2);
  };

  const onSendRequest = async() => {
    if (!isValidEmail(state.email)) {
      setState({ ...state, errorMessage: 'Invalid email.' });
      return;
    }
    try {
      setLoading(true)
      await Auth.forgotPassword(state.email.trim());
      props.onStep1Continue(state.email)
      onChangeStep(0);
    }
    catch (error:any) {
      setLoading(false)
    }
    // call API to check is existed email
    // if not existed --> show error
  };

  return (
    <Box className={classnames(styles.findEmailStep, props.className)}>
      <p className={styles.findEmailInstruction}>
        Please enter your email in the field below to send the reset password request.
      </p>
      <CustomInput
        containerClassName={styles.findEmailInput}
        name="email"
        title="Email"
        value={state.email}
        placeholder="Enter your email here"
        onChange={onChangeInput}
        isError={!!state.errorMessage}
        errorMessage={state.errorMessage}
      />
      <CustomButton
        className={styles.sendRequestBtn}
        disabled={state.email.length === 0 || state.errorMessage.length !== 0}
        fullWidth
        onClick={onSendRequest}
        loading={loading}
      >
        Send Request
      </CustomButton>
    </Box>
  );
};

FindEmailStep.defaultProps = {
  className: '',
  onChangeStep: () => {},
};

FindEmailStep.propTypes = {
  /** override className */
  className: PropTypes.string,
  /** on send request event */
  onChangeStep: PropTypes.func,
};

export default FindEmailStep;
