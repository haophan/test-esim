import Auth from "@aws-amplify/auth";
import _ from "lodash";

const handleForgotPasswordSubmitError = (error: any) => {
  const resultObject = {
    errorObject: {
    },
    errorMessage: 'Error',
  };
  const { code, message } = error;
  switch (code) {
    case 'UserNotFoundException':
    case 'LimitExceededException':
    case 'ExpiredCodeException':
      _.assign(resultObject, {
        errorMessage: message,
      });
      break;
    case 'CodeMismatchException':
      _.assign(resultObject.errorObject, {
        verifyCode: message,
      });
      break;
    case 'NetworkError':
      _.assign(resultObject, {
        errorMessage: 'Network error',
      });
      break;
    default:
      break;
  }
  return resultObject;
};

export const handleForgotPasswordSubmit = async ({ email, code, password }: any) => {
  try {
    if (_.isEmpty(email?.trim()) || _.isEmpty(code) || _.isEmpty(password?.trim())) {
      return handleForgotPasswordSubmitError({ code: 'Error', message: 'Error' });
    }
    await Auth.forgotPasswordSubmit(email.trim(), code, password.trim());
    return { isSuccess: true };
  } catch (error) {
    return handleForgotPasswordSubmitError(error);
  }
};