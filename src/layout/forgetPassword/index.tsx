import { Box } from '@mui/material';
import classnames from 'classnames';
import { NextRouter, useRouter } from 'next/router';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import LoginHeader from '../../components/loginHeader';

import FindEmailStep from './findEmailStep';
import styles from './styles.module.scss';
import UpdatePasswordStep from './updatePasswordStep';

interface IProps {
  className: string,
}

const ForgetPassword = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();

  const [currentStep, setCurrentStep] = useState<number>(2);

  const emailRef = useRef<string>('')

  const onChangeStep = (step: number = 1): void => {
    setCurrentStep(step);
  };

  const onStep1Continue = (email: string): void => {
    emailRef.current = email;
  }

  const onPasswordUpdated = () => {
    router.push('/login');
  }

  return (
    <Box className={classnames(styles.forgetPasswordPage, props.className)}>
      <LoginHeader
        className={styles.loginHeader}
        title={currentStep === 1 ? 'Forget your password' : 'Update your password'}
      />
      <Box className={styles.forgetPasswordForm}>
        {currentStep === 1 && (
          <FindEmailStep
            className={styles.forgetPasswordZone}
            onChangeStep={onChangeStep}
            onStep1Continue={onStep1Continue}
          />
        )}
        {currentStep === 2 && (
          <UpdatePasswordStep
            className={styles.forgetPasswordZone}
            currentEmail={emailRef.current}
            onPasswordUpdated={onPasswordUpdated}
          />
        )}
      </Box>
    </Box>
  );
};

ForgetPassword.defaultProps = {
  className: '',
};

ForgetPassword.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default ForgetPassword;
