import classnames from 'classnames';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { Box, Divider } from '@mui/material';
import _ from 'lodash';
import Image from 'next/image';
import iconPaper from '../../../../public/images/shop/png/iconPaper.jpg';
import CustomButton from '../../../components/buttons/customButton';
import CountDownComponent from '../../../components/countDownComponent';
import CustomInput from '../../../components/inputs/customInput';
import { handleForgotPasswordSubmit } from '../helper';
import styles from './styles.module.scss';

interface IProps {
  className: string,
  currentEmail: string,
  onPasswordUpdated: () => void
}

interface IState {
  code: string,
  password: string,
  confirmPassword: string,
  codeErrorMessage: string,
  passwordErrorMessage: string,
}

const UpdatePasswordStep = (props: IProps): JSX.Element => {
  const [state, setState] = useState<IState>({
    code: '',
    password: '',
    confirmPassword: '',
    codeErrorMessage: '',
    passwordErrorMessage: '',
  });

  const [isCountDown, setCountDown] = useState<boolean>(false);

  const [loading, setLoading] = useState<boolean>(false);

  const toggleCountDown = () => {
    setCountDown(!isCountDown);
  };

  // const isDisabledBtn = (): boolean => {
  //   return state.password.length === 0 || state.confirmPassword.length === 0 || state.errorMessage.length !== 0 || state.password.length < 8 || state.confirmPassword.length < 8
  // }
  const onChangeInput = (name: string, value: string) => {
    if (state.codeErrorMessage || state.passwordErrorMessage) {
      setState({ ...state, [name]: value, codeErrorMessage: '', passwordErrorMessage: '' });
      return;
    }
    setState({ ...state, [name]: value });
  };

  const onUpdatePasswordClick = async () => {
    if (state.password !== state.confirmPassword) {
      setState({ ...state, passwordErrorMessage: 'Passwords are not matched.' });
      return;
    }

    const { errorMessage, errorObject, isSuccess = false }: any = await handleForgotPasswordSubmit({
      email: props.currentEmail.trim(),
      code: state.code,
      password: state.password,
    });
    if (errorMessage === 'Network error') {
      // showErrorNotification('No internet connection', 'Please check your connection and try again');
      setLoading(false);
      return;
    }

    if (isSuccess) {
      props.onPasswordUpdated();
    } else if (!_.isEmpty(errorObject)) {
      setLoading(false);
      setState({ ...state, codeErrorMessage: 'Wrong verification code. Please type again.' });
    }
  };

  return (
    <Box className={classnames(styles.updatePassword, props.className)}>
      <Box className={styles.updatePasswordInstruction}>
        <Image src={iconPaper} alt="Paper icon" width={48} height={48} />
        <span className={styles.updatePasswordInstructionContent}>
          Please enter the verification code that we sent to your email:
        </span>
      </Box>
      <CustomInput
        containerClassName={styles.verificationCodeInput}
        title=""
        name="code"
        value={state.code}
        onChange={onChangeInput}
        placeholder="Enter verification code"
        isError={!!state.codeErrorMessage}
        errorMessage={state.codeErrorMessage}
      />
      <Box className={styles.resendCode}>
        <p>You didn’t receive the code?</p>
        {isCountDown ? (
          <h4 className={classnames(styles.resendText, isCountDown ? styles.countdowning : '')}>
            {'Resend '}
            <span><CountDownComponent seconds={59} onStopCountDown={toggleCountDown} /></span>
          </h4>
        ) : (
          <Box
            className={styles.resendBtn}
            onClick={toggleCountDown}
          >
            Resend now
          </Box>
        )}
      </Box>
      <Divider className={styles.updatePasswordDivider} />
      <p className={styles.passwordInstruction}>
        Passwords must be at least 8 characters.
      </p>
      <CustomInput
        containerClassName={styles.passwordInput}
        name="password"
        title="Password"
        value={state.password}
        onChange={onChangeInput}
        placeholder="Enter your password"
        disableUnderline
        type="password"
      />
      <CustomInput
        containerClassName={styles.confirmPasswordInput}
        name="confirmPassword"
        title="Confirm password"
        value={state.confirmPassword}
        onChange={onChangeInput}
        placeholder="Re-enter your password"
        disableUnderline
        type="password"
        isError={!!state.passwordErrorMessage}
        errorMessage={state.passwordErrorMessage}
      />
      <Box className={styles.updatePasswordFooter}>
        <CustomButton
          className={styles.updateBtn}
          fullWidth
          // disabled={isDisabledButton(state)}
          loading={loading}
          onClick={onUpdatePasswordClick}
        >
          Update
        </CustomButton>
      </Box>
    </Box>
  );
};

UpdatePasswordStep.defaultProps = {
  className: '',
};

UpdatePasswordStep.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default UpdatePasswordStep;
