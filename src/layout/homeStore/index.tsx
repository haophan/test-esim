//@ts-nocheck
import React from 'react';
import classnames from "classnames";

import { Box } from "@mui/material";
import BriefProduct from "../../components/briefProduct";
import BriefProductCard from "../../components/briefProductCard";
import IBriefProductCard from "../../modals/briefProductCardInterface";
import IBriefProduct from "../../modals/briefProductInterface";

import styles from "./styles.module.scss";

interface IProps {
  className?: string;
  homeProduct?: IBriefProduct;
  relationProducts?: IBriefProductCard[];
}
const HomeStore = (props: IProps): JSX.Element => {
  // home store will list products and offered products
  return (
    <div className={classnames(styles.homeStore, props.className)}>
      <BriefProduct product={props.homeProduct} isHome />
      {props.relationProducts && props.relationProducts.length !== 0 && (
        <Box className={styles.offeredItems}>
          <p className={styles.offeredSentence}>
            We also offer individual items too
          </p>
          <Box className={styles.items}>
            {props.relationProducts.map((product: IBriefProductCard) => {
              return <BriefProductCard key={product.id} product={product} />;
            })}
          </Box>
        </Box>
      )}
    </div>
  );
};

export default HomeStore;
