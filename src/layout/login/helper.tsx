import { Auth } from "@aws-amplify/auth";
import { createUser } from "../../rest/post/createUser";
import { auth } from "../../utils/auth";

export const handleAfterSignIn = async (
  user: any
): Promise<{
  isSuccess: boolean;
  loginData?: { accessToken: string };
  errorMessage?: string;
}> => {
  if (
    user?.signInUserSession?.accessToken?.jwtToken &&
    user?.signInUserSession?.idToken?.jwtToken
  ) {
    const createStrapiUser = await createUser({
      cognitoIdToken: user?.signInUserSession?.idToken?.jwtToken || "",
      cognitoAccessToken: user?.signInUserSession?.accessToken?.jwtToken || "",
    });
    if (createStrapiUser.isSuccess) {
      return {
        isSuccess: true,
        loginData: createStrapiUser.loginData,
      };
    }
    return {
      isSuccess: false,
      errorMessage: createStrapiUser.errors
        ? createStrapiUser.errors[0].message
        : "Unknown error",
    };
  }
  return {
    isSuccess: false,
    errorMessage: "Unknown error",
  };
};

export const handleSignIn = async (email: string, password: string) => {
  try {
    const user = await Auth.signIn(email, password);
    const afterCognitoSignin = await handleAfterSignIn(user);
    if (afterCognitoSignin?.isSuccess) {
      auth.setLoginData(afterCognitoSignin.loginData);
      return {
        isSuccess: true,
      };
    } else {
      return {
        isSuccess: false,
        error: afterCognitoSignin?.errorMessage,
      };
    }
  } catch (error: any) {
    return {
      isSuccess: false,
      error: error.message,
    };
  }
};
