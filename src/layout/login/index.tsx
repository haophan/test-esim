//@ts-nocheck
import classnames from "classnames";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";

import { Box, Link } from "@mui/material";
import Image from "next/image";
import { NextRouter, useRouter } from "next/router";
import appleIcon from "../../../public/images/shop/svg/apple-icon.svg";
import facebookIcon from "../../../public/images/shop/svg/facebook-icon.svg";
import googleIcon from "../../../public/images/shop/svg/google-icon.svg";
import CustomButton from "../../components/buttons/customButton";
import CustomInput from "../../components/inputs/customInput";
import { isValidEmail } from "../../utils";
import { handleSignIn } from "./helper";
import styles from "./styles.module.scss";

import LoginHeader from "../../components/loginHeader";
import { Hub } from "@aws-amplify/core";
import Auth from "@aws-amplify/auth";
import { createUser } from "../../rest/post/createUser";
import { auth } from "../../utils/auth";

interface IProps {
  className: string;
}

interface IState {
  username: string;
  password: string;
  errorMessage: string;
}

const Login = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();
  const [state, setState] = useState<IState>({
    username: "",
    password: "",
    errorMessage: "",
  });
  const [loading, setLoading] = useState<boolean>(false);

  const onChangeInput = (name: string, value: string) => {
    if (state.errorMessage) {
      setState({ ...state, [name]: value, errorMessage: "" });
      return;
    }
    setState({ ...state, [name]: value });
  };

  const onSignIn = async () => {
    setLoading(true);
    if (!isValidEmail(state.username)) {
      setState({ ...state, errorMessage: "Invalid email." });
      return;
    }
    const result: any = await handleSignIn(state.username, state.password);
    if (result?.isSuccess) {
      if (Object.keys(router.query).includes("fromBuynowCheckout")) {
        if (router.query.fromBuynowCheckout === "true") {
          router.push("/checkout?buyNow=true");
        } else {
          router.push("/checkout");
        }
      } else {
        router.push("/home");
      }
    } else {
      if (result.error && result.error.includes("User is not confirmed.")) {
        router.push(`/verify-email/?email=${state.username}`);
      } else {
        setState({ ...state, errorMessage: result.error });
      }
    }
    setLoading(false);
  };

  const onSocialLoginClick = (socialType?: string) => {
    Auth.federatedSignIn({
      provider: socialType,
    });
  };

  const onCreateAccount = () => {
    router.push("/signup");
  };

  useEffect(() => {
    if (router.isReady) {
      Hub.listen("auth", async ({ payload: { event, data } }) => {
        if (event === "cognitoHostedUI") {
          const user = await Auth.currentSession();
          const createStrapiUser = await createUser({
            cognitoIdToken: user?.idToken?.jwtToken || "",
            cognitoAccessToken: user?.accessToken?.jwtToken || "",
          });
          if (createStrapiUser.isSuccess) {
            auth.setLoginData(createStrapiUser.loginData);
            router.push("/home");
          } else {
            setState({ ...state, errorMessage: "Can not login!" });
          }
        }
      });
    }
  }, [router.isReady]);

  return (
    <Box className={classnames(styles.loginPage, props.className)}>
      <LoginHeader className={styles.loginHeader} title="Sign in" />
      <Box className={styles.loginForm}>
        <Box className={styles.loginZone}>
          <CustomInput
            containerClassName={classnames(
              styles.loginInput,
              styles.usernameInput
            )}
            title="Email"
            name="username"
            type="text"
            placeholder="Enter your email"
            value={state.username}
            onChange={onChangeInput}
            errorMessage={state.errorMessage}
          />
          <CustomInput
            containerClassName={classnames(
              styles.loginInput,
              styles.passwordInput
            )}
            title="Password"
            name="password"
            type="password"
            placeholder="Enter your password"
            value={state.password}
            onChange={onChangeInput}
            errorMessage={state.errorMessage}
            isError={!!state.errorMessage}
          />
          <Box className={styles.textForgotPassword}>
            <Link href="/forgot-password">Forget password?</Link>
          </Box>
          <CustomButton
            disabled={
              state.username.length === 0 ||
              state.password.length === 0 ||
              state.errorMessage.length !== 0 ||
              state.password.length < 8 ||
              !isValidEmail(state.username)
            }
            className={styles.loginButton}
            type="primary"
            fullWidth
            htmlType="submit"
            onClick={onSignIn}
            loading={loading}
          >
            Sign in
          </CustomButton>
        </Box>
        <Box className={styles.orDivider}>
          <Box className={styles.horizontalDevider} />
          <p className={styles.orText}>Or sign in with</p>
        </Box>
        <Box className={styles.otherMethod}>
          <CustomButton
            className={classnames(styles.socialButton, styles.googleButton)}
            onClick={() => onSocialLoginClick("Google")}
            type="outlined"
            fullWidth
          >
            <Image src={googleIcon} width={40} height={40} alt="Google icon" />
          </CustomButton>
          <CustomButton
            className={classnames(styles.socialButton, styles.facebookButton)}
            onClick={() => onSocialLoginClick("Facebook")}
          >
            <Image
              src={facebookIcon}
              width={40}
              height={40}
              alt="Google icon"
            />
          </CustomButton>
          <CustomButton
            className={classnames(styles.socialButton, styles.appleButton)}
            onClick={() => onSocialLoginClick("SignInWithApple")}
          >
            <Image src={appleIcon} width={40} height={40} alt="Apple icon" />
          </CustomButton>
        </Box>
        <CustomButton
          className={styles.otherMethodDescrip}
          onClick={onCreateAccount}
        >
          Don&apos;t have an account? Create
        </CustomButton>
      </Box>
    </Box>
  );
};

Login.defaultProps = {
  className: "",
};

Login.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default Login;
