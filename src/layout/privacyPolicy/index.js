import { Box } from '@mui/material';
import React from 'react';
import styles from './styles.module.scss';

const PrivacyPolicyContent = () => (
  <Box className={styles.privacyPolicy}>
    <div className={styles.title}>Privacy policy</div>

    <Box className={styles.contentContainer}>
      <p>
        Biotricity Inc. (&quot;Biotricity&quot;, &quot;we&quot;, &quot;us&quot; or &quot;our&quot;) is committed to maintaining the privacy of your  information. By using our products, services, mobile apps and websites now or in the future, you agree to provide us with your information. This page is used to inform you regarding our policies with the collection, use, and disclosure of information. This Privacy Policy applies to all Biotricity mobile applications, as well as websites and domains of
        &nbsp;
        <a className={styles.email} href="https://biotricity.com" target="_blank" rel="noreferrer">
          www.biotricity.com
        </a>
        &nbsp;
        including
        &nbsp;
        <a className={styles.email} href="https://bioheart.com" target="_blank" rel="noreferrer">
          www.bioheart.com
        </a>
        &nbsp;
        and the Clinic (&quot;Sites&quot;).
      </p>
      <p>
        This Privacy Policy applies to the secured part of the site that requires a username and password to log-in. (&quot;Clinic&quot;). The Clinic is used by licensed healthcare providers (&quot;Healthcare Provider&quot;) who prescribe one of our devices or services. Only Biotricity and the Healthcare Provider will have access to this Clinic.  All information collected and stored by Biotricity or added by patients and Healthcare Providers on the Clinic is considered Protected Health Information (&quot;PHI&quot;) and/or medical information and is governed by applicable state and federal laws that apply to that information, specifically the Health Insurance Portability and Accountability Act (HIPAA).
      </p>
      <p>
        This Privacy Policy describes the types of information that we collect from visitors of our public and secured sites, and users of our mobile apps. The Policy describes our practices for using, maintaining, sharing and protecting information. Finally, this Policy describes the rights and choices you may have with respect to your information and how you may contact us regarding our Privacy Policy.
      </p>
      <div>
        <ol type="A">
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Information Collected</strong>
            <p>
              We collect information through three different sources: information you provide us, information we collect, and information provided by third parties.
            </p>
            <ol>
              <li className={styles.listTitleMain}>
                <strong>Information You Provide Us</strong>
                <ul>
                  <li>
                    <strong>Personal Information</strong>
                    &nbsp;
                    To set up an account on the Clinic or mobile application, personal information will be required such as your name, address, telephone number, email address, password, date of birth, and gender. You may also choose to provide other types of information such as a short biography and the health goals you wish to achieve.
                  </li>
                  <li>
                    <strong>Health Information</strong>
                    &nbsp;
                    Biotricity is committed to working with you to improve and maintain your health. So for a better experience in using our services, we may collect information like your blood pressure, height, weight, blood group and logs for food, weight change, sleep, water, medications, fitness, exercise information and other health vitals. In most cases you are not required to provide this information requested unless your Healthcare Provider advises otherwise.
                  </li>
                  <li>
                    <strong>Payment Information</strong>
                    &nbsp;
                    In purchasing devices from us, we will collect payment information, which includes your billing address and method of payment.
                  </li>
                </ul>
              </li>
              <li className={styles.listTitleMain}>
                <strong>Information we receive from your use of our products and services</strong>
                <ul>
                  <li>
                    <strong>Device Information</strong>
                    &nbsp;
                    We collect information of your device such as the device serial number.
                  </li>
                  <li>
                    <strong>Data from the Device</strong>
                    &nbsp;
                    Our devices may collect data like ECGs, the number of steps you take, your distance traveled, calories burned, oxygen, pulse rate, blood pressure, weight, heart rate, sleep stages, active minutes, glucose levels, and other health vitals. The data collected, and the accuracy of the data, varies depending on which device you use. When your device syncs with our applications or software, data recorded on your device is transferred from your device to our servers.
                  </li>
                  <li>
                    <strong>Geolocation Information</strong>
                    &nbsp;
                    Some of our devices may collect precise geolocation data, including GPS signals, device sensors, Wi-Fi access points, and cell tower IDs.  For some of our services, geolocation data is important for emergency purposes. However, other services may require you to grant us access to your location, in which case you can remove our access using your own mobile device settings. We may also derive your approximate location from your IP address.
                  </li>
                  <li>
                    <strong>Usage Information</strong>
                    &nbsp;
                    When you access or use our Sites or mobile applications, we receive certain usage or network activity information. This includes information about your interaction with our services, for example, when you install applications or software, create or log into your account, or pair your device to your account. Other information includes data relating to performance, crashes, diagnostic and other usage data.
                  </li>
                  <li>
                    <strong>Log Data</strong>
                    &nbsp;
                    In case of an error in the app, we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (&quot;IP&quot;) address, device name, operating system version, the configuration of the app when utilizing our service, the time and date of your use of our service, and other statistics.
                  </li>
                </ul>
              </li>
              <li className={styles.listTitleMain}>
                <strong>Information from Third Parties</strong>
                <ul>
                  <li>
                    <strong>Third Party Sign In:</strong>
                    &nbsp;
                    You may create an account and/or sign in to access some of our services using an account from a third party provider. Should you do so, we may collect some or all of the information that you share with the third party service provider in accordance with this Privacy Policy. You are responsible for managing the security of the third party account as Biotricity will not have access to the credentials of your third party account. You can stop sharing your information with us by removing our access to that other service.
                  </li>
                  <li>
                    <strong>Partnerships:</strong>
                    &nbsp;
                    We may partner with third parties, such as employers and insurance companies. In such cases, those companies may provide us with your name, email address, or other personal Information so that we can invite you to participate or determine your eligibility for particular benefits.
                  </li>
                </ul>
              </li>
            </ol>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>How We Use the Information</strong>
            <p>
              We may use the information you provide in the following ways:
            </p>
            <ol>
              <li className={styles.listTitleMain}>
                <strong>Diagnostic Services:</strong>
                &nbsp;
                Healthcare Providers may prescribe our devices and services to diagnose or treat medical issues you may have. The health information derived from these devices will be used by the Healthcare Provider to determine treatments for you.
              </li>
              <li className={styles.listTitleMain}>
                <strong>Improvement to the Services</strong>
                &nbsp;
                We use the information to improve our devices, apps, services and websites. For example, we may need the information to improve your personal dashboard that supports you in meeting your health goals. We also use the information to develop new products and services. Information is also valuable for us in order to fix errors, protection and security, conducting research and analysis, and improving your experience in using our services.
              </li>
              <li className={styles.listTitleMain}>
                <strong>Healthcare and Emergency:</strong>
                &nbsp;
                We may share information with your Healthcare Provider for diagnostic or post-diagnostic treatment in the event that you are suffering from any ailment or there are suspicions that you are suffering from an ailment. Information relating to your location is for emergency purposes.
              </li>
              <li className={styles.listTitleMain}>
                <strong>Communication with You</strong>
                &nbsp;
                Your information will allow us to send you notifications and respond to you when you contract us. We also use your information to promote new features or products that we think you would be interested in. You can control marketing communications and most service notifications by using your notification preferences in account settings on the mobile app or in the Clinic (if available).
              </li>
              <li className={styles.listTitleMain}>
                <strong>Protection and Security</strong>
                &nbsp;
                We use the information we collect to promote the safety and security of the services, our users, yourself and other parties.  Personal and payment information, for instance, helps us protect against fraud and abuse, respond to a legal request or claim, and secure transactions.
              </li>
              <li className={styles.listTitleMain}>
                <strong>Third Party Services</strong>
                &nbsp;
                We may employ third-party companies and individuals for the following reasons:
                <ul>
                  <li>
                    To facilitate our services;
                  </li>
                  <li>
                    To provide a service on our behalf;
                  </li>
                  <li>
                    To perform service-related services; or
                  </li>
                  <li>
                    To assist us in analyzing how our service is used.
                  </li>
                </ul>
                <p>
                  These third parties may have access to your information for the purposes of performing the tasks assigned to them on our behalf. These partners provide us with services for customer support, payments, sales, marketing, data analysis, research, and surveys. However, they are obligated not to disclose or use the information for any other purpose.  Any of your information provided to a Third Party will be de-identified.
                </p>
              </li>
              <li className={styles.listTitleMain}>
                <strong>Comply with Law</strong>
                &nbsp;
                We may also disclose information to comply with any applicable regulation, civil or criminal investigations, or a lawful government request.
              </li>
            </ol>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Data Retention</strong>
            <p>
              We keep any personal and health information for as long as it is required for our business purposes.
            </p>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Cookies</strong>
            <p>
              Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device&apos;s internal memory. On our Sites, we may collect information about your online activities for use in providing you with advertising about products and services tailored to your individual interests. You may see certain ads on other websites because we participate in advertising networks. Ad networks allow us to target our messaging to users through demographic, interest-based and contextual means. These networks track your online activities over time by collecting information through automated means, including through the use of cookies, web server logs, and web beacons. The networks use this information to show you advertisements that may be tailored to your individual interests. The information our ad networks may collect includes information about your visits to websites that participate in the relevant advertising networks, such as the pages or advertisements you view and the actions you take on the websites. This data collection takes place both on our Sites and on third-party websites that participate in the ad networks. This process also helps us track the effectiveness of our marketing efforts.
            </p>
            <p>
              Each type of web browser offers ways to restrict and delete cookies. For more information on how to manage cookies, you should consult your browser settings. Notwithstanding this, you may disable or delete browser cookies through your browser settings. Cookies generally are easy to disable or delete, but the method varies between browsers. If you disable or delete cookies, or if you are running third-party software that intercepts or deletes cookies, please note that some parts of our Sites may not work properly.
            </p>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Links to Other Sites</strong>
            <p>
              Our Sites may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.
            </p>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Children’s Privacy</strong>
            <p>
              Our services do not address anyone under the age of 13, or any higher minimum age in the jurisdiction where the child resides. We do not knowingly collect personally identifiable information from children under the minimum age. In the case we discover that a child under the minimum age has provided us with personal information without parental consent, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to take the necessary actions.
            </p>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Security of Personal Data</strong>
            <p>
              We secure your information through administrative, technical, and physical safeguards designed to protect against the risk of accidental, unlawful, or unauthorized destruction, loss, alteration, access, disclosure, or use.  If we should collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way.
            </p>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>International Data Transfers</strong>
            <p>
              The Sites are controlled and operated by us from the United States and are not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of the United States. Any information you provide to us through use of our Sites may be stored and processed, transferred between and accessed from the United States and other countries that may not guarantee the same level of protection of personal data as the one in which you reside. However, we will handle your personal information in accordance with this Privacy Policy regardless of where your personal information is stored/accessed.
            </p>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>California Privacy Disclosures</strong>
            <p>
              If you are a California resident, please review the following additional privacy disclosures under the California Consumer Privacy Act (&quot;CCPA&quot;).
            </p>
            <ol type="i">
              <li className={styles.listTitleMain}>
                <strong>How to Exercise Your Legal Rights</strong>
                <p>
                  You have the right to understand how we collect, use, and disclose your personal information, to access your information, to request that we delete certain information, and to not be discriminated against for exercising your privacy rights.
                </p>
                <p>
                  Provided our devices have not been prescribed to you by a Healthcare Provider, when you log onto your account on the mobile and access your account settings, you may exercise your right to access your personal information and to understand how we collect, use, and disclose it. Your account settings also let you exercise your right to delete personal information.
                </p>
                <p>
                  If any of our devices are prescribed to you, the information derived from you, your Healthcare Provider and these devices are considered Protected Health Information (&quot;PHI&quot;) and/or medical information and is governed the Health Insurance Portability and Accountability Act (HIPAA). You should refer to HIPAA for better understanding on how to exercise your legal rights.
                </p>
                <p>
                  If you need further assistance regarding your rights, please contact our
                  &nbsp;
                  <a className={styles.email} href="mailto:legal@biotricity.com" target="_blank" rel="noreferrer">
                    legal@biotricity.com
                  </a>
                  , and we will consider your request in accordance with applicable laws.
                </p>
              </li>
              <li>
                <strong>Categories of Information We Collect</strong>
                <p>
                  We collect the categories of personal information listed below
                </p>
                <ul className={styles.dashList}>
                  <li>
                    <strong>Identifiers:</strong>
                    &nbsp;
                    Name or username, email address, mailing address, phone number, IP address, account ID, device ID, and other similar identifiers.
                  </li>
                  <li>
                    <strong>Demographic information:</strong>
                    &nbsp;
                    Gender, age, health information, and physical characteristics or description, which may be protected by law.
                  </li>
                  <li>
                    <strong>Commercial Information:</strong>
                    &nbsp;
                    Payment information, records of devices purchased, considered for purchase or prescribed.
                  </li>
                  <li>
                    <strong>Biometric information:</strong>
                    &nbsp;
                    ECG, exercise, activity, sleep, or other health vitals.
                  </li>
                  <li>
                    <strong>Internet or other electronic network activity information:</strong>
                    &nbsp;
                    Web-Behavior Information such as use of our website and mobile app, and collected through cookies, web beacons, and similar technologies. Such information may include your browser type, domains, page views, how long you spent on a page or feature of the website and mobile app, or other data about your engagement with our Services.
                  </li>
                  <li>
                    <strong>Geolocation data:</strong>
                    &nbsp;
                    GPS signals, device sensors, Wi-Fi access points, and cell tower IDs.
                  </li>
                </ul>
              </li>
              <li>
                <strong>How We Use Personal Data</strong>
                <p>
                  Please refer to Section B of this Privacy Policy.
                </p>
              </li>
            </ol>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Changes to This Privacy Policy</strong>
            <p>
              We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.
            </p>
          </li>
          <li className={styles.listTitleMain}>
            <strong className={styles.listTitleSub}>Contact Us</strong>
            <p>
              If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at
            </p>
            <p>
              <strong>Tel:</strong>
              &nbsp;
              +1 650 832 1626.
            </p>
            <p>
              <strong>Email:</strong>
              &nbsp;
              <a className={styles.email} href="mailto:legal@biotricity.com" target="_blank" rel="noreferrer">
                legal@biotricity.com
              </a>
            </p>
            <p>
              <strong>Address:</strong>
            </p>
            <p>
              Biotricity, Inc
            </p>
            <p>
              275 Shoreline Drive, Suite 150
            </p>
            <p>
              Redwood City CA 94065
            </p>
          </li>
        </ol>
      </div>
    </Box>
  </Box>
);

export default PrivacyPolicyContent;
