import React from "react";
import classnames from "classnames";
import ShopFooter from "../../components/shopFooter";
import ShopHeader from "../../components/shopHeader";

import { NextRouter, useRouter } from "next/router";
import styles from "./styles.module.scss";

interface IProps {
  className?: string;
  children: any;
}

const ShopLayoutComponent = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();
  const isShowHeader = (): boolean => {
    return ["/home", "/products/[slug]"].includes(router.pathname);
  };
  const isShowFooter = (): boolean => {
    return ["/home", "/products/[slug]", "/checkout", "/payment-result"].includes(router.pathname);
  };
  return (
    <div className={classnames(styles.shopLayoutComponent, props.className)}>
      {isShowHeader() && <ShopHeader />}
      <main>
        {props.children}
      </main>
      {isShowFooter() && <ShopFooter />}
    </div>
  );
};

export default ShopLayoutComponent;
