import {Auth} from "@aws-amplify/auth";

export const handleCreateAccount = async ({
  email,
  firstName,
  lastName,
  password,
}: any) => {
  try {
    const { user } = await Auth.signUp({
      username: email?.trim().toLowerCase(),
      password,
      attributes: {
        email,
        given_name: firstName.trim(),
        family_name: lastName.trim(),
      },
      autoSignIn: {
        enabled: false,
      },
    });
    return {
      isSuccess: true,
      user,
    };
  } catch (error) {
    return {
      isSuccess: false,
      message: error,
    };
  }
};
