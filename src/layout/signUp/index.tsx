import classnames from "classnames";
import React, { useState } from "react";

import Auth from "@aws-amplify/auth";
import { Box } from "@mui/material";
import { NextRouter, useRouter } from "next/router";
import CustomButton from "../../components/buttons/customButton";
import CustomInput from "../../components/inputs/customInput";
import LoginHeader from "../../components/loginHeader";
import { isValidEmail } from "../../utils";
import { handleCreateAccount } from "./helper";
import styles from "./styles.module.scss";
import { handleAfterSignIn } from "../login/helper";

interface IProps {
  className?: string;
}

interface IState {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
}

interface IErrorState {
  emailError: string;
  passwordError: string;
}

const SignUp = (props: IProps): JSX.Element => {
  const router: NextRouter = useRouter();

  const [state, setState] = useState<IState>({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
  });

  const [errorState, setErrorState] = useState<IErrorState>({
    emailError: "",
    passwordError: "",
  });

  const [loading, setLoading] = useState<boolean>(false);

  const isError = () => {
    return !!errorState.emailError || !!errorState.passwordError;
  };

  const resetErrorState = () => {
    setErrorState({ emailError: "", passwordError: "" });
  };

  const onChangeInput = (name: string, value: string) => {
    if (isError()) {
      resetErrorState();
    }
    setState({ ...state, [name]: value });
  };

  const onSignUp = async () => {
    setLoading(true);
    if (!isValidEmail(state.email)) {
      setErrorState({ ...errorState, emailError: "Invalid email." });
      setLoading(false);
      return;
    }
    if (state.password !== state.confirmPassword) {
      setErrorState({
        ...errorState,
        passwordError: "Passwords are not matched.",
      });
      setLoading(false);
      return;
    }
    const result = await handleCreateAccount({ ...state });
    console.log(result);
    if (!result.isSuccess) {
      if (
        result.message &&
        result.message.toString().includes("UsernameExistsException")
      ) {
        setErrorState({ ...errorState, emailError: "Email is existed!" });
      } else if (
        result.message &&
        result.message.toString().includes("NotAuthorizedException")
      ) {
        setErrorState({
          ...errorState,
          passwordError: "Something went wrong!",
        });
      } else {
        setErrorState({
          ...errorState,
          passwordError: "Something went wrong!",
        });
      }
    } else {
      router.push(`/verify-email/?email=${state.email}`);
    }
    setLoading(false);
  };

  return (
    <Box className={classnames(styles.signUpPage, props.className)}>
      <Box className={styles.signUpForm}>
        <LoginHeader
          className={styles.loginHeader}
          title={"Create an account"}
        />
        <CustomInput
          containerClassName={classnames(styles.signUpInput, styles.mt16)}
          title="First name"
          name="firstName"
          type="text"
          placeholder="Enter here"
          value={state.firstName}
          onChange={onChangeInput}
        />
        <CustomInput
          containerClassName={classnames(styles.signUpInput, styles.mt16)}
          title="Last name"
          name="lastName"
          type="lastName"
          placeholder="Enter here"
          value={state.lastName}
          onChange={onChangeInput}
        />
        <CustomInput
          containerClassName={classnames(styles.signUpInput, styles.mt16)}
          title="Email"
          name="email"
          type="email"
          placeholder="Enter here"
          value={state.email}
          onChange={onChangeInput}
          errorMessage={errorState.emailError}
          isError={!!errorState.emailError}
        />
        <CustomInput
          containerClassName={classnames(styles.signUpInput, styles.mt16)}
          title="Password"
          name="password"
          type="password"
          placeholder="Enter here"
          value={state.password}
          onChange={onChangeInput}
          errorMessage={!!errorState.passwordError}
        />
        <p className={styles.passwordInstruction}>
          Passwords must be at least 8 characters.
        </p>
        <CustomInput
          containerClassName={classnames(styles.signUpInput, styles.mt16)}
          title="Confirm password"
          name="confirmPassword"
          type="password"
          placeholder="Enter here"
          value={state.confirmPassword}
          onChange={onChangeInput}
          errorMessage={errorState.passwordError}
          isError={!!errorState.passwordError}
        />
        <CustomButton
          className={classnames(styles.signUpBtn)}
          fullWidth
          disabled={
            state.password.length === 0 ||
            state.confirmPassword.length === 0 ||
            state.password.length < 8 ||
            state.confirmPassword.length < 8 ||
            isError()
          }
          loading={loading}
          onClick={onSignUp}
        >
          Sign up
        </CustomButton>
      </Box>
    </Box>
  );
};

export default SignUp;
