import classnames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import { Box } from "@mui/material";
import BriefProductCard from "../../components/briefProductCard";
import DetailProduct from "../../components/detailProduct";

import styles from "./styles.module.scss";
import IDetailProduct from "../../modals/detailProductInterface";
import IBriefProductCard from "../../modals/briefProductCardInterface";

interface IProps {
  className?: string;
  mainProduct: IDetailProduct;
  relationProducts: IBriefProductCard[];
}

const ViewProductDetail = (props: IProps): JSX.Element => {
  // must extract options which have size by format { option_id, size: []}
  return (
    <Box className={classnames(styles.viewProductDetail, props.className)}>
      <DetailProduct product={props.mainProduct} />
      {props.relationProducts && props.relationProducts.length !== 0 && (
        <Box className={styles.relevantItems}>
          {props.relationProducts && (
            <>
              <p className={styles.relenvantSentence}>Relevant items</p>
              <Box className={styles.items}>
                {props.relationProducts?.map((product: IBriefProductCard) => (
                  <BriefProductCard key={product.id} product={product} />
                ))}
              </Box>
            </>
          )}
        </Box>
      )}
    </Box>
  );
};

ViewProductDetail.defaultProps = {
  className: "",
};

ViewProductDetail.propTypes = {
  /** override className */
  className: PropTypes.string,
};

export default ViewProductDetail;
