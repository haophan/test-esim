import IProductOption from "./productOptionInterface";

export default interface IBriefProductCard {
  id: number,
  alias?: string,
  title: string,
  slug: string,
  description?: string,
  thumbnail: { id: string, url?: string},
  price: number,
  discount?: number,
  final_price?: number,
  product_options: Array<IProductOption>,
  stripe_subscription_id?: string,
  currency: string,
}