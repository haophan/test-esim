import IBundle from "./bundleInterface";
import IProductOption from "./productOptionInterface";

export default interface IBriefProduct {
  id: number;
  slug: string;
  is_bundle: boolean;
  thumbnail: { id: string; url: string };
  title: string;
  description?: string;
  price: number;
  discount?: number; // in percentage
  final_price?: number;
  product_options: Array<IProductOption>;
  bundles: Array<IBundle>;
  stripe_subscription_id?: string,
  currency: string,
}
