import IBundleItem from "./bundleItemInterface";

export default interface IBundle {
  id: number;
  title: string;
  bundle_items: Array<IBundleItem>;
}
