export default interface IBundleItemDescription {
  id: number;
  title: string;
}