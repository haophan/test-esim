import IBundleItemDescription from "./bundleItemDescriptionInterface";
import IProductOptionIcon from "./productOptionIconInterface";

export default interface IBundleItem {
  id: number;
  title: string;
  icon: IProductOptionIcon;
  bundle_item_descriptions: Array<IBundleItemDescription>;
}
