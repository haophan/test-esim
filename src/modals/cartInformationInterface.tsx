import ICartProduct from "./cartProductInterface";

export default interface ICartInformation {
  quantity: number;
  product: ICartProduct
}