import IProductVariation from "./productVariationInterface";

export default interface ICartProduct {
  id: number;
  is_bundle?: boolean;
  title: string;
  buy_limit?: number;
  price: number;
  discount?: number;
  final_price?: number;
  thumbnail?: { id: number; url?: string };
  optionType?: any;
  selectedOptions?: {
    id: number;
    variation: IProductVariation;
  };
  stripe_subscription_id?: string,
  currency: string,
}
