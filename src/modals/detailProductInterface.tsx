import IBundle from "./bundleInterface";
import IProductOption from "./productOptionInterface";

export default interface IDetailProduct {
  id: number;
  slug: string;
  alias?: string;
  buy_limit?: number;
  is_bundle?: boolean;
  detail_images: Array<{ id: number; url?: string }>;
  thumbnail?: { id: number; url?: string };
  title: string;
  description?: string;
  price: number;
  discount?: number; // in percentage
  final_price?: number;
  product_options?: Array<IProductOption>;
  bundles: Array<IBundle>;
  stripe_subscription_id?: string,
  currency: string,
}
