import IBriefProductCard from "./briefProductCardInterface";
import IBriefProduct from "./briefProductInterface";

export default interface IHomeProduct {
  homeProduct?: IBriefProduct,
  relationProducts: IBriefProductCard[],
}