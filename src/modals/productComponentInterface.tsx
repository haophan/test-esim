import IProductOptionIcon from "./productOptionIconInterface";
import IProductOptionDescription from "./productOptionsDescriptionInterface";

export default interface IProductComponent {
  id: number,
  title: string,
  icon: IProductOptionIcon,
  bundle_item_descriptions?: IProductOptionDescription[]
}