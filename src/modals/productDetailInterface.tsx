import IBriefProductCard from "./briefProductCardInterface";
import IDetailProduct from "./detailProductInterface";

export default interface IProductDetail {
  mainProduct: IDetailProduct,
  relationProducts: IBriefProductCard[],
}