export default interface IProductOptionIcon {
  alternativeText: string,
  id: number,
  name: string,
  url: string,
}