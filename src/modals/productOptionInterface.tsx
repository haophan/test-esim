import IProductOptionIcon from "./productOptionIconInterface";
import IProductVariation from "./productVariationInterface";

export default interface IProductOption {
  id: number,
  be_product_id?: number,
  title: string,
  icon: IProductOptionIcon,
  product_variations?: Array<IProductVariation>
}