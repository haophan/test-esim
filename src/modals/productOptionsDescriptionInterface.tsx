export default interface IProductOptionDescription {
  id: number,
  title: string,
}