export default interface IProductSlug {
  id: number, 
  slug: string,
}