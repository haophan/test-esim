export default interface IProductVariation {
  id: number,
  fullname: string,
  short_name: string,
  description?: string,
}