export default interface IRequestResponseWrapper {
  data?: any;
  meta?: any;
  errors?: Array<{message: string}>
}