import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: any = {}

const buyNowSlice = createSlice({
  name: 'buyNow',
  initialState,
  reducers: {
    updateBuyNow: (state, action: PayloadAction<any>) => {
      return action.payload
    },
    removeBuyNow: (state) => {
      return {}
    }
  }
})

export const { updateBuyNow, removeBuyNow } =
buyNowSlice.actions;

export default buyNowSlice.reducer;
