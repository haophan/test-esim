import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import _ from "lodash";
import ICartInformation from "../../modals/cartInformationInterface";

type CartState = {
  products: Array<ICartInformation>;
};

const initialState: CartState = {
  products: [],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addProduct: (state, action: PayloadAction<ICartInformation>) => {
      const existedProductIdx = !_.isEmpty(action.payload.product.selectedOptions) 
        ? state.products.findIndex(
            (product: ICartInformation) =>
              product.product.id === action.payload.product.id &&
              product.product.selectedOptions &&
              action.payload.product.selectedOptions &&
              product.product.selectedOptions.id ===
                action.payload.product.selectedOptions.id &&
              product.product?.selectedOptions.variation.id ===
                action.payload.product.selectedOptions.variation.id
          )
        : state.products.findIndex(
            (product: any) => product.product.id === action.payload.product.id
          );
      if (existedProductIdx < 0) {
        return {
          ...state,
          products: [
            ...state.products,
            {
              quantity: action.payload.quantity,
              product: action.payload.product,
            },
          ],
        };
      } else {
        state.products[existedProductIdx].quantity += action.payload.quantity;
      }
    },
    removeProduct: (
      state,
      action: PayloadAction<{
        id: number;
        optionId?: number;
        variationId?: number;
      }>
    ) => {
      return {
        ...state,
        products:
          action.payload.optionId && action.payload.variationId
            ? state.products.filter(
                (product: ICartInformation) =>
                  !(
                    product.product.id === action.payload.id &&
                    product.product.selectedOptions &&
                    product.product.selectedOptions.id ===
                      action.payload.optionId &&
                    product.product.selectedOptions.variation &&
                    product.product.selectedOptions.variation.id ===
                      action.payload.variationId
                  )
              )
            : state.products.filter(
                (product) => product.product.id !== action.payload.id
              ),
      };
    },
    increaseQuantity: (
      state,
      action: PayloadAction<{
        id: number;
        optionId?: number;
        variationId?: number;
      }>
    ) => {
      const targetProduct =
        action.payload.optionId && action.payload.variationId
          ? state.products.find(
              (product: ICartInformation) =>
                product.product.id === action.payload.id &&
                product.product.selectedOptions &&
                product.product.selectedOptions.id ===
                  action.payload.optionId &&
                product.product.selectedOptions.variation.id ===
                  action.payload.variationId
            )
          : state.products.find(
              (product) => product.product.id === action.payload.id
            );
      if (targetProduct) {
        targetProduct.quantity++;
      }
    },
    decreaseQuantity: (
      state,
      action: PayloadAction<{
        id: number;
        optionId?: number;
        variationId?: number;
      }>
    ) => {
      const targetProduct =
        action.payload.optionId && action.payload.variationId
          ? state.products.find(
              (product: ICartInformation) =>
                product.product.id === action.payload.id &&
                product.product.selectedOptions &&
                product.product.selectedOptions.id ===
                  action.payload.optionId &&
                product.product.selectedOptions.variation.id ===
                  action.payload.variationId
            )
          : state.products.find(
              (product) => product.product.id === action.payload.id
            );
      if (targetProduct) {
        targetProduct.quantity--;
      }
    },

    clearCart: (state) => {
      state.products = []
    }
  },
});

export const { addProduct, removeProduct, increaseQuantity, decreaseQuantity, clearCart} =
  cartSlice.actions;

export default cartSlice.reducer;
