import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { combineReducers } from '@reduxjs/toolkit';
import loadingReducer from './loading';
import cartReducer from './cart.tsx';
import buyNowReducer from './buyNow.tsx';

const rootPersistConfig = {
  key: 'root',
  storage,
  whitelists: ['cart'],
};

const rootReducer = combineReducers({
  loading: loadingReducer,
  cart: cartReducer,
  buyNow: buyNowReducer,
});

const persistedReducer = persistReducer(rootPersistConfig, rootReducer);

export default persistedReducer;
