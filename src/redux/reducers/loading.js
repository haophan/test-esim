import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  value: {
    loading: false,
  },
};

export const loadingSlice = createSlice({
  name: 'loading',
  initialState,
  reducers: {
    setLoadingPage: (state, action) => {
      state.value = {
        ...state.value,
        ...action.payload,
      };
    },
  },
});

// Action creators are generated for each case reducer function
export const { setLoadingPage } = loadingSlice.actions;

export const isLoadingPage = state => state.loading.value.loading;

export default loadingSlice.reducer;
