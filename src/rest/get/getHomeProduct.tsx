import IHomeProduct from "../../modals/homeProductInterface";
import IRelationProduct from "../../modals/relationProductInterface";
import { getError } from "../helper";

export interface IGetHomeProductResponse {
  homeProduct: IHomeProduct | any;
  relationProducts: IRelationProduct[];
}

export const getHomeProduct = async (
  fields: string[] = []
): Promise<IGetHomeProductResponse | Error> => {
  try {
    const response = await fetch(
      `http://192.168.20.230:1337/api/home-products?populate[product][populate][sizes]=*&populate[product][populate][detail_images]=*&populate[product][populate][relation][populate][detail_images]=*&&populate[product][populate][relation][populate][sizes]=*`,
    );
    if (!response.ok) {
      return new Error(response.statusText);
    }
    const result = await response.json();
    // homeProduct will be at result.data[0].attributes.product.data
    // must extract relationrelationProductData. -> flatten home product
    // flatten relationProduct
    const homeProduct = {
      ...result.data[0].attributes.product.data.attributes,
      id: result.data[0].attributes.product.data.id,
    };
    const relationProducts = { ...homeProduct.relation };
    delete homeProduct.relation;
    const unwindedRelationProducts = relationProducts.data.map(
      (product: any) => ({ ...product.attributes, id: product.id })
    );
    return {
      homeProduct,
      relationProducts: unwindedRelationProducts,
    };
  } catch (error: unknown) {
    return getError(error);
  }
};
