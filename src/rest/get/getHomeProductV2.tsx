import createRequest from "..";
import IHomeProduct from "../../modals/homeProductInterface";
import IJSONResponse from "../../modals/jsonResponseInterface";
import IRequestResponseWrapper from "../../modals/requestResponseInterface";
import { DEFAULT_REQUEST_HEADERS } from "../helper";

export const getHomeProductV2 = async (): Promise<
  IJSONResponse<IHomeProduct>
> => {
  const homepageData: IRequestResponseWrapper = await createRequest(
    "home-products?populate[product][populate][thumbnail]=*&populate[product][populate][bundles][populate][bundle_items][populate][icon]=*&populate[product][populate][bundles][populate][bundle_items][populate][bundle_item_descriptions]=*&populate[product][populate][relation][populate][thumbnail]=*&populate[product][populate][relation][populate][product_options][populate][product_variations]]=*",
    {
      headers: { ...DEFAULT_REQUEST_HEADERS },
    }
  );
  if (homepageData.errors || homepageData.data.length === 0) {
    return { errors: homepageData.errors };
  }
  const homeProduct = homepageData.data[0].product;
  const relationProducts = homeProduct.relation;
  delete homeProduct?.relation;
  return { data: { homeProduct, relationProducts } };
};