import createRequest from "..";
import IBriefProductCard from "../../modals/briefProductCardInterface";
import IBriefProduct from "../../modals/briefProductInterface";
import IDetailProduct from "../../modals/detailProductInterface";
import IJSONResponse from "../../modals/jsonResponseInterface";
import IProductDetail from "../../modals/productDetailInterface";
import IProductOption from "../../modals/productOptionInterface";
import IProductVariation from "../../modals/productVariationInterface";
import IRequestResponseWrapper from "../../modals/requestResponseInterface";
import { DEFAULT_REQUEST_HEADERS } from "../helper";

export const getProductBySlug = async (
  slug: string,
  fields: string[] = []
): Promise<IJSONResponse<IProductDetail>> => {
  const detailPageData: IRequestResponseWrapper = await createRequest(
    `products?filters[slug][$eq]=${slug}&populate[product_options][populate][product_variations]=*&populate[product_options][populate][icon]=*&populate[detail_images]=*&populate[thumbnail]=*&populate[bundles][populate][bundle_items][populate][icon]=*&populate[bundles][populate][bundle_items][populate][bundle_item_descriptions]=*&populate[relation][populate][product_options][populate][product_variations]=*&populate[relation][populate][thumbnail]=*`,
    {
      headers: {...DEFAULT_REQUEST_HEADERS}
    }
  );
  if (detailPageData.errors || detailPageData.data.length === 0) {
    return { errors: detailPageData.errors || [{ message: "Error" }] };
  }
  const mainProduct: IDetailProduct = detailPageData.data[0];
  const relationProducts: IBriefProductCard[] = detailPageData.data[0].relation;
  return {
    data: { mainProduct, relationProducts },
  };
};
