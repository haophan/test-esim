import createRequest from "..";
import IJSONResponse from "../../modals/jsonResponseInterface";
import IProductSlug from "../../modals/productSlugInterface";
import IRequestResponseWrapper from "../../modals/requestResponseInterface";
import { DEFAULT_REQUEST_HEADERS } from "../helper";

export const getProductSlugs = async (): Promise<IJSONResponse<IProductSlug[]>> => {
  const result: IRequestResponseWrapper = await createRequest("products?fields=slug", {
    headers: {...DEFAULT_REQUEST_HEADERS}

  });
  if (result.errors) {
    return { errors: result.errors };
  }
  return {
    data: result.data,
  };
};
