export function getError(error: unknown) {
  if (error instanceof Error) {
    return error;
  }
  throw new Error("Unknown error!");
}
function flattenArray(obj: any) {
  return obj.map((e: any) => flatten(e));
}

function flattenData(obj: any): any {
  return flatten(obj.data);
}

function flattenAttrs(obj: any): any {
  let attrs: any = {};
  for (let key in obj.attributes) {
    attrs[key] = flatten(obj.attributes[key]);
  }
  return {
    id: obj.id,
    ...attrs,
  };
}

export function flatten(obj: any) {
  if (Array.isArray(obj)) {
    return flattenArray(obj);
  }
  if (obj && obj.data) {
    return flattenData(obj);
  }
  if (obj && obj.attributes) {
    return flattenAttrs(obj);
  }
  return obj;
}

export const DEFAULT_REQUEST_HEADERS = {
  "Content-Type": "application/json",
};
