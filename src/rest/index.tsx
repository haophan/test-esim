import axios from 'axios';
import { LINK_CONFIG } from "../aws-amplify/env";
import IRequestResponseWrapper from "../modals/requestResponseInterface";
import { DEFAULT_REQUEST_HEADERS, flatten } from "./helper";

async function createRequest(
  endpoint: string,
  config: any = {}
): Promise<IRequestResponseWrapper> {
  try {
    const response: Response = await fetch(
      `${LINK_CONFIG.HOST_URL}${endpoint}`,
      { ...config }
    );

    const result = await response.json();
    // result will include data and meta
    // data save the data, meta save meta data, pagination for specific.
    // because we only have 3 items --> dont care much about meta.
    if (response.ok) {
      if (result.data) {
        return { data: flatten(result.data), meta: result.meta };
      } else {
        return { errors: [{ message: "Unknown" }] };
      }
    } else {
      const statusText: string = await response.text();
      console.log('home api error', statusText);
      return { errors: [{ message: statusText }] };
    }
  } catch (error: any) {
    console.log('home api catch error', error);
    return { errors: [{ message: `catch ${error?.message?.toString()}` }] };
  }
}

export const createRequest_1 = async (
  endpoint: string,
  config: any = {}
): Promise<string>  => {
  try {
    const response = await fetch(
      'https://shopad.alpha.bioheart.bioflux.io/api/home-products',
    );
    // const text1 = JSON.stringify(await JSON.parse(await response.text()));
    // console.log('response: ' + text1);
    const text1 = await response.text();
    // const res = await fetch('https://api.github.com/repos/vercel/next.js');
    // console.log('res 1: ' + JSON.stringify(await res.json()));
    // const text2 = await res.text();
    return text1;
    // return  text1 + JSON.stringify(await res.json());
    // result will include data and meta
    // data save the data, meta save meta data, pagination for specific.
    // because we only have 3 items --> dont care much about meta.
  } catch (error: any) {
    console.log('home api catch error', error);
    return error;
  }
}
const api = {
  get: () => {},
  post: () => {},
};

export default createRequest;

export async function createRequestTest(
  endpoint: string,
  config: any = {}
): Promise<IRequestResponseWrapper> {
  try {
    const response: any = await axios.get(
      `${LINK_CONFIG.HOST_URL}${endpoint}`,
      { ...config }
    );

    // const result = await response.json();
    // const result = await response.text();
    const result = response?.data;
    console.log('home api result ', result);
    // result will include data and meta
    // data save the data, meta save meta data, pagination for specific.
    // because we only have 3 items --> dont care much about meta.
    return { data: result, meta: "" };
    // if (response.ok) {
    //   return { data: result, meta: "" };
    //   // if (result.data) {
    //   //   return { data: flatten(result.data), meta: result.meta };
    //   // } else {
    //   //   return { errors: [{ message: "Unknown" }] };
    //   // }
    // } else {
    //   // const statusText: string = await response.text();
    //   console.log('home api error', statusText);
    //   return { errors: [{ message: statusText }] };
    // }

    // const result = await response.json();
    // result will include data and meta
    // data save the data, meta save meta data, pagination for specific.
    // because we only have 3 items --> dont care much about meta.
    // if (response.ok) {
    //   if (result.data) {
    //     return { data: flatten(result.data), meta: result.meta };
    //   } else {
    //     return { errors: [{ message: "Unknown" }] };
    //   }
    // } else {
    //   const statusText: string = await response.text();
    //   return { errors: [{ message: statusText }] };
    // }
    // return { data: response }
  } catch (error: any) {
    return { errors: [{ message: error?.message?.toString() }] };
  }
}
