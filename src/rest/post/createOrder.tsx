import createRequest from "..";
import ICartInformation from "../../modals/cartInformationInterface";
import IRequestResponseWrapper from "../../modals/requestResponseInterface";
import { auth } from "../../utils/auth";
import { DEFAULT_REQUEST_HEADERS } from "../helper";

export const createOrder = async (
  shippingInfo: any,
  cart: { products: ICartInformation[] }
): Promise<{
  isSuccess: boolean;
  errors?: Array<{ message: string }>;
  data?: { orderId?: number };
}> => {
  // work flow
  // 1. create shipping info
  // 2. create order
  // 3. create order detail
  // 4. if product has option --> create order option
  // -------------------- start ------------------------

  const accessToken = auth.getLoginData()?.accessToken || "";
  if (!accessToken) {
    return {
      isSuccess: false,
      errors: [{ message: "No authentication found." }],
    };
  }

  const token = {
    authorization: `Bearer ${accessToken}`,
  };

  //  1. create shipping info
  const createShippingInfoResponse = await createRequest("shipping-infos", {
    method: "POST",
    headers: { ...DEFAULT_REQUEST_HEADERS, ...token },
    body: JSON.stringify({
      data: {
        first_name: shippingInfo.firstName,
        last_name: shippingInfo.lastName,
        email: shippingInfo.email,
        address: shippingInfo.address,
        secondary_address: shippingInfo.secondaryAddress,
        city: shippingInfo.city,
        state: shippingInfo.state,
        zip_code: shippingInfo.zip,
        country: shippingInfo.country,
        phone_number: shippingInfo.phoneNumber.toString(),
      },
    }),
  });

  if (createShippingInfoResponse.errors) {
    return {
      isSuccess: false,
      errors: createShippingInfoResponse.errors,
    };
  }

  // 2. create order
  const createOrderResponse = await createRequest("orders", {
    method: "POST",
    headers: { ...DEFAULT_REQUEST_HEADERS, ...token },

    body: JSON.stringify({
      data: { shipping_info: createShippingInfoResponse.data.id },
    }),
  });
  if (createOrderResponse.errors) {
    return {
      isSuccess: false,
      errors: createOrderResponse.errors,
    };
  }

  // 3. create order detail
  let mappingOrderDetail: any[] = [];

  cart.products.forEach((product: ICartInformation) => {
    mappingOrderDetail.push({
      quantity: product.quantity,
      order: createOrderResponse.data.id,
      product: product.product.id,
    });
  });
  const createOrderDetailRequests = mappingOrderDetail.map((item: any) => {
    return createRequest("order-details", {
      method: "POST",
      headers: { ...DEFAULT_REQUEST_HEADERS, ...token },
      body: JSON.stringify({ data: item }),
    });
  });

  const createOrderDetailResponses = await Promise.all(
    createOrderDetailRequests
  );

  createOrderDetailResponses.forEach((response: IRequestResponseWrapper) => {
    if (response.errors) {
      return {
        isSuccess: false,
        errors: response.errors,
      };
    }
  });

  // 4. create order option
  let mappingOrderProductOption: any[] = [];

  cart.products.forEach((product: ICartInformation, index: number) => {
    if (
      product.product.selectedOptions &&
      product.product.selectedOptions.variation &&
      !createOrderDetailResponses[index].errors
    ) {
      mappingOrderProductOption.push({
        order_detail: createOrderDetailResponses[index]?.data?.id,
        product_option: product.product.selectedOptions.id,
        product_variation: product.product.selectedOptions.variation.id,
      });
    }
  });

  const createOrderDetailOptionRequests = mappingOrderProductOption.map(
    (item: any) => {
      return createRequest("order-options", {
        method: "POST",
        headers: { ...DEFAULT_REQUEST_HEADERS, ...token },
        body: JSON.stringify({ data: item }),
      });
    }
  );

  const createOrderDetailOptionResponses = await Promise.all(
    createOrderDetailOptionRequests
  );

  createOrderDetailOptionResponses.forEach(
    (response: IRequestResponseWrapper) => {
      if (response.errors) {
        return {
          isSuccess: false,
          errors: response.errors,
        };
      }
    }
  );
  return {
    isSuccess: true,
    data: {
      orderId: createOrderResponse.data.id,
    },
  };
};
export default createOrder;
