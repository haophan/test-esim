import createRequest from "..";
import { DEFAULT_REQUEST_HEADERS } from "../helper";

export const createUser = async (userInfo: {
  cognitoIdToken: string;
  cognitoAccessToken: string;
}) => {
  const createUserResponse = await createRequest("sign-in", {
    method: "POST",
    headers: {...DEFAULT_REQUEST_HEADERS},
    body: JSON.stringify({
      ...userInfo,
    }),
  });

  if (createUserResponse.errors) {
    return {
      isSuccess: false,
      errors: createUserResponse.errors,
    };
  }

  if (createUserResponse.data && !createUserResponse.data.isSuccess) {
    return {
      isSuccess: false,
      errors: [{ message: createUserResponse.data.message }],
    };
  }
  return {
    isSuccess: true,
    loginData: {
      accessToken: createUserResponse.data.accessToken,
    },
  };
};
