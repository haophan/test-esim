import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  status: {
    danger: 'FDE9EC',
  },
  typography: {
    fontFamily: [
      'Cera Pro',
      'sans-serif',
    ].join(','),
  },
});

export default theme;
