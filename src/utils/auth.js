
const { localStorage } = global.window || {};
export const auth = {
  setLoginData(loginData) {
    localStorage.loginData = JSON.stringify(loginData);
    localStorage.loggedIn = true;
    // localStorage.photo = base64 || '';
    // emitter.emit("CHANGE_PROFILE");
  },

  getLoginData() {
    return localStorage?.loginData ? JSON.parse(localStorage.loginData) : {};
  },

  getIsFirstLogin() {
    return localStorage?.firstLogin === "true" || undefined;
  },

  setIsFirstLogin(value) {
    localStorage.firstLogin = value;
  },

  isLoggedIn() {
    return localStorage?.loggedIn === "true" || false;
  },

  setLoggedIn(loggedIn) {
    localStorage.loggedIn = loggedIn;
  },

  getAvatar() {
    return localStorage?.photo || "";
  },

  setAvatar(base64) {
    localStorage.photo = base64;
  },

  signOut() {
    localStorage.clear();
  },
};
