const isEnvCustomer = () => !window.location.hostname.includes('alpha.cardiac')
  && !window.location.hostname.includes('staging.cardiac');
const isEnvLocalDev = () => window.location.hostname.includes('localhost');

const consoleLog = (command, data) => {
  if (isEnvLocalDev()) {
    console.log(command, data);
  }
};
export default consoleLog;
