import { useEffect, useState } from 'react';

/*
 *  const debouncedSearchTerm = useDebounce(searchTerm, 500);
 * The hook will only return the latest value if it's been more than 500ms since searchTerm changed.
 */
export const useDebounce = (value, delay) => {
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);
    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);
  return debouncedValue;
};
