/* eslint-disable consistent-return */
import { useEffect } from 'react';

export function useIntersectionObserver(element, appear, dissolve, deps) {
  const observer = new IntersectionObserver(async (entries) => {
    if (entries[0].isIntersecting) {
      appear();
    } else {
      dissolve();
    }
  });
  useEffect(() => {
    if (!element) return;
    observer.observe(element);
    return () => {
      if (element) { observer.unobserve(element); }
    };
  }, [element, ...deps]);
  return observer;
}
