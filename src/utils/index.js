/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-async-promise-executor */
import axios from 'axios';
import { isValidPhoneNumber } from 'libphonenumber-js';
import { auth } from './auth';

export const logout = async () => {
  // await Auth.signOut();
  auth.signOut();
  // socket.disconnectSocket();
};


export const isValidEmail = (email = '') => {
  const re = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email.trim()).toLowerCase());
};


export const checkingDivisionZipCode = (division = '', postalCode = '') => {
  const url = `https://sm.alpha.bioflux.io/api/geocode/check-postal?division=${division}&postalCode=${postalCode}`;
  return axios.get(url).then((result) => result?.data || {});
};

export const getFormatedPhone = (phoneNumber = '') => {
  if (!phoneNumber) {
    return '';
  }
  const phone = `${phoneNumber}`;
  if (phone?.includes('-')) {
    if (phone?.[3] === '-' && phone?.[7] === '-') {
      return phone;
    }
    const newPhone = phone.replaceAll('-', '');
    return `${newPhone.slice(0, 3)}-${newPhone.slice(3, 6)}-${newPhone.slice(
      6,
    )}`;
  }
  return `${phone.slice(0, 3)}-${phone.slice(3, 6)}-${phone.slice(6)}`;
};

export const downloadPdfByClick = (path) => {
  const anchor = document.createElement('a');
  anchor.href = path;
  // anchor.download = filename;
  document.body.appendChild(anchor);
  anchor.click();
  document.body.removeChild(anchor);
};

export const isValidPhone = (phoneNumber, countryCode) => isValidPhoneNumber(phoneNumber, countryCode);
