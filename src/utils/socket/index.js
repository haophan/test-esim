/* eslint-disable import/no-cycle */
/* eslint-disable class-methods-use-this */
import Auth from '@aws-amplify/auth';
import io from 'socket.io-client';
import { LINK_CONFIG } from '../../aws-amplify/env';
import { EmitterEventEnum, SocketEventEnum } from '../../constants';
import consoleLog from '../consoleLog';
import emitter from '../eventEmitter';

class SocketClient {
  constructor() {
    this.isconnected = false;
    this.socket = null;
  }

  async connectToServer() {
    if (!this.socket) {
      const query = {};
      try {
        const currentSession = await Auth.currentSession();
        const awsToken = currentSession.accessToken.jwtToken;
        query.token = awsToken;
        this.socket = io(LINK_CONFIG.URL_SOCKET, {
          query,
          forceNew: true,
          transports: ['websocket'],
        });
        this.socket.on(SocketEventEnum.CONNECT, this.connectListener);
      } catch (error) {
        if (error === 'No current user') {
          emitter.emit(EmitterEventEnum.LOGOUT);
        }
      }
    }
  }

  connectListener = async () => {
    try {
      this.isConnected = true;
      this.socket.io.off(SocketEventEnum.RECONNECT);
      this.socket.on(SocketEventEnum.DISCONNECT, this.disconnectListener);

      this.socket.on(SocketEventEnum.ACCOUNT_DELETION, this.onAccountDeletion);
      this.socket.on(SocketEventEnum.RECOVER_ACCOUNT, this.onRecoverAccount);
      this.socket.on(SocketEventEnum.UPDATE_USER_SETTING, this.onUpdateUserSetting);
      this.socket.on(SocketEventEnum.ASK_MONITOR_AUTH, this.onAskMonitorAuth);
      this.socket.on(SocketEventEnum.UPDATE_MONITOR_AUTH, this.onUpdateMonitorAuth);
      this.socket.on(SocketEventEnum.EXPORT_SNAPSHOT, this.onExportSnapshot);
      this.socket.on(SocketEventEnum.EXPORT_SUMMARY_REPORT, this.onExportSummaryReport);
      this.socket.on(SocketEventEnum.CREATE_SNAPSHOT, this.onCreateSnapshot);

      emitter.emit(EmitterEventEnum.SOCKET_CONNECTED);
      consoleLog('Socket IO is connected');
    } catch (error) {
      consoleLog('Failed to connect: ', error);
    }
  };

  onAccountDeletion = (msg) => {
    consoleLog('onAccountDeletion', msg);
    emitter.emit(EmitterEventEnum.ACCOUNT_DELETION, msg);
  };

  onRecoverAccount = (msg) => {
    consoleLog('onRecoverAccount', msg);
    emitter.emit(EmitterEventEnum.RECOVER_ACCOUNT, msg);
  };

  onUpdateUserSetting = (msg) => {
    consoleLog('onUpdateUserSetting', msg);
    emitter.emit(EmitterEventEnum.UPDATE_USER_SETTING, msg);
  };

  onAskMonitorAuth = (msg) => {
    consoleLog('onAskMonitorAuth', msg);
    // update event from cardiac
    emitter.emit(EmitterEventEnum.ASK_MONITOR_AUTH, msg);
  };

  onUpdateMonitorAuth = (msg) => {
    consoleLog('onUpdateMonitorAuth', msg);
    // update event from bioheart
    emitter.emit(EmitterEventEnum.UPDATE_MONITOR_AUTH, msg);
  };

  onExportSnapshot = (msg) => {
    consoleLog('onExportSnapshot', msg);
    emitter.emit(EmitterEventEnum.EXPORT_SNAPSHOT, msg);
  };

  onExportSummaryReport = (msg) => {
    consoleLog('onExportSummaryReport', msg);
    emitter.emit(EmitterEventEnum.EXPORT_SUMMARY_REPORT, msg);
  };

  onCreateSnapshot = (msg) => {
    consoleLog('onCreateSnapshot', msg);
    emitter.emit(EmitterEventEnum.CREATE_SNAPSHOT, msg);
  };

  disconnectListener = async (error) => {
    consoleLog('disconnectListener', error);
    this.isConnected = false;

    this.socket.io.on(SocketEventEnum.RECONNECT, this.reconnectListener);

    this.socket.off(SocketEventEnum.CONNECT);
    this.socket.off(SocketEventEnum.DISCONNECT);

    this.socket.off(SocketEventEnum.ACCOUNT_DELETION);
    this.socket.off(SocketEventEnum.RECOVER_ACCOUNT);

    try {
      const currentSession = await Auth.currentSession();
      const token = currentSession.accessToken.jwtToken;
      if (error && error.trim() === 'ping timeout' && !token) {
        this.disconnectSocket();
        this.socket = null;
      }
    } catch (err) {
      consoleLog('Failed to disconecet Socket: ', err);
    }
  };

  reconnectListener = () => {
    consoleLog('Socket IO is reconnected');
    this.socket.on(SocketEventEnum.CONNECT, this.connectListener);
    window.location.reload();
  };

  disconnectSocket() {
    if (this.socket) {
      this.socket.disconnect();
    }
    this.socket = null;
    this.isConnected = false;
    consoleLog('Socket IO is disconnected');
  }
}

const staticSocket = new SocketClient();

export default staticSocket;
