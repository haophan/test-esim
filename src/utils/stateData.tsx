export const USStates: Array<{ name: string; fullCode: string; code: string }> =
  [
    {
      name: "Alabama",
      fullCode: "US-AL",
      code: "AL",
    },
    {
      name: "Connecticut",
      fullCode: "US-CT",
      code: "CT",
    },
    {
      name: "Hawaii",
      fullCode: "US-HI",
      code: "HI",
    },
    {
      name: "Kansas",
      fullCode: "US-KS",
      code: "KS",
    },
    {
      name: "Maine",
      fullCode: "US-ME",
      code: "ME",
    },
    {
      name: "Montana",
      fullCode: "US-MT",
      code: "MT",
    },
    {
      name: "New Jersey",
      fullCode: "US-NJ",
      code: "NJ",
    },
    {
      name: "Oklahoma",
      fullCode: "US-OK",
      code: "OK",
    },
    {
      name: "South Dakota",
      fullCode: "US-SD",
      code: "SD",
    },
    {
      name: "Vermont",
      fullCode: "US-VT",
      code: "VT",
    },
    {
      name: "Arkansas",
      fullCode: "US-AR",
      code: "AR",
    },
    {
      name: "District of Columbia",
      fullCode: "US-DC",
      code: "DC",
    },
    {
      name: "Iowa",
      fullCode: "US-IA",
      code: "IA",
    },
    {
      name: "Kentucky",
      fullCode: "US-KY",
      code: "KY",
    },
    {
      name: "Michigan",
      fullCode: "US-MI",
      code: "MI",
    },
    {
      name: "California",
      fullCode: "US-CA",
      code: "CA",
    },
    {
      name: "Florida",
      fullCode: "US-FL",
      code: "FL",
    },
    {
      name: "North Carolina",
      fullCode: "US-NC",
      code: "NC",
    },
    {
      name: "New Mexico",
      fullCode: "US-NM",
      code: "NM",
    },
    {
      name: "Illinois",
      fullCode: "US-IL",
      code: "IL",
    },
    {
      name: "Oregon",
      fullCode: "US-OR",
      code: "OR",
    },
    {
      name: "Tennessee",
      fullCode: "US-TN",
      code: "TN",
    },
    {
      name: "Massachusetts",
      fullCode: "US-MA",
      code: "MA",
    },
    {
      name: "Washington",
      fullCode: "US-WA",
      code: "WA",
    },
    {
      name: "Missouri",
      fullCode: "US-MO",
      code: "MO",
    },
    {
      name: "Nebraska",
      fullCode: "US-NE",
      code: "NE",
    },
    {
      name: "New York",
      fullCode: "US-NY",
      code: "NY",
    },
    {
      name: "Rhode Island",
      fullCode: "US-RI",
      code: "RI",
    },
    {
      name: "Utah",
      fullCode: "US-UT",
      code: "UT",
    },
    {
      name: "West Virginia",
      fullCode: "US-WV",
      code: "WV",
    },
    {
      name: "Alaska",
      fullCode: "US-AK",
      code: "AK",
    },
    {
      name: "Colorado",
      fullCode: "US-CO",
      code: "CO",
    },
    {
      name: "Georgia",
      fullCode: "US-GA",
      code: "GA",
    },
    {
      name: "Indiana",
      fullCode: "US-IN",
      code: "IN",
    },
    {
      name: "Maryland",
      fullCode: "US-MD",
      code: "MD",
    },
    {
      name: "Mississippi",
      fullCode: "US-MS",
      code: "MS",
    },
    {
      name: "New Hampshire",
      fullCode: "US-NH",
      code: "NH",
    },
    {
      name: "Ohio",
      fullCode: "US-OH",
      code: "OH",
    },
    {
      name: "South Carolina",
      fullCode: "US-SC",
      code: "SC",
    },
    {
      name: "Virginia",
      fullCode: "US-VA",
      code: "VA",
    },
    {
      name: "Wyoming",
      fullCode: "US-WY",
      code: "WY",
    },
    {
      name: "Arizona",
      fullCode: "US-AZ",
      code: "AZ",
    },
    {
      name: "Delaware",
      fullCode: "US-DE",
      code: "DE",
    },
    {
      name: "Idaho",
      fullCode: "US-ID",
      code: "ID",
    },
    {
      name: "Louisiana",
      fullCode: "US-LA",
      code: "LA",
    },
    {
      name: "Minnesota",
      fullCode: "US-MN",
      code: "MN",
    },
    {
      name: "North Dakota",
      fullCode: "US-ND",
      code: "ND",
    },
    {
      name: "Nevada",
      fullCode: "US-NV",
      code: "NV",
    },
    {
      name: "Pennsylvania",
      fullCode: "US-PA",
      code: "PA",
    },
    {
      name: "Texas",
      fullCode: "US-TX",
      code: "TX",
    },
    {
      name: "Wisconsin",
      fullCode: "US-WI",
      code: "WI",
    },
  ];

export const CAStates: Array<{ name: string; fullCode: string; code: string }> =
  [
    {
      name: "Newfoundland and Labrador",
      fullCode: "CA-NL",
      code: "NL",
    },
    {
      name: "Manitoba",
      fullCode: "CA-MB",
      code: "MB",
    },
    {
      name: "Nunavut",
      fullCode: "CA-NU",
      code: "NU",
    },
    {
      name: "Prince Edward Island",
      fullCode: "CA-PE",
      code: "PE",
    },
    {
      name: "Yukon",
      fullCode: "CA-YT",
      code: "YT",
    },
    {
      name: "Alberta",
      fullCode: "CA-AB",
      code: "AB",
    },
    {
      name: "Nova Scotia",
      fullCode: "CA-NS",
      code: "NS",
    },
    {
      name: "Quebec",
      fullCode: "CA-QC",
      code: "QC",
    },
    {
      name: "New Brunswick",
      fullCode: "CA-NB",
      code: "NB",
    },
    {
      name: "Ontario",
      fullCode: "CA-ON",
      code: "ON",
    },
    {
      name: "British Columbia",
      fullCode: "CA-BC",
      code: "BC",
    },
    {
      name: "Northwest Territories",
      fullCode: "CA-NT",
      code: "NT",
    },
    {
      name: "Saskatchewan",
      fullCode: "CA-SK",
      code: "SK",
    },
  ];

export const mappingState = (country: "US" | "CA" | string) => {
  return country === "US"
    ? USStates.map(
        (state: { name: string; fullCode: string; code: string }) => ({
          value: state.code,
          label: state.name,
        })
      )
    : CAStates.map(
        (state: { name: string; fullCode: string; code: string }) => ({
          value: state.code,
          label: state.name,
        })
      );
};

export const getFullCode = (country: 'US' | 'CA' | string, stateCode: string): string | undefined=> {
  return country === "US"
    ? USStates.find(
        (state: { name: string; fullCode: string; code: string }) => (state.code === stateCode)
      )?.fullCode
    : CAStates.find(
      (state: { name: string; fullCode: string; code: string }) => (state.code === stateCode)
    )?.fullCode;
}